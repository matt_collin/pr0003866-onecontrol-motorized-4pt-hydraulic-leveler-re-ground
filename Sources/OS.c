// OS.c
// Program entry point
// (c) 2012 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void OS_Startup(void);

#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void OS_OnTIMCH7Interrupt(void);
#pragma CODE_SEG DEFAULT

#pragma CODE_SEG __NEAR_SEG NON_BANKED
uint16 OS_GetElapsedMilliseconds16(void);
uint32 OS_GetElapsedMilliseconds32(void);
#pragma CODE_SEG DEFAULT

// 500 microsecond interrupt
#define TIMER_PRESCALER         2
#define TIMER_INTERRUPT_COUNTS  (uint16)(((FBUS / TIMER_PRESCALER) * 0.0005) + 0.5)
#define TIMER_CLOCK            	(FBUS / TIMER_PRESCALER)

#else

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static volatile DWORD ElapsedMilliseconds = { 0 };

#pragma CODE_SEG __NEAR_SEG NON_BANKED
uint16 OS_GetElapsedMilliseconds16(void) { return AtomicRead16(&ElapsedMilliseconds.ui16.lo); };
uint32 OS_GetElapsedMilliseconds32(void) { return AtomicRead32(&ElapsedMilliseconds.ui32); }
#pragma CODE_SEG DEFAULT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void Startup_InitSFR(void)
{
	// pin routing register
	// this only applies to the 20 pin package
	// PRR0 00000000
	//      ||||||||
	//      |||||| \\__ SCI0 RX / TX routing   00 = PE0 / PE1   01 = PS4 / PS7   10 = PAD4 / PAD57   11 = reserved
	//      |||| \\____ IOC2 routing           00 = PS5   01 = PE0   10 = PAD4   11 = reserved
	//      || \\______ IOC3 routing           00 = PS6   01 = PE1   10 = PAD5   11 = reserved
	//      | \________ PWM2/ETRIG2 routing    0 = PS4   1 = PAD4
	//       \_________ PWM3/ETRIG3 routing    0 = PS7   1 = PAD5
	PRR0 = 0x00;

	// pin routing register
	// this only applies to the 100 pin package
	// PRR1 -------0
	//             |
	//              \__ 0 = AN inputs on PORTAD    1 = AN inputs on PORTC
//  PRR1 = 0x00;

	// pullup/pulldown control register
	// PUCR -1-11111 data register
	//       | |||||
	//       | |||| \__ PUPAE  1 = enable pullup on PORT A     0 = pullup disabled
	//       | ||| \___ PUPBE  1 = enable pullup on PORT B     0 = pullup disabled
	//       | || \____ PUPCE  1 = enable pullup on PORT C     0 = pullup disabled
	//       | | \_____ PUPDE  1 = enable pullup on PORT D     0 = pullup disabled
	//       |  \______ PDPEE  1 = enable pulldown on PORT E   0 = pulldown disabled
	//        \________ BKPUE  1 = enable pullup on BKGD pin   0 = pullup disabled
	PUCR = 0x5F;

	// PORT AD01
	//  PT01AD  xxxxxxxx xxxxxxxx data register
	//  PTI01AD -------- -------- input only
	//  DDR01AD 00000000 00000000 data direction register
	//  PER01AD 11111111 01100000 pull up enable register
	//  PPS01AD xxxxxxxx xxxxxxxx interrupt polarity select register (1 = pulldown, 0 = pullup)
	//  PIE01AD 00000000 00000000 interrupt enable register
	//  PIF01AD 11111111 11111111 interrupt flag register
	//  ATDDIEN 11111111 11100011 digital input enable (set to 1 to enable digital readout on PT1AD)
	//          |||||||| ||||||||
	//          |||||||| ||||||| \__ PARK_BRAKE_SIGNAL
	//          |||||||| |||||| \___ PSI_SW_SIGNAL
	//          |||||||| ||||| \____ ADC_SENSOR_SIGNAL
	//          |||||||| |||| \_____ ADC_PVOUT_SIGNAL
	//          |||||||| ||| \______ ADC_IGNITION_RUN_SIGNAL
	//          |||||||| || \_______ unused
	//          |||||||| | \________ unused
	//          ||||||||  \_________ DEBUG_1
	//
	//          ||||||| \___________ unused
	//          |||||| \____________ unused
	//          ||||| \_____________ unused
	//          |||| \______________ unused
	//          ||| \_______________ unused
	//          || \________________ unused
	//          | \_________________ unused
	//           \__________________ unused
	PT01AD  = 0x0000;
	DDR01AD = 0x0000;
	PER01AD = 0xFF60;
	PPS01AD = 0x0000;
	PIE01AD = 0x0000;
	PIF01AD = 0xFFFF; // clear flags
	ATDDIEN = 0xFFE3;

	// PORT A  
	// PORTA xxxxxxxx data register
	//  DDRA 00000000 data data direction register
	//       ||||||||
	//       ||||||| \__ unused
	//       |||||| \___ unused
	//       ||||| \____ unused
	//       |||| \_____ unused
	//       ||| \______ unused
	//       || \_______ unused
	//       | \________ unused
	//        \_________ unused
	PORTA = 0x00;
	DDRA  = 0x00;

	// PORT B
	// PORTB x0xxxxxx data register
	//  DDRB 01000000 data data direction register
	//       ||||||||
	//       ||||||| \__ unused
	//       |||||| \___ unused
	//       ||||| \____ unused
	//       |||| \_____ unused
	//       ||| \______ unused
	//       || \_______ unused
	//       | \________ unused
	//        \_________ unused
	PORTB = 0x00;
	DDRB  = 0x00;

	// PORT C  
	// PORTC xxxxxxxx data register
	//  DDRC 00000000 data data direction register
	//       ||||||||
	//       ||||||| \__ unused
	//       |||||| \___ unused
	//       ||||| \____ unused
	//       |||| \_____ unused
	//       ||| \______ unused
	//       || \_______ unused
	//       | \________ unused
	//        \_________ unused
	PORTC = 0x00;
	DDRC  = 0x00;

	// PORT D
	// PORTD x0xxxxxx data register
	//  DDRD 01000000 data data direction register
	//       ||||||||
	//       ||||||| \__ unused
	//       |||||| \___ unused
	//       ||||| \____ unused
	//       |||| \_____ unused
	//       ||| \______ unused
	//       || \_______ unused
	//       | \________ unused
	//        \_________ unused
	PORTD = 0x00;
	DDRD  = 0x00;

	// PORT E
	// PORTE x0xxxxxx data register
	//  DDRE 01000000 data data direction register
	//       ||||||||
	//       ||||||| \__ unused
	//       |||||| \___ unused
	//       ||||| \____ unused
	//       |||| \_____ unused
	//       ||| \______ unused
	//       || \_______ unused
	//       | \________ unused
	//        \_________ unused
	PORTE = 0x00;
	DDRE  = 0x00;

	// PORT J
	//  PTJ xxxxxxxx data register
	// PTIJ -------- input register
	// DDRJ 00001111 data direction register
	// PERJ 11110000 pull resistor enable register
	// PPSJ 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
	// PIEJ 00000000 interrupt enable register
	// PIFJ 11111111 interrupt flag register
	//      ||||||||
	//      ||||||| \__ EXTERNAL_SWITCH_CONTROL
	//      |||||| \___ AIR_FILL_CONTROL
	//      ||||| \____ AIR_DUMP_CONTROL
	//      |||| \_____ CAN_STANDBY
	//      ||| \______ unused
	//      || \_______ unused
	//      | \________ unused
	//       \_________ unused
	PTJ  = 0x00;
	DDRJ = 0x0F;
	PERJ = 0xF0;
	PPSJ = 0x00;
	PIEJ = 0x00;
	PIFJ = 0xFF; // clear flags

	// PORT M
	//  PTM xxxxxx1x data register
	// PTIM -------- input register
	// DDRM 00000010 data direction register
	// PERM 11111101 pull resistor enable register
	// PPSM 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
	// WOMM 00000000 wired or mode (0 = push/pull, 1 = open-drain)
	//      ||||||||
	//      ||||||| \__ RXCAN
	//      |||||| \___ TXCAN
	//      ||||| \____ unused
	//      |||| \_____ unused
	//      ||| \______ unused
	//      || \_______ unused
	//      | \________ unused
	//       \_________ unused
	PTM  = 0x02;
	DDRM = 0x02;
	PERM = 0xFD;
	PPSM = 0x00;
	WOMM = 0x00;

	// PORT P
	//  PTP xx000000 data register
	// PTIP -------- input register
	// DDRP 00111111 data direction register
	// PERP 11000000 pull resistor enable register
	// PPSP 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
	// PIEP 00000000 interrupt enable register
	// PIFP 11111111 interrupt flag register
	//      ||||||||
	//      ||||||| \__ LR_CONTROL
	//      |||||| \___ RF_CONTROL
	//      ||||| \____ EXT_CONTROL
	//      |||| \_____ RET_CONTROL
	//      ||| \______ RR_CONTROL
	//      || \_______ LF_CONROL
	//      | \________ unused
	//       \_________ unused
	PTP  = 0x00;
	DDRP = 0x3F;
	PERP = 0xC0;
	PPSP = 0x00;
	PIEP = 0x00;
	PIFP = 0xFF; // clear flags

	// PORT S
	//  PTS 0000001x data register
	// PTIS -------- input register
	// DDRS 00000010 data direction register
	// PERS 11111101 pull resistor enable register
	// PPSS 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
	// WOMS 00000000 wired or mode (0 = push/pull, 1 = open-drain)
	//      ||||||||
	//      ||||||| \__ RXD0
	//      |||||| \___ TXD0
	//      ||||| \____ STAT3
	//      |||| \_____ STAT2
	//      ||| \______ STAT1
	//      || \_______ STAT4
	//      | \________ unused
	//       \_________ unused
	PTS  = 0x02;
	DDRS = 0x02;
	PERS = 0xFD;
	PPSS = 0x00;
	WOMS = 0x00;

	// PORT T
	//  PTT xxxxxxxx data register
	// PTIT -------- input register
	// DDRT 00010000 data direction register
	// PERT 11000000 pull resistor enable register
	// PPST 00000000 pull resistor polarity select (0 = pullup, 1 = pulldown)
	//      ||||||||
	//      ||||||| \__ HBRIDGE_11 (input for now)
	//      |||||| \___ HBRIDGE_10 (input for now)
	//      ||||| \____ HBRIDGE_01 (input for now)
	//      |||| \_____ HBRIDGE_00 (input for now)
	//      ||| \______ TP_TX_CONTROL
	//      || \_______ TP_RX_SIGNAL
	//      | \________ unused
	//       \_________ unused
	PTT  = 0x00;
	DDRT = 0x10;
	PERT = 0xC0;
	PPST = 0x00;
}

void main(void)
{
	uint16 timer_init;
	
	// device initialization
	// code auto generated by dev environment
	MCU_init();

	Startup_InitSFR();

	// module initialization
	ADC_Init();
	SCI_Init();

	// intialize real-time interrupt
	TIOS = 0x80;  	// enable output compare 7
	TTOV = 0x00;  	// toggle output compare pin on overflow disabled
	TCTL1 = 0x00; 	// no action on output compare
	TCTL2 = 0x00; 	// no action on output compare
	TCTL3 = 0x00; 	// input capture disabled
	TCTL4 = 0x00; 	// input capture disabled
	OCPD = 0x80;  	// disable the OC port.  OC action will not occur on pin, but OC flag will still become set
	TIE = 0x80;   	// enable output compare 7 interrupt
	TSCR2 = TIMER_PRESCALER >> 1;  	// prescale 0   clock = FBUS / 2

	EEPROM_Init();
	Sensor_InitBuffers();
	Config_Init();
	Input_Init();
	CAN_Init();
	IDS_CAN_Init();
	Panel_Init();
	LevelerType3_Init();
	Logic_Init();
	Jack_Init();
	Manufacturing_Init();
	IDS_CAN_EnableDevice(DEVICE_LEVELER);
	IDS_CAN_EnableDevice(DEVICE_CHASSIS_INFO);
	
	// sensor hardware drive timing is tied to TCNT, it is important that Sensor_Init() and first TC7 program are back-to-back
	timer_init = TCNT;
	VirtualSCI_Init(timer_init);
	Sensor_InitOutputCompare(timer_init);
	TC7 = timer_init + TIMER_INTERRUPT_COUNTS;
	TSCR1 = 0xA0; 	// enable the timer

	// background program
	for(;;)
	{
		static uint8 _1ms = 0;
		static uint8 _10ms = 0;

		EnableInterrupts;

		// service the COP while interrupts appear to be running
		if (_1ms != ElapsedMilliseconds.word.lo.ui8.lo)
		{
			_1ms = ElapsedMilliseconds.word.lo.ui8.lo;
			_FEED_COP();
		}

		if ((ElapsedMilliseconds.word.lo.ui8.lo - _10ms) >= 10)
		{
			_10ms = ElapsedMilliseconds.word.lo.ui8.lo;

			// execute 10 millisecond program
			Input_Task();
			Manufacturing_Task10ms();
			Logic_Task10ms();
			Jack_Task10ms();
			LevelerType3_Task10ms();
            Lockout_Task10ms();
		}

		// continuous background program
		Panel_Task();
		Config_Task();
		SCI_Task();
		SCIHost_Task();
		Sensor_Task();
		CAN_Task();
		IDS_CAN_Task();
	}
}

#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void OS_OnTIMCH7Interrupt(void)
{
	static uint8 bit = 0;

	// service interrupt
	TFLG1 = BIT7;
	
	TC7 += TIMER_INTERRUPT_COUNTS;

	// there 2 time sensitive tasks to run
	// alternate every other interrupt to hopefully minimize edge jitter
	Sensor_Task500us();
	VirtualSCI_Task500us();

	if (++bit & 1)
		ElapsedMilliseconds.ui32++;
}
#pragma CODE_SEG DEFAULT

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
