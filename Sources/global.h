// Global.h
// Global header for 17759 LCI Level-Up Controller (LincPad) SW
// (c) 2012, 2013, 2014, 2015 Innovative Design Solutions, Inc.
// All rights reserved

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef HEADER
#define HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define DESCRIPTION "OneControl Motorized 4pt Hydraulic Leveler (Re-Ground) SW"
#define PART_NUMBER "25778-B"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// basic operating environment

#define MC9S12G96
#define XTAL            16000000
#define FBUS            25000000

// include header information 
#include <hidef.h>      // common defines and macros
#include "derivative.h" // auto-generated derivative-specific definitions
#include "MCUinit.h"    // auto-generated initialization routines
#include "Sys HC12.c"   // IDS HC12 system/environment routines

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// port I/O macros

#define PARK_BRAKE_SIGNAL                   PT01AD_PT01AD0

#define PSI_SWITCH_SIGNAL                   PT01AD_PT01AD1

#define STATUS_OUTPUT_CONTROL               PTJ_PTJ0
#define Assert_STATUS_OUTPUT_CONTROL        (PTJ_PTJ0 = 1)
#define Deassert_STATUS_OUTPUT_CONTROL      (PTJ_PTJ0 = 0)

#define AIR_FILL_CONTROL                    PTJ_PTJ1
#define Assert_AIR_FILL_CONTROL             (PTJ_PTJ1 = 1)
#define Deassert_AIR_FILL_CONTROL           (PTJ_PTJ1 = 0)

#define AIR_DUMP_CONTROL                    PTJ_PTJ2
#define Assert_AIR_DUMP_CONTROL             (PTJ_PTJ2 = 1)
#define Deassert_AIR_DUMP_CONTROL           (PTJ_PTJ2 = 0)

#define CAN_STANDBY                         PTJ_PTJ3
#define Assert_CAN_STANDBY                  (PTJ_PTJ3 = 1)
#define Deassert_CAN_STANDBY                (PTJ_PTJ3 = 0)

#define LR_CONTROL                          PTP_PTP0
#define Assert_LR_CONTROL                   (PTP_PTP0 = 1)
#define Deassert_LR_CONTROL                 (PTP_PTP0 = 0)

#define RF_CONTROL                          PTP_PTP1
#define Assert_RF_CONTROL                   (PTP_PTP1 = 1)
#define Deassert_RF_CONTROL                 (PTP_PTP1 = 0)

#define _4WAY_VALVE_CONTROL                 PTP_PTP2
#define Assert_4WAY_VALVE_CONTROL           (PTP_PTP2 = 1)
#define Deassert_4WAY_VALVE_CONTROL         (PTP_PTP2 = 0)

#define PUMP_CONTROL                        PTP_PTP3
#define Assert_PUMP_CONTROL                 (PTP_PTP3 = 1)
#define Deassert_PUMP_CONTROL               (PTP_PTP3 = 0)

#define RR_CONTROL                          PTP_PTP4
#define Assert_RR_CONTROL                   (PTP_PTP4 = 1)
#define Deassert_RR_CONTROL                 (PTP_PTP4 = 0)

#define LF_CONTROL                          PTP_PTP5
#define Assert_LF_CONTROL                   (PTP_PTP5 = 1)
#define Deassert_LF_CONTROL                 (PTP_PTP5 = 0)

#define STAT3                               PTS_PTS2
#define STAT2                               PTS_PTS3
#define STAT1                               PTS_PTS4
#define STAT4                               PTS_PTS5

#define TP_TX_CONTROL                       PTT_PTT4
#define Assert_TP_TX_CONTROL                (PTT_PTT4 = 1)
#define Deassert_TP_TX_CONTROL              (PTT_PTT4 = 0)

#define TP_RX_SIGNAL                        PTT_PTT5

//#define SENSOR_ADC_DEBUG_1                  PT01AD_PT01AD7
//#define DEBUG_1                             PT01AD_PT01AD7
//#define DEBUG_CAN_TX_INTERRUPT_TASK_PORT    PT01AD_PT01AD7
//#define DEBUG_CAN_RX_INTERRUPT_TASK_PORT    PT01AD_PT01AD7

// ADC pin definitions
// ADC channels
#define ADC_SENSOR_OUTPUT   2
#define TP_VBATT_SIGNAL     3
#define ADC_VBATT_SIGNAL    4

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// sensor definitions
// SENSOR_TIMER_CHANNEL(name, channel)
//		name 	  - name of the channel / pin
//		channel - hardware timer channel
//
//		note:  the sensor drive scheme is as follows (by order in list)
//			- X1: SENSOR_PIN1 = HI, SENSOR_PIN2 = HI, SENSOR_PIN3 = LO, SENSOR_PIN4 = LO
//			- X2: SENSOR_PIN1 = LO, SENSOR_PIN2 = LO, SENSOR_PIN3 = HI, SENSOR_PIN4 = HI
//			- Y1: SENSOR_PIN1 = HI, SENSOR_PIN2 = LO, SENSOR_PIN3 = LO, SENSOR_PIN4 = HI
//			- Y2: SENSOR_PIN1 = LO, SENSOR_PIN2 = HI, SENSOR_PIN3 = HI, SENSOR_PIN4 = LO

#define DECLARE_SENSOR_TIMER_CHANNELS \
	SENSOR_TIMER_CHANNEL(SENSOR_PIN1, 1)	\
	SENSOR_TIMER_CHANNEL(SENSOR_PIN2, 2)	\
	SENSOR_TIMER_CHANNEL(SENSOR_PIN3, 3)	\
	SENSOR_TIMER_CHANNEL(SENSOR_PIN4, 0)

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// special types/enumerations specific to this system
#define LOW_VOLTAGE  10.2
#define CAL_VOLTAGE	 11.5
#define VOLTAGE_RATIO  (uint16)((256*LOW_VOLTAGE/CAL_VOLTAGE)+0.5)

// battery dropout detection parameters
#define BATTERY_DROPOUT_TRIP_VOLTAGE            7.5     // was 6.0 but never triggered, 7.0 sometimes worke, 7.25 might be good enough but might get an occasional nvm fault, so go to 7.5
#define BATTERY_DROPOUT_RETURN_VOLTAGE          8.0
#define BATTERY_DROPOUT_DETECTION_LOOP_TIME     0.001   /* 1 milliseconds */
#define BATTERY_DROPOUT_TRIP_DEBOUNCE           (uint16) (((0.004 / BATTERY_DROPOUT_DETECTION_LOOP_TIME) + 0.5))    /* 4 milliseconds */
#define BATTERY_DROPOUT_RETURN_DEBOUNCE         (uint16) (((1.0 / BATTERY_DROPOUT_DETECTION_LOOP_TIME) + 0.5))      /* 250 milliseconds */

// battery range detection parameters
#define BATTERY_RANGE_DETECTION_LOOP_TIME       0.010   /* 10 milliseconds */
#define BATTERY_RANGE_DEBOUNCE                  (uint8) (((1.0 / BATTERY_RANGE_DETECTION_LOOP_TIME) + 0.5)) /* 1 second */

#define BATTERY_LOW_TRIP_VOLTAGE                9.0     // this is checked when a move is attempted
#define BATTERY_LOW_RETURN_VOLTAGE              9.5

#define BATTERY_HIGH_TRIP_VOLTAGE               18.0
#define BATTERY_HIGH_RETURN_VOLTAGE             17.5

// these are time based checks, no movement attempt is needed
#define UNIVERSAL_LOOP_TIME                     0.01    /* 10 milliseconds    */
#define UNIVERSAL_LOW_BATT1                     10.0
#define UNIVERSAL_LOW_BATT1_DEBOUNCE            (uint16) (((5*60 / UNIVERSAL_LOOP_TIME) + 0.5))
#define UNIVERSAL_LOW_BATT2                     9.0
#define UNIVERSAL_LOW_BATT2_DEBOUNCE            (uint16) (((1*60 / UNIVERSAL_LOOP_TIME) + 0.5))

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

typedef struct {
	uint16 PN; 
	char Rev;
	uint32 SN;
} IDS_PCB_BARCODE;

// declare non-volatile configuration sets
#define DECLARE_CONFIGURATION                   \
\
CONFIG_BLOCK(UserBlock, USE_NVM, NO_KAM)        \
CONFIG(int16, XZero)                            \
CONFIG(int16, YZero)                            \
CONFIG(uint8, ZeroValid)                        \
CONFIG(uint16, AirBagPrefillTime)               \
CONFIG(uint8, TouchPadPresent)                  \
\
CONFIG_BLOCK(ProductionBlock, USE_NVM, NO_KAM)  \
CONFIG(uint16,          XLSBsPerDegree)         \
CONFIG(uint16,          YLSBsPerDegree)         \
CONFIG(uint16,          VBATT_EngineRun)        \
CONFIG(uint16,          VBATT_Low)              \
CONFIG(uint8,           ProductionByte)         \
CONFIG(IDS_PCB_BARCODE, IDSPCBBarCode)          \
CONFIG(uint8,           MAC[6])                 \
CONFIG(uint8,           DtcByte)                \
\
CONFIG_BLOCK(DiagnosticBlock, USE_NVM, NO_KAM)  \
CONFIG(uint8, LatchedRetractError)              \
CONFIG(uint8, FullRetractByUser)                \
CONFIG(uint8, AirFeaturesEnabled)               \
CONFIG(uint8, TouchpadErrorDisplayed)           \

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// digital input debounce information
// Usage: INPUT(name, TEST(expression), DEBOUNCE(type, count), init, event)
//       name - software name of input (Input.Digital.name and Input.Digital.nameEvent)
// expression - expression that returns actual digital value of input port
//       type - debounce counter variable type (uint8, uint16, etc...)
//      count - debounce trigger count (when count reached input is debounced)
//       init - initial boot value (INIT_TRUE / INIT_FALSE / INIT_ACTUAL)
//      event - defines optional event support (EVENT_SUPPORT / NO_EVENT_SUPPORT)
#define DIGITAL_INPUTS \
INPUT(JacksAllUp,  TEST(PSI_SWITCH_SIGNAL), DEBOUNCE(uint16, PSI_Switch_Debounce), INIT_TRUE,  NO_EVENT_SUPPORT) \
INPUT(ParkBrakeIsOn,  TEST(!PARK_BRAKE_SIGNAL), DEBOUNCE(uint16, 5), INIT_TRUE,  NO_EVENT_SUPPORT) \
INPUT(AirFillStatus,  TEST(STAT2), DEBOUNCE(uint16, 50), INIT_FALSE,  NO_EVENT_SUPPORT) \
INPUT(AirDumpStatus,  TEST(STAT2), DEBOUNCE(uint16, 50), INIT_FALSE,  NO_EVENT_SUPPORT) \

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// analog input information
// Usage: INPUT(name, read, filter)
//   name - software name of the input (creates Input_name() function)
//   read - expression that reads the analog value (16 bit result)
// filter - number of filter bits (value += (newvalue - value) >> filter)
#define ANALOG_INPUTS                                     \
INPUT(VBatt, ADC_Convert16(ADC_VBATT_SIGNAL),  2) \

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// analog voltage to LSB conversion

// hardware environment
#define VDD  5.0    /* volts */
#define ADC_LSB(v)  	((uint16) ((((float) (v) / VDD) * 0xFFC0) + 0.5) )
#define VBATT_LSB(v)	ADC_LSB( ((v) * 95.3) / (221 + 95.3) )

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// virtual SCI parameters
#define VIRTUAL_SCI_MAX_RX_MESSAGE_LENGTH 2
#define VIRTUAL_SCI_MAX_TX_MESSAGE_LENGTH 2

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// SCI parameters
#define SCI_PORT(reg)                 SCI0##reg /* SCI, SCI0, SCI1, etc... */
#define SCI_DEFAULT_BAUD_RATE         57600
#define SCI_TX_BUFFER_SIZE_BYTES      256
#define SCI_RX_BUFFER_SIZE_BYTES      256 /* only needed in interrupt operation */
#define SCI_RX_MAX_MESSAGE_LENGTH     40
#define SCI_DRIVER_INTERRUPT
//#define SCI_MAINTAIN_STATISTICS       /* usage statistics are maintained if this is defined */
//#define DEBUG_SCI_INTERRUPT_TASK_PORT   DEBUG_1
//#define DEBUG_SCI_BACKGROUND_TASK_PORT  DEBUG_1

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// CAN settings

// CAN timing parameters (TSEG1, TSEG2, SJW, etc...)
// need to be coordinated with the network
#define CAN_BAUD_RATE   250000  /* 250kbps */
#define CAN_TSEG1       12
#define CAN_TSEG2       3
#define CAN_SJW         3

// CAN driver parameters
// NOTE: buffer sizes do not need to be a power of 2
#define CAN_DRIVER_INTERRUPT           /* define either CAN_DRIVER_POLLED or CAN_DRIVER_INTERRUPT */
#define CAN_DRIVER_RX_INTERRUPT_NEST		/* define if we have other timing critical interrupts */
#define CAN_DRIVER_TX_INTERRUPT_NEST		/* define if we have other timing critical interrupts */
#define CAN_BUFFER_SIZE_BYTES  2048  
#define CAN_MAINTAIN_STATISTICS        /* usage statistics are maintained if this is enabled */
//#define DEBUG_CAN_TX_INTERRUPT_TASK_PORT   DEBUG_1
//#define DEBUG_CAN_RX_INTERRUPT_TASK_PORT   DEBUG_1
//#define DEBUG_CAN_BACKGROUND_TASK_PORT  DEBUG_1

// CAN message filters
// messages that match these identifiers will trigger the appropriate callback
// only MASK bits set to 1 are matched -- other bits are assumed to match
// filters are scanned in the order listed here
//
// there are two types of filters, TX and RX
// CAN_TX_MESSAGE(filter_id, filter_mask, callback)
// CAN_RX_MESSAGE(filter_id, filter_mask, callback)
//    filter match occurs when (message_id & filter_mask) == filter_id
//    callback is called if tx/rx match is made
#define CAN_MESSAGE_FILTERS \
    CAN_RX_MESSAGE_FILTER( ID(0), MASK(0), CALLBACK(Manufacturing_CANMessageRx)) \
    CAN_RX_MESSAGE_FILTER( ID(0), MASK(0), CALLBACK(IDS_CAN_OnCanMessageRx))

// declare all devices that are supported
// IDS_CAN_DEVICE(device_name, DEVICE(device_type, device_instance, device_capabilities))
//   device_name = local enumerated name passed to functions
//   device_type = DEVICE_TYPE of the device
//   device_instance = device instance
//   device_capabilities = expression that returns the capabilities of the device
#define DECLARE_IDS_CAN_DEVICES \
    DECLARE_IDS_CAN_DEVICE(DEVICE_LEVELER, DEVICE(IDS_CAN_DEVICE_TYPE_LEVELER_TYPE_3, 0x00, 0x01)) \
    DECLARE_IDS_CAN_DEVICE(DEVICE_CHASSIS_INFO, DEVICE(IDS_CAN_DEVICE_TYPE_CHASSIS_INFO, 0x00, 0x00)) \

// console parameters
// IDS_CAN_TEXT_CONSOLE(name, width, height)
//   name = local name passed to functions
//   width = characters per line (cannot be greater than 64)
//   height = total rows (cannot be greater than 32)

// console parameters
#define CONSOLE_HEIGHT 7
#define CONSOLE_WIDTH 25

#define DECLARE_IDS_CAN_TEXT_CONSOLES \
    DECLARE_IDS_CAN_TEXT_CONSOLE(DEVICE_LEVELER, CONSOLE_WIDTH, CONSOLE_HEIGHT)

// define the DTC block storage API
#define IDS_CAN_DTC_PAUSE_POWER_CYCLES                          1
#define IDS_CAN_DTC_NUM_STORAGE_BLOCKS                          NUM_STORAGE_BLOCKS /* all blocks reserved for DTCs */
#define IDS_CAN_DTC_STORAGE_BLOCK_SIZE                          STORAGE_BLOCK_SIZE_BYTES /* each block is 128 bytes long */
#define IDS_CAN_DTC_STORAGE_ERASED_VALUE                        STORAGE_BLOCK_ERASED_STATE
#define IDS_CAN_DTC_STORAGE_ERASE_BLOCK(block)                  Block_Erase(block)
#define IDS_CAN_DTC_STORAGE_READ(block, offset, count, buffer)  Block_Read(block, offset, count, buffer)
#define IDS_CAN_DTC_STORAGE_WRITE32(block, offset, buffer)      Block_Write(block, offset, 4, buffer)

// define all DTCs that are supported by the firmware
#define DECLARE_SUPPORTED_IDS_CAN_DTCS \
    DECLARE_DTC(IDS_CAN_DTC_0002_ECU_IS_DEFECTIVE,                  1) \
    DECLARE_DTC(IDS_CAN_DTC_0005_BATTERY_VOLTAGE_LOW,               1) \
    DECLARE_DTC(IDS_CAN_DTC_000B_LEVELER_ZERO_POINT_NOT_CONFIGURED, 1) \
    DECLARE_DTC(IDS_CAN_DTC_000F_TOUCH_PAD_COMM_FAILURE,            1) \
    DECLARE_DTC(IDS_CAN_DTC_0447_JACK_LF_OUT_OF_STROKE,             1) \
    DECLARE_DTC(IDS_CAN_DTC_0463_JACK_LR_OUT_OF_STROKE,             1) \
    DECLARE_DTC(IDS_CAN_DTC_0471_JACK_RF_OUT_OF_STROKE,             1) \
    DECLARE_DTC(IDS_CAN_DTC_048D_JACK_RR_OUT_OF_STROKE,             1) \
    DECLARE_DTC(IDS_CAN_DTC_0636_AUTO_LEVEL_TIMEOUT,                1) \
    DECLARE_DTC(IDS_CAN_DTC_0637_AUTO_LEVEL_FAIL,                   1) \
    DECLARE_DTC(IDS_CAN_DTC_0638_AUTO_RETRACT_TIMEOUT,              1) \
    DECLARE_DTC(IDS_CAN_DTC_063A_GROUND_JACKS_TIMEOUT,              1) \
    DECLARE_DTC(IDS_CAN_DTC_063B_EXCESS_ANGLE,                      1) \
    DECLARE_DTC(IDS_CAN_DTC_063C_USER_PANIC_STOP,                   1) \
    DECLARE_DTC(IDS_CAN_DTC_063D_PARK_BRAKE_NOT_ENGAGED,            1) \
    DECLARE_DTC(IDS_CAN_DTC_063F_PSI_SWITCH_TIMEOUT,                1) \
    DECLARE_DTC(IDS_CAN_DTC_0651_AUTO_START_VOLTAGE_LOW,            1) \
    DECLARE_DTC(IDS_CAN_DTC_0687_OPERATING_VOLTAGE_DROPOUT,         1)

#define IDS_CAN_NUM_DAQ_CHANNELS    16

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define IDSCORE_LOG_ON_ASSERT_FAILURE
#define IDSCORE_ASSERT_LOG_SIZE        32

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define LOCKOUT_DEVICE_BEHAVIORS    LOCKOUT_DEVICE(DEVICE_LEVELER,      ALL_COMMANDS_DURING_LOCKOUT) \
                                    LOCKOUT_DEVICE(DEVICE_CHASSIS_INFO, ALL_COMMANDS_DURING_LOCKOUT)

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(_MSC_VER) || defined(_ECLIPSE)
#else

// IDS-CAN support
#include "CAN.c"                 // required - CAN driver
#include "IDS-CAN.c"             // required - IDS-CAN driver

// include software module header information
#include "ADC.c"
#include "Config_B.c"
#include "ConsoleDriver.c"
#include "CRC.c"
#include "EEPROM.c"
#include "IDS-CAN_Integration.c"
#include "IDSCore.c"
#include "IDSCore.Assert.c"
#include "Input.c"
#include "Jack.c"
#include "LevelerType3.c"
#include "Lockout.c"
#include "Logic.c"
#include "Manufacturing.c"
#include "OS.c"
#include "RF.c"
#include "SCI.c"
#include "SCI Host.c"
#include "Sensor.c"
#include "Touchpad.c"
#include "VirtualSCI.c"

#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#undef HEADER
#endif
