// LevelerType3.c
// handles IDS-CAN status and command messages for leveler type 3
// (c) 2016 Innovative Design Solutions, Inc.
// All rights reserved																		

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void LevelerType3_Init(void);
void LevelerType3_Task10ms(void);
 
void LevelerType3_OnCommandMessageRx(const IDS_CAN_RX_MESSAGE *message);
void LevelerType3_OnIDSCANSessionOpened(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
void LevelerType3_OnIDSCANSessionClosed(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
uint16 LevelerType3_TimeSinceSessionClosed_ms(void); // zero if session opened, timer otherwise
uint16 LevelerType3_GetInputAge_ms(void); // time since last "valid" input message received

typedef enum {
   LEVELER_TYPE_3_SCREEN_DEFAULT      		= 0,
   LEVELER_TYPE_3_SCREEN_IDLE_MOTORIZED   = 1,
   LEVELER_TYPE_3_SCREEN_IDLE_TOWABLE     = 2,
   LEVELER_TYPE_3_SCREEN_AUTO             = 3,
   LEVELER_TYPE_3_SCREEN_MANUAL           = 4,
   LEVELER_TYPE_3_SCREEN_SELECT			  = 5,
   LEVELER_TYPE_3_SCREEN_YES_NO           = 6,
   LEVELER_TYPE_3_SCREEN_INFO             = 7,
   LEVELER_TYPE_3_SCREEN_ALERT            = 8,
   LEVELER_TYPE_3_SCREEN_INVALID          = 0xFF
} LEVELER_TYPE_3_SCREEN;

void LevelerType3_SetScreen(LEVELER_TYPE_3_SCREEN screen);

typedef enum {
   LEVELER_TYPE_3_OUTPUT_OFF           = 0,
   LEVELER_TYPE_3_OUTPUT_BLINK         = 1,
   LEVELER_TYPE_3_OUTPUT_INVERSE_BLINK = 2,
   LEVELER_TYPE_3_OUTPUT_ON            = 3
} LEVELER_TYPE_3_OUTPUT_STATUS;

void LevelerType3_SetAllStatusOutputs(LEVELER_TYPE_3_OUTPUT_STATUS status);
void LevelerType3_SetAllGreyButtons(uint8 set);

typedef enum {
   BLINK_125_MS 	= 0x00,
   BLINK_250_MS 	= 0x01,
   BLINK_375_MS 	= 0x02,
   BLINK_500_MS 	= 0x03,
   BLINK_625_MS 	= 0x04,
   BLINK_750_MS 	= 0x05,
   BLINK_875_MS 	= 0x06,
   BLINK_1_S 		= 0x07,
   BLINK_1_125_S 	= 0x08,
   BLINK_1_250_S 	= 0x09,
   BLINK_1_375_S 	= 0x0A,
   BLINK_1_500_S 	= 0x0B,
   BLINK_1_625_S 	= 0x0C,
   BLINK_1_725_S 	= 0x0D,
   BLINK_1_875_S 	= 0x0E,
   BLINK_2_S = 0x0F
} LEVELER_TYPE_3_BLINK_RATE;
   
typedef struct {
   uint8 Extend:1;
   uint8 Back:1;
   uint8 MenuUp:1;
   uint8 AutoHitch:1;
   uint8 EnterSetup:1;
   uint8 b13:1;
   uint8 b14:1;
   uint8 b15:1;

   uint8 Right:1;
   uint8 Left:1;
   uint8 Rear:1;
   uint8 Front:1;
   uint8 AutoLevel:1;
   uint8 Retract:1;
   uint8 Enter:1;
   uint8 MenuDown:1;
} LEVELER_TYPE_3_BUTTONS;

typedef struct {
   LEVELER_TYPE_3_SCREEN Screen;
   LEVELER_TYPE_3_BUTTONS Buttons;
} LEVELER_TYPE_3_COMMAND_MESSAGE;

typedef struct {
   LEVELER_TYPE_3_SCREEN Screen;
   LEVELER_TYPE_3_BUTTONS ButtonsToGrey;
   
   struct {
      uint8 OnCount:4;
      uint8 OffCount:4;
   } BlinkRate;

   struct {
      uint8 RightLED:2;
      uint8 LeftLED:2;
      uint8 RearLED:2;
      uint8 FrontLED:2;

      uint8 Buzzer:2;
      uint8 RetractLED:2;
      uint8 ExtendLED:2;
      uint8 LevelLED:2;
   } Outputs;  
} LEVELER_TYPE_3_STATUS_MESSAGE;

typedef struct 
{
   LEVELER_TYPE_3_COMMAND_MESSAGE Input;
   LEVELER_TYPE_3_STATUS_MESSAGE Status;
} LEVELER_UI;

extern LEVELER_UI LevelerType3;

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#undef SECONDS
#define SECONDS(s)                  (uint16)((s) / .010 + 0.5)

#define STATUS_TRANSMIT_TIME_SLOW   SECONDS(1.0)
#define STATUS_TRANSMIT_TIME_FAST   SECONDS(0.25)

LEVELER_UI LevelerType3;

static const LEVELER_TYPE_3_BUTTONS ClearButtonInputs = {0}; 

static uint16 InputAge_ms = 0xFFFF;
uint16 LevelerType3_GetInputAge_ms(void) { return InputAge_ms; }

static uint16 SessionClosedAge_ms = 0xFFFF;
uint16 LevelerType3_TimeSinceSessionClosed_ms(void) { return SessionClosedAge_ms; }

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static void ClearInputs(void)
{
	LevelerType3.Input.Screen = LEVELER_TYPE_3_SCREEN_INVALID;
	LevelerType3.Input.Buttons = ClearButtonInputs;
}

static void SetChassisInfoDeviceStatus(void)
{
    uint8 val = 0;

    if (Input.Digital.ParkBrakeIsOn)
        val |= 0x02;
    else
        val |= 0x01;

    // if we are powered, ign is on
    val |= 0x04;

    IDS_CAN_SetDeviceStatus(DEVICE_CHASSIS_INFO, 1, (const uint8*)&val);
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// handle the h-bridge command on the CAN bus
void LevelerType3_OnCommandMessageRx(const IDS_CAN_RX_MESSAGE *message)
{
	IDS_CAN_DEVICE device;

	// verify this is a command message of the proper length
	if (message->Length != 3) return;

	// is the target address of the command one of our devices?
	device = IDS_CAN_GetDeviceFromAddress(message->TargetAddress);
	if (device >= NUM_IDS_CAN_DEVICES) return;

	// ignore the command if the session is not open
	if (IDS_CAN_GetSessionOwner(device, IDS_CAN_SESSION_ID_REMOTE_CONTROL) != message->SourceAddress) return;

	LevelerType3.Input.Buttons = ClearButtonInputs;
	LevelerType3.Input.Screen = (LEVELER_TYPE_3_SCREEN) message->Data[0];
	
	// are we on the correct screen?
	// ignore button state if we are on the wrong screen 
	if (LevelerType3.Input.Screen != LevelerType3.Status.Screen)
	   return;

	// screen state is correct, accept the button states
	if ((message->Data[1] & 0x10) && !LevelerType3.Status.ButtonsToGrey.EnterSetup) LevelerType3.Input.Buttons.EnterSetup = 1;
	if ((message->Data[1] & 0x08) && !LevelerType3.Status.ButtonsToGrey.AutoHitch)  LevelerType3.Input.Buttons.AutoHitch = 1;
	if ((message->Data[1] & 0x04) && !LevelerType3.Status.ButtonsToGrey.MenuUp)     LevelerType3.Input.Buttons.MenuUp = 1;
	if ((message->Data[1] & 0x02) && !LevelerType3.Status.ButtonsToGrey.Back)       LevelerType3.Input.Buttons.Back = 1;
	if ((message->Data[1] & 0x01) && !LevelerType3.Status.ButtonsToGrey.Extend)     LevelerType3.Input.Buttons.Extend = 1;
	
	if ((message->Data[2] & 0x80) && !LevelerType3.Status.ButtonsToGrey.MenuDown)   LevelerType3.Input.Buttons.MenuDown = 1;
	if ((message->Data[2] & 0x40) && !LevelerType3.Status.ButtonsToGrey.Enter)      LevelerType3.Input.Buttons.Enter = 1;
	if ((message->Data[2] & 0x20) && !LevelerType3.Status.ButtonsToGrey.Retract)    LevelerType3.Input.Buttons.Retract = 1;
	if ((message->Data[2] & 0x10) && !LevelerType3.Status.ButtonsToGrey.AutoLevel)  LevelerType3.Input.Buttons.AutoLevel = 1;
	if ((message->Data[2] & 0x08) && !LevelerType3.Status.ButtonsToGrey.Front)      LevelerType3.Input.Buttons.Front = 1;
	if ((message->Data[2] & 0x04) && !LevelerType3.Status.ButtonsToGrey.Rear)       LevelerType3.Input.Buttons.Rear = 1;
	if ((message->Data[2] & 0x02) && !LevelerType3.Status.ButtonsToGrey.Left)       LevelerType3.Input.Buttons.Left = 1;
	if ((message->Data[2] & 0x01) && !LevelerType3.Status.ButtonsToGrey.Right)      LevelerType3.Input.Buttons.Right = 1;

	InputAge_ms = 0; // age means "how long since we received a valid command whose screen matched ours"
}

#pragma MESSAGE DISABLE C5703 /* variables not used */
void LevelerType3_OnIDSCANSessionOpened(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id)
{
	if (device == DEVICE_LEVELER && id == IDS_CAN_SESSION_ID_REMOTE_CONTROL)
	{
		ClearInputs();
		SessionClosedAge_ms = 0;
	}
	//lint -e(715)   variables not used
}
#pragma MESSAGE DEFAULT C5703

#pragma MESSAGE DISABLE C5703 /* variables not used */
void LevelerType3_OnIDSCANSessionClosed(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id)
{
	if (device == DEVICE_LEVELER && id == IDS_CAN_SESSION_ID_REMOTE_CONTROL)
	{
		ClearInputs();
		if (SessionClosedAge_ms == 0)
			SessionClosedAge_ms = 1; // start counting
	}
	//lint -e(715)   variables not used
}
#pragma MESSAGE DEFAULT C5703

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void LevelerType3_SetScreen(LEVELER_TYPE_3_SCREEN screen)
{
	LevelerType3.Status.Screen = screen;   
}

void LevelerType3_SetAllStatusOutputs(LEVELER_TYPE_3_OUTPUT_STATUS set)
{
	const uint8 status[4] = {0x00, 0x55, 0xAA, 0xFF};
	uint8 * output;

	output = (uint8*)&LevelerType3.Status.Outputs;   
	*output++ = status[set];
	*output = status[set];
}

void LevelerType3_SetAllGreyButtons(uint8 set)
{
	uint8 n = sizeof(LevelerType3.Status.ButtonsToGrey);
	do
	{
		((uint8 *) &LevelerType3.Status.ButtonsToGrey)[--n] = set;
	} while (n);  
}

static void ResetStatus(void)
{
	uint8 n = sizeof(LevelerType3.Status);
	do
	{
		((uint8 *) &LevelerType3.Status)[--n] = 0;
	} while (n);
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void LevelerType3_Init(void)
{
	ClearInputs();
	InputAge_ms = 0xFFFF; // inputs are bogus
	ResetStatus();
	LevelerType3.Status.BlinkRate.OnCount = LevelerType3.Status.BlinkRate.OffCount = (uint8) BLINK_375_MS;
}

void LevelerType3_Task10ms(void)
{
	// age the inputs
	if (InputAge_ms < (0xFFFF - 10))
		InputAge_ms += 10;
	else
		InputAge_ms = 0xFFFF;

	if ((SessionClosedAge_ms > 0) || (InputAge_ms > 3000))
	{
		ClearInputs();
	}

	// age the session
	if (IDS_CAN_GetSessionOwner(DEVICE_LEVELER, IDS_CAN_SESSION_ID_REMOTE_CONTROL))
		SessionClosedAge_ms = 0; // session is open
	else if (SessionClosedAge_ms < (0xFFFF - 10))
		SessionClosedAge_ms += 10; // session has closed
	else
		SessionClosedAge_ms = 0xFFFF;
		
	IDS_CAN_SetDeviceStatus(DEVICE_LEVELER, sizeof(LEVELER_TYPE_3_STATUS_MESSAGE), (const uint8 *) &LevelerType3.Status.Screen);
	SetChassisInfoDeviceStatus();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
