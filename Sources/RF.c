// RF.c
// RF Receiver
// (c) 2005, 2008, 2009 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void RF_Init(void);           // initialize module
void RF_Task(void);           // background task
void RF_Task250us(void);      // real-time task

const uint8 * RF_RX(void);    // get pointer to RX message (if any)
void RF_RXFlushMessage(void); // flush oldest RX message from buffer

uint8 RF_RXActive(void);      // non-zero if RF module is receiving

// diagnostics
uint32 RF_NumRxBytes(void);
uint32 RF_NumRxMessages(void);
uint32 RF_NumRxOverflows(void);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined RF_ACTIVE_HIGH && defined RF_ACTIVE_LOW
   #error cannot define both RF_ACTIVE_HIGH and RF_ACTIVE_LOW
#elif !defined RF_ACTIVE_HIGH && !defined RF_ACTIVE_LOW
 #error must define either RF_ACTIVE_HIGH or RF_ACTIVE_LOW
#endif

// speed at which the real time runs
#define REAL_TIME_TASK_PERIOD  0.000250 /* 250 usec */

// stream timeout
// must be larger than 4 bit times
// should also include headroom for accomodate polling jitter
#define STREAM_TIMEOUT         (uint8)(8.0 / RF_BAUD_RATE / REAL_TIME_TASK_PERIOD + 0.5) /* 8 bit times */

// macro to calculate bit time, in timer clock cycles
#define BIT_TIME(percent)      ((uint16)(((((float) percent) * TIMER_CLOCK) / RF_BAUD_RATE) + 0.5))

// datagram tags
#define PREAMBLE               0x0  /* 0000 */

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// message buffer structure
typedef struct {
	uint8 Length;
	struct {
		uint8 Data[RF_RX_MAX_MESSAGE_LENGTH];
	} Buffer;
} RX_MESSAGE;

// the message buffer
static RX_MESSAGE RX = { 0 };

// bit-stream buffer
// bits are captured in real-time, and stored here for background processing
static struct {
	uint8 Timeout;                    // stream timeout
	uint8 LastBit;                    // last bit received by the stream
	uint8 Head, Tail;                 // buffer management
	uint8 Buffer[RF_BIT_QUEUE_SIZE];  // circular bit buffer
} Stream = { 0, 0, 0, 0 };

// diagnostics
static uint32 NumRxBytes = 0;
static uint32 NumRxMessages = 0;
static uint32 NumRxOverflows = 0;
uint32 RF_NumRxBytes(void) { return NumRxBytes; }
uint32 RF_NumRxMessages(void) { return NumRxMessages; }
uint32 RF_NumRxOverflows(void) { return NumRxOverflows; }

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void RF_Init(void)
{
	EnterCriticalSection();

	TCTL4 &= 0xCF;
	  
	#if defined RF_ACTIVE_HIGH
	  TCTL4 |= 0x20;    // channel 3, capture falling edge
	#elif defined RF_ACTIVE_LOW
	  TCTL4 |= 0x10;   // channel 3, capture rising edge
	#else
	  #error must define input edge capture
	#endif
	

	LeaveCriticalSection();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// message decoding
// protocol identical to that used by CompuGlobalHyperMegaNet

const uint8 * RF_RX(void)
{
	if (!RX.Length)
		return NULL;
	return (const uint8 *) &RX;
}

void RF_RXFlushMessage(void)
{
	RX.Length = 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// background task
// processes bits received from the bitstream buffer
void RF_Task(void)
{
	static uint8 shift_register = 0;       // used to build incoming bytes
	static uint8 bit_count = 0;            // number of bits in shift register
	static RX_MESSAGE msg = {0};           // message construction buffer
	static uint8 lastbyte = 0;             // last byte received
	static uint8 crc = 0;                  // running CRC
	uint8 c = elementsof(Stream.Buffer)/2; // process up to half of the buffer each time we are called
	uint8 bit;

	// loop until all bits are processed, or we use up all attempts
	do
	{
		// exit when buffer is empty
		if (Stream.Head == Stream.Tail)
			break;

		// unload the next bit
		bit = Stream.Buffer[Stream.Head % elementsof(Stream.Buffer)];
		Stream.Head++;

		// is the stream reset flag set?
		if (bit & 0x80)
		{
			// we need to clear out any partially received messages

			// is the message valid?
			// - at least 1 byte (+ crc) was received
			// - CRC is valid
			if (msg.Length >= 2 && crc == lastbyte)
			{
			  NumRxMessages++;

  			// save the message if RX buffer is empty
			  // and message (without crc) fits within buffer
			  if (!RX.Length && --msg.Length <= sizeof(msg.Buffer))
			  {
  				RX.Buffer = msg.Buffer; // save message
  				RX.Length = msg.Length; // save length (without checksum)
			  }
			  else
			    NumRxOverflows ++;
			}

			// reset stream
			msg.Length = 0;     // reset message buffer
			crc = 0;		        // restart crc
			shift_register = 0; // reset the shift register
			bit_count = 0;      // look for preamble
		}

		// process the bit

		// shift into our shift register
		shift_register = (uint8) ((shift_register << 1) | (bit & 1));

		// handle bits received
		// bit_count 1-4 = looking for preamble
		// bit_count 5-12 = receiving byte
		if (++bit_count == 4)
		{
			// check for preamble
			if ((shift_register & 0xF) != PREAMBLE)
				bit_count = 3; // keep waiting until preamble is received
		}
		else if (bit_count >= 4 + 8)
		{
			// byte recieved in shift register
			NumRxBytes++;

			// update the CRC with the last byte received
			if (msg.Length)
				crc = CRC8_Update(crc, lastbyte);

			// save byte if there is room in the buffer
			if (msg.Length < sizeof(msg.Buffer.Data))
				msg.Buffer.Data[msg.Length] = shift_register;

			// increase message length by 1
			if (msg.Length != 0xFF)
				msg.Length++;

			// remember this byte for the next pass
			lastbyte = shift_register;

			// get ready to read another 8 bits
			bit_count = 4;
		}
	} while (--c); // loop until we run out of attempts
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// edge detection
static uint8 BitsCaptured(void)
{
	static WORD edge = { 0 };
	uint8 bits = 255;
	uint16 lastedge;

	// edge detected?
	if (!TFLG1_C2F)
	  return 0;
	
	// remember time that last edge was captured
	lastedge = edge.ui16;
  
  // read the time the edge was captured
	edge.ui8.hi = TC2Hi;  // read MSB first
	edge.ui8.lo = TC2Lo; 
	TFLG2 = 0x04;         // clear the flag
	
	// if last edge is still valid
	if (Stream.Timeout)
	{
	    // calculate bits seen since last edge
		  //
		  //        edge - lastedge
		  // bits = --------------- + 0.5 (rounding)
		  //          1 bit time
		
		  bits = (uint8) ((uint16) ((edge.ui16 - lastedge) + BIT_TIME(0.5)) / BIT_TIME(1.0) );
	  					
	  	// if less than 1 bit was captured
	  	if (!bits)
		  {
		  	// we've captured a glitch
		  	edge.ui16 = lastedge; // ignore glitch - wait for next edge
		  	return 0;
		  }
  }
	
	// reset timeout
	Stream.Timeout = STREAM_TIMEOUT;

	return bits;
}

static void StreamIn(uint8 bit)
{
	static uint8 reset = 0x80;
  
	// if bit is invalid
	if (bit > 1)
	{
		// reset the buffer
		Stream.LastBit = 0;
		reset = 0x80;
		return;
	}

	// if no room in buffer
	if ((uint8)((uint8)(Stream.Tail - Stream.Head) % elementsof(Stream.Buffer)) >= elementsof(Stream.Buffer) - 1)
	{
		// add reset flag next time around
		reset = 0x80;
		return;
	}

	// save the bit
	Stream.Buffer[Stream.Tail++ % elementsof(Stream.Buffer)] = (uint8)(bit | reset);

	// remember last bit received
	Stream.LastBit = bit;

	// clear reset flag
	reset = 0;
}

void RF_Task250us(void)
{
	// manage stream timeout
	if (Stream.Timeout && !--Stream.Timeout) 
		StreamIn(0xFF); // reset stream
	
	// manchester decode the bits received
	switch (BitsCaptured())
	{
	case 0: // no bits captured
		break;

	case 2: // 2 bits
		StreamIn(Stream.LastBit);
		break;

	case 3: // 3 bits
		if (Stream.LastBit)
		{
			StreamIn(0);
			break;
		}
		//lint -fallthrough

	case 4: // 4 bits
		StreamIn(0);
		StreamIn(1);
		break;

	default: // bad number of bits
		StreamIn(0xFF); // reset stream
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

uint8 RF_RXActive(void)
{
	// receiver is active if ANY of the following are true
	// - stream timeout has not expired
	// - message buffer is full
	return (uint8)(Stream.Timeout | RX.Length);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
