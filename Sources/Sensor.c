// Sensor.c
// Sensor drive routine for Fredericks tilt sensor
// (c) 2003,7,9 Innovative Design Solutions, Inc.
// All rights reserved

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// called periodically to execute sensor sampling routines
void Sensor_InitOutputCompare(uint16 init);
void Sensor_InitBuffers(void);

void Sensor_Task(void);
void Sensor_Task500us(void);
uint16 Abs(int16 val);

// external access to sensor data
typedef struct { uint16 Set, Reset; int16 Raw, Filter, Angle; } SENSOR_CHANNEL;
typedef struct { uint8 Valid; SENSOR_CHANNEL X, Y; } SENSOR;
#ifndef SENSOR_C

/*lint -save -e18 lint doesn't how this is used */
extern const SENSOR Sensor; // constant access externally
/*lint -restore */

#endif

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
		  
#define SENSOR_C

#include "global.h"

#define SENSOR_EDGE_TIME(p) ((uint16)((p * TIMER_INTERRUPT_COUNTS) + 0.5))

#define FILTER       4  /* shift by 4 routine called every 10ms*/

const uint8 TIMER_FLAG_BITS = 
	#undef SENSOR_TIMER_CHANNEL
	#define SENSOR_TIMER_CHANNEL(name, channel) (1 << channel) |
	DECLARE_SENSOR_TIMER_CHANNELS
	0;
 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// sensor information (externally visible)
SENSOR Sensor;

// buffered sensor data (copied from real-time program)
static struct {
	uint8 Valid;
	uint8 Locked;
	struct { uint16 Set, Reset; int16 Raw; } X, Y;
} Buffer;

// data history and accumulator
#define HISTORY_SIZE      16 /* 16 sample history buffer */
#define ACCUMULATOR_SHIFT 4  /* shift by 4 to divide by 16 */
static struct {
	struct {
	int32 Value; // holds the sum
	int16 History[HISTORY_SIZE]; // sample history
	} X, Y;
} Accumulator;

static struct {
	uint16 Set, Reset, Sample;
	int16 Raw;
} RT;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static void Clear(uint8 count, void * address)
{
	while (count--)
		((uint8 *) address)[count] = 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
uint16 Abs(int16 val)
{
	if (val >= 0)
		return (uint16) val;
	else
		return (uint16) -val;
}


static void ProcessSample(int16 raw, SENSOR_CHANNEL * channel, int16 zero, uint16 lsbsperdegree)
{
	int32 filter;

#if 0
	// limit movement per sample
	if (Sensor.Valid)
	{
		uint16 MaxMovementPerSample = lsbsperdegree >> 2; // 1/4 degree

		if (raw >= channel->Raw)
		{
			if((uint16)(raw - channel->Raw) > MaxMovementPerSample)
				raw = channel->Raw + MaxMovementPerSample;
		}
		else if((uint16)(channel->Raw - raw) > MaxMovementPerSample)
			raw = channel->Raw - MaxMovementPerSample;
	}
#endif

	// save raw sample
	channel->Raw = raw;

	// filter the value
	if (!Sensor.Valid)
		channel->Filter = raw; // set initial value
	if (raw != channel->Filter)
	{
		int16 delta = (raw - channel->Filter); // delta value (relies on fact that raw cannot change full scale in two back-to-back readings)
		delta >>= FILTER;
		if (!delta)
			delta = 1;
		channel->Filter += delta;
	}

	// promote to 32 bits
	filter = channel->Filter;

	// subtract zero point
	filter -= zero;

	// calculate angle (fixed point, 1/256th of a degree resolution)
	//
	// (filter - zero) LSBs      1 Degree         256
	// ------------------ x ------------------- x ---
	//          1           lsb_per_degree LSBs    1
	filter <<= 8;                     // multiply by 256 (for 1/256 degree resolution)
//	raw = lsbsperdegree >> 1; // rounding factor (0.5)
//	if (filter < 0)
//		raw = -raw;	
//	filter += raw; // round by adding 0.5
	channel->Angle = (int16) (filter / lsbsperdegree); // calculate angle
}

void Sensor_Task(void)
{
	static uint8 _10ms = 0;
	int16 raw;

	if (Buffer.Valid && (uint8) (OS_GetElapsedMilliseconds16() - _10ms) >= 10)
	{
		_10ms += 10;

		// lock the buffer (prevent real-time from using it)
		// and copy data to background program
		// then process it
		Buffer.Locked = TRUE;
		Sensor.X.Set = Buffer.X.Set;
		Sensor.X.Reset = Buffer.X.Reset;
		raw = Buffer.X.Raw;
		Buffer.Locked = FALSE;
		ProcessSample(raw, &Sensor.X, Config.UserBlock.XZero, Config.ProductionBlock.XLSBsPerDegree);

		Buffer.Locked = TRUE;
		Sensor.Y.Set = Buffer.Y.Set;
		Sensor.Y.Reset = Buffer.Y.Reset;
		raw = Buffer.Y.Raw;
		Buffer.Locked = FALSE;
		ProcessSample(raw, &Sensor.Y, Config.UserBlock.YZero, Config.ProductionBlock.YLSBsPerDegree);

		Sensor.Valid = TRUE;
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// real-time variables
static uint8 HistoryIndex = 0xFF; // index into history buffers

static void CalcRaw(int16 last)
{
	// save reset value
	RT.Reset = RT.Sample;

	// calculate raw value
	RT.Raw = (RT.Set >> 1) - (RT.Reset >> 1);

	// raw value must not change more than 768 LSBs
	if (Sensor.Valid && Abs(RT.Raw - last) > 768)
	{
		if (RT.Raw > last)
			RT.Raw = last + 768;
		else
			RT.Raw = last - 768;
	}
}

static void X1(void)
{
	// just acquired Y2

	// drive next sensor output state (X1)
	
	#undef SENSOR_PIN1
	#define SENSOR_PIN1(level) level = 1;
	#undef SENSOR_PIN2
	#define SENSOR_PIN2(level) level = 1;
	#undef SENSOR_PIN3
	#define SENSOR_PIN3(level) level = 0;
	#undef SENSOR_PIN4
	#define SENSOR_PIN4(level) level = 0;
	
	#undef SENSOR_TIMER_CHANNEL
	#define SENSOR_TIMER_CHANNEL(name, channel) name(TCTL2_OL##channel)
	DECLARE_SENSOR_TIMER_CHANNELS
	
	// calculate raw value
	CalcRaw(Accumulator.Y.History[(uint8)(HistoryIndex-1) % HISTORY_SIZE]);

	// update history buffer, and accumulator
	Accumulator.Y.Value -= Accumulator.Y.History[HistoryIndex % HISTORY_SIZE];
	Accumulator.Y.History[HistoryIndex % HISTORY_SIZE] = RT.Raw;
	Accumulator.Y.Value += RT.Raw;

	// copy to background program
	if (!Buffer.Locked)
	{
		Buffer.Y.Set = RT.Set;
		Buffer.Y.Reset = RT.Reset;
		Buffer.Y.Raw = (int16) (Accumulator.Y.Value >> ACCUMULATOR_SHIFT);
	}

	// we have finished acquiring an X/Y data set
	// restart sampling cycle -- move to next index
	if (++HistoryIndex >= HISTORY_SIZE + 7)
	{
		// data is valid after 25th cycle
		// this is a full set of data + 7 samples which are ignored
		// this takes 50 ms after interrupts start running
		Buffer.Valid = TRUE;
	}
}

// NOTE: WE MUST START IN X2 STATE, OTHERWISE ACCUMULATORS GET SCREWED UP!
static void X2(void)
{
	// just acquired X1

	// drive next sensor output state (X2)
	// TCTLx_OLx: output action to be taken on next compare. 0 = clear  1 = set
 	#undef SENSOR_PIN1
	#define SENSOR_PIN1(level) level = 0;
	#undef SENSOR_PIN2
	#define SENSOR_PIN2(level) level = 0;
	#undef SENSOR_PIN3
	#define SENSOR_PIN3(level) level = 1;
	#undef SENSOR_PIN4
	#define SENSOR_PIN4(level) level = 1;
	
	#undef SENSOR_TIMER_CHANNEL
	#define SENSOR_TIMER_CHANNEL(name, channel) name(TCTL2_OL##channel)
	DECLARE_SENSOR_TIMER_CHANNELS
  	
 	// save X1
 	RT.Set = RT.Sample;
}

static void Y1(void)
{
	// just acquired X2

	// drive next sensor output state (Y1)
	// TCTLx_OLx: output action to be taken on next compare. 0 = clear  1 = set
   #undef SENSOR_PIN1
	#define SENSOR_PIN1(level) level = 1;
	#undef SENSOR_PIN2
	#define SENSOR_PIN2(level) level = 0;
	#undef SENSOR_PIN3
	#define SENSOR_PIN3(level) level = 0;
	#undef SENSOR_PIN4
	#define SENSOR_PIN4(level) level = 1;
	
	#undef SENSOR_TIMER_CHANNEL
	#define SENSOR_TIMER_CHANNEL(name, channel) name(TCTL2_OL##channel)
	DECLARE_SENSOR_TIMER_CHANNELS
	
	// calculate raw value
	CalcRaw(Accumulator.X.History[(uint8)(HistoryIndex-1) % HISTORY_SIZE]);

	// update history buffer, and accumulator
	Accumulator.X.Value -= Accumulator.X.History[HistoryIndex % HISTORY_SIZE];
	Accumulator.X.History[HistoryIndex % HISTORY_SIZE] = RT.Raw;
	Accumulator.X.Value += RT.Raw;

	// copy to background program
	if (!Buffer.Locked)
	{
		Buffer.X.Set = RT.Set;
		Buffer.X.Reset = RT.Reset;
		Buffer.X.Raw = (int16) (Accumulator.X.Value >> ACCUMULATOR_SHIFT);
	}
}

static void Y2(void)
{
	// just acquired Y1

	// drive next sensor output state (Y2)
	// TCTLx_OLx: output action to be taken on next compare. 0 = clear  1 = set
	#undef SENSOR_PIN1
	#define SENSOR_PIN1(level) level = 0;
	#undef SENSOR_PIN2
	#define SENSOR_PIN2(level) level = 1;
	#undef SENSOR_PIN3
	#define SENSOR_PIN3(level) level = 1;
	#undef SENSOR_PIN4
	#define SENSOR_PIN4(level) level = 0;
	
	#undef SENSOR_TIMER_CHANNEL
	#define SENSOR_TIMER_CHANNEL(name, channel) name(TCTL2_OL##channel)
	DECLARE_SENSOR_TIMER_CHANNELS

   RT.Set = RT.Sample; // save Y1
}


void Sensor_InitOutputCompare(uint16 init)
{
	// clear timer flag
	TFLG1 = TIMER_FLAG_BITS;

	// configure OC pin, set up 1st OC at 0.25 bit time from now (This should allow the ADC reading to occur ~3/4 way between sensor edges)
	// then enable the OC channel
	#undef SENSOR_TIMER_CHANNEL
	#define SENSOR_TIMER_CHANNEL(name, channel) TIOS_IOS##channel = 1; TCTL2_OM##channel = 1; TC##channel = init + SENSOR_EDGE_TIME(0.25);
	DECLARE_SENSOR_TIMER_CHANNELS

	//drive y2, to set up for first phase (x1)
	Y2();
}

void Sensor_InitBuffers(void)
{
	Clear(sizeof(Sensor),      &Sensor);
	Clear(sizeof(Buffer),      &Buffer);
	Clear(sizeof(Accumulator), &Accumulator);
	Clear(sizeof(RT),          &RT);
}

void Sensor_Task500us(void)
{
	// controls X1/X2/Y1/Y2 state of real-time
	// NOTE: WE MUST START IN X1 STATE, OTHERWISE LOGIC/VARS GET SCREWED UP!
	static void (* const Func[4])(void) = { X1, X2, Y1, Y2 };
	static uint8 State = 0;

	// clear flags, make sure OC channels are enabled, prepare for next OC (500us from last, ~250us from now)
	TFLG1 = TIMER_FLAG_BITS;

	#undef SENSOR_TIMER_CHANNEL
	#define SENSOR_TIMER_CHANNEL(name, channel) TC##channel += SENSOR_EDGE_TIME(1.0);
	DECLARE_SENSOR_TIMER_CHANNELS

	// take present ADC sample
#ifdef SENSOR_ADC_DEBUG_1
	SENSOR_ADC_DEBUG_1 = 1;
#endif

	RT.Sample = (uint16) (ADC_Convert16(ADC_SENSOR_OUTPUT));

#ifdef SENSOR_ADC_DEBUG_1
	SENSOR_ADC_DEBUG_1 = 0;
#endif

	// execute appropriate function
	Func[(State++) & 3]();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
