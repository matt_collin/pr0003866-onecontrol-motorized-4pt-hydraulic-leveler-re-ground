// ADC.c
// ADC driver for Freescale MC9S12 microcontrollers
// Version 1.2
// (c) 2010, 2012 Innovative Design Solutions, Inc.
// All rights reserved

// history
// 1.0  First revision - based on HCS08 ADC driver version 1.6
// 1.1  Updated to support MC9S12P family
// 1.2  Updated to support MC9S12G family

// usage
//
// ON DEMAND CONVERSION
//
// Module defaults to on-demand conversions, which are performed
// when ADC_Convert() is called.
//
//
// CONTINUOUS CONVERSION
// 
// Optionally, the converter can be placed into continous conversion
// mode, where channels are automatically converted without user
// intervention.
//
// To enable this mode, define both
//    ADC_CONTINUOUS_CONVERSION_START_CHANNEL
//    ADC_CONTINUOUS_CONVERSION_END_CHANNEL
// These values define the range of channels to be continuously converted
//
// The last ADC conversion taken can be retrieved by calling ADC_Convert()
//
// Note that channels outside of the continuous conversion range cannot
// be read from, and will return 0 LSBs.

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// processor check / determine ADC module version
#if   defined MC9S12G48 || defined MC9S12G64 || defined MC9S12G96 || defined MC9S12G128
	#define ADC10B12CV2 /* 10-bit, 12 channels */
#elif defined MC9S12HY32 || defined MC9S12HY48 || defined MC9S12HY64
	#define ADC12B8CV1
#elif defined MC9S12P32 || defined MC9S12P64 || defined MC9S12P96 || defined MC9S12P128
	#define ADC12B10C
#else
	#error ADC service not designed for this processor
#endif

// declare number of ADC bits read
#if   defined ADC10B12CV2
	#define ADC_BITS 10
	#define ADC_MAX 0xFFC0
#elif defined ADC12B8CV1
	#define ADC_BITS 12
	#define ADC_MAX 0xFFF0
#endif
#if   defined ADC12B10C
	#define ADC_BITS 10
	#define ADC_MAX 0xFFC0
#endif

// VDD check
#ifndef VDD
	#error Microcontroller VDD must be defined
#endif

// conversion macros for turning ADC voltages into LSB results
#define VADC_TO_LSB8(v)    (uint8) (((((((v) <= 0) ? 0 : (((v) >= VDD) ? VDD : (v)))) / VDD) * (uint8)(ADC_MAX >> 8)) + 0.5)
#define VADC_TO_LSB16(v)   (uint16)(((((((v) <= 0) ? 0 : (((v) >= VDD) ? VDD : (v)))) / VDD) * ADC_MAX) + 0.5)

void ADC_Init(void);                  // initialize driver
uint8 ADC_Convert8(uint8 channel);    // 8-bit conversion
uint16 ADC_Convert16(uint8 channel);  // 16-bit conversion
uint32 ADC_Convert32(uint8 channel);  // 32-bit conversion

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if !defined ADC_CONTINUOUS_CONVERSION_START_CHANNEL && !defined ADC_CONTINUOUS_CONVERSION_END_CHANNEL
  #define ADC_ON_DEMAND_CONVERSION
#elif !defined ADC_CONTINUOUS_CONVERSION_START_CHANNEL || !defined ADC_CONTINUOUS_CONVERSION_END_CHANNEL
  #error ADC: must define both ADC_CONTINUOUS_CONVERSION_START_CHANNEL and ADC_CONTINUOUS_CONVERSION_END_CHANNEL
#elif ADC_CONTINUOUS_CONVERSION_START_CHANNEL < 0
  #error ADC: ADC_CONTINUOUS_CONVERSION_START_CHANNEL cannot be less than 0
#elif ADC_CONTINUOUS_CONVERSION_END_CHANNEL > 7
  #error ADC: ADC_CONTINUOUS_CONVERSION_END_CHANNEL cannot be greather than 7
#elif ADC_CONTINUOUS_CONVERSION_START_CHANNEL > ADC_CONTINUOUS_CONVERSION_END_CHANNEL
  #error ADC: ADC_CONTINUOUS_CONVERSION_START_CHANNEL cannot be greater than ADC_CONTINUOUS_CONVERSION_END_CHANNEL
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef ADC10B12CV2

// ADC10B12CV2
// 10-bit ADC, 12 channels

// A/D clock frequency is specified between 250kHz and 8MHz
// ADC_CLOCK = FBUS / 2 / (PRS + 1)
//
// PRS = ceil(FBUS / 2 / ADC_CLOCK) - 1

#if FBUS < 1000000
	#error FBUS too slow for proper ADC operation
#endif

// max clock is 8 MHz
// min clock is 250 kHz
#define ADC_CLOCK_MAX (8000000 * 0.95) /* give us 5% headroom to prevent out of spec operation */
#define ADC_CLOCK_MIN ( 250000 * 1.05) /* give us 5% headroom to prevent out of spec operation */

#define CEIL(a, b) ( ((a) + (b) - 1) / (b) )

#define PRS (CEIL(FBUS / 2.0, ADC_CLOCK_MAX) - 1)
#if PRS < 0x00
  #error FBUS too slow for proper ADC operation
#endif
#if PRS > 0x1F
  #error FBUS too fast for proper ADC operation
#endif

#define ADC_CLK   (FBUS / 2.0 / (PRS + 1))

void ADC_Init(void)
{
  // this register specifies the wrap around channel when doing multi-conversions
  ATDCTL0 = 0x0F;

  // ATDCTL1
  // 00110000
	// ||||||||
	// |||| \\\\__ ETRIGGCH3:0   source for external trigger (not used)
	// ||| \______ SMP_DIS        1 = discharge sample capacitor before starting a conversion
	// | \\_______ SRES1:0       01 = 10-bit conversions
	//  \_________ ETRIGSEL      source for external trigger (not used)
  ATDCTL1 = 0x30;

  // ATDCTL2
	// x1x00000
	//  | |||||
	//  | |||| \__ ACMPIE     0 = compare interrupt disabled
	//  | ||| \___ ASCIE      0 = sequence complete interrupt disabled
	//  | || \____ ETRIGE     0 = disable external trigger
	//  | | \_____ ETRIGP     external trigger polarity (unused)
	//  |  \______ ETRIGLE    sensitivity of external trigger (unused)
	//   \________ AFFC       1 = fast flag clear sequence (reading result clears flag)
  ATDCTL2 = 0x40;

  // ATDCTL3
  // 0xxxx000
  // ||||||||
  // |||||| \\__ FRZ1:0    00 = continue conversion when breakpoint encountered
  // ||||| \____ FIFO       0 = first conversion in first register, etc...
  // | \\\\_____ SC      xxxx = number of conversion per sequence
	//  \_________ DJM        0 = left justified results
  #ifdef ADC_ON_DEMAND_CONVERSION
  ATDCTL3 = 0x08; // 1 conversion per sequence
  #else
  ATDCTL3 = (((1 + ADC_CONTINUOUS_CONVERSION_END_CHANNEL - ADC_CONTINUOUS_CONVERSION_START_CHANNEL) & 0xF) << 3);
  #endif

  // ATDCTL4
	// 111xxxxx
	// ||||||||
	// ||| \\\\\__ PRS     ADC_CLK = FBUS / 2 / (PRS + 1)
	//  \\\_______ SMP2:0  111 = sample time is 24 ADC_CLK periods
  ATDCTL4 = 0xE0 | (uint8)PRS;

  ATDSTAT0 = 0xFF; // clear flags
 
  ATDCMPE = 0x0000; // automatic compare disabled

  ATDSTAT2 = 0xFFFF; // clear flags

  // ATDDIEN is product specific initalized in Startup_InitSFR() 

  ATDCMPHT = 0x000; // compare feature not used

  // ATDCTL5
	// -xxxxxxx
	//  |||||||
	//  ||| \\\\__ C        selects channel to convert
	//  || \______ MULT     single channel sampled for each conversion or multiple channels in round-robin
	//  | \_______ SCAN     single conversion or repeat conversion
	//   \________ SC       selects special channel conversion
  #ifdef ADC_ON_DEMAND_CONVERSION
  ATDCTL5 = 0x46; // convert internal test channel
  #elif ADC_CONTINUOUS_CONVERSION_START_CHANNEL == ADC_CONTINUOUS_CONVERSION_END_CHANNEL
  ATDCTL5 = 0x20 | ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #else
  ATDCTL5 = 0x30 | ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #endif
  
	// wait for A/D converter to stabilize
	// this takes one conversion time
  #ifdef ADC_ON_DEMAND_CONVERSION
	(void) ADC_Convert16(0x46);
	#endif
}

uint16 ADC_Convert16(uint8 channel)
{
#ifdef ADC_CONTINUOUS_CONVERSION_START_CHANNEL
  #if ADC_CONTINUOUS_CONVERSION_START_CHANNEL > 0
  channel -= ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #endif
  if (channel <= (uint8)ADC_CONTINUOUS_CONVERSION_END_CHANNEL - ADC_CONTINUOUS_CONVERSION_START_CHANNEL)
    return (&(ATDDR0))[channel];
  return 0;
#else
  
	// range check
	// channels 0x00-0x0F are regular channels
	// channels 0x40-0x46 and 0x48-0x4B are special internal channels
	if (channel > 0x0F && !(channel >= 0x40 && channel <= 0x46) && !(channel >= 0x48 && channel <= 0x4B))
	    return 0;
	
	// attempt conversion until one succeeds that is not interrupted
	for (;;)
	{
  	static volatile uint8 interrupted;
	  uint16 result;
	
		// start A/D conversion
		interrupted = FALSE;	
		ATDCTL5 = channel;

		// wait until conversion is complete or we are interrupted
wait:	if (interrupted)
			continue; // re-start
		if (!ATDSTAT2_CCF0)
			goto wait;

		// get the A/D result
		result = ATDDR0;

		// verify we've successfully read the result
		if (!interrupted)
		{
			// set flag in case this conversion has interrupted another one
			interrupted = TRUE;

			return result;
		}
	}
#endif // ADC_CONTINUOUS_CONVERSION
}

#endif // ADC10B12CV2

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef ADC12B8CV1

// ADC12B8CV1
// 12-bit ADC

// A/D clock frequency is specified between 250kHz and 8MHz
// ADC_CLOCK = FBUS / 2 / (PRS + 1)
//
// PRS = ceil(FBUS / 2 / ADC_CLOCK) - 1

#if FBUS < 1000000
	#error FBUS too slow for proper ADC operation
#endif

// max clock is 8 MHz
// min clock is 250 kHz
#define ADC_CLOCK_MAX (8000000 * 0.95) /* give us 5% headroom to prevent out of spec operation */
#define ADC_CLOCK_MIN ( 250000 * 1.05) /* give us 5% headroom to prevent out of spec operation */

#define CEIL(a, b) ( ((a) + (b) - 1) / (b) )

#define PRS (CEIL(FBUS / 2.0, ADC_CLOCK_MAX) - 1)
#if PRS < 0x00
  #error FBUS too slow for proper ADC operation
#endif
#if PRS > 0x1F
  #error FBUS too fast for proper ADC operation
#endif

#define ADC_CLK   (FBUS / 2.0 / (PRS + 1))

void ADC_Init(void)
{
  // this register specifies the wrap around channel when doing multi-conversions
  ATDCTL0 = 0x0F;

  // ATDCTL1
  // 01010000
	// ||||||||
	// |||| \\\\__ ETRIGGCH3:0   source for external trigger (not used)
	// ||| \______ SMP_DIS        1 = discharge sample capacitor before starting a conversion
	// | \\_______ SRES1:0       10 = 12-bit conversions
	//  \_________ ETRIGSEL      source for external trigger (not used)
  ATDCTL1 = 0x50;

  // ATDCTL2
	// x1000000
	//  |||||||
	//  |||||| \__ ACMPIE     0 = compare interrupt disabled
	//  ||||| \___ ASCIE      0 = sequence complete interrupt disabled
	//  |||| \____ ETRIGE     0 = disable external trigger
	//  ||| \_____ ETRIGP     external trigger polarity (unused)
	//  || \______ ETRIGLE    sensitivity of external trigger (unused)
	//  | \_______ ICLKSTP    0 = conversions disabled in STOP mode
	//   \________ AFFC       1 = fast flag clear sequence (reading result clears flag)
  ATDCTL2 = 0x40;

  // ATDCTL3
  // 0xxxx000
  // ||||||||
  // |||||| \\__ FRZ1:0    00 = continue conversion when breakpoint encountered
  // ||||| \____ FIFO       0 = first conversion in first register, etc...
  // | \\\\_____ SC      xxxx = number of conversion per sequence
	//  \_________ DJM        0 = left justified results
  #ifdef ADC_ON_DEMAND_CONVERSION
  ATDCTL3 = 0x08; // 1 conversion per sequence
  #else
  ATDCTL3 = (((1 + ADC_CONTINUOUS_CONVERSION_END_CHANNEL - ADC_CONTINUOUS_CONVERSION_START_CHANNEL) & 0xF) << 3);
  #endif

  // ATDCTL4
	// 111xxxxx
	// ||||||||
	// ||| \\\\\__ PRS     ADC_CLK = FBUS / 2 / (PRS + 1)
	//  \\\_______ SMP2:0  111 = sample time is 24 ADC_CLK periods
  ATDCTL4 = 0xE0 | (uint8)PRS;

  ATDSTAT0 = 0xFF; // clear flags
 
  ATDCMPE = 0x0000; // automatic compare disabled

  ATDSTAT2 = 0xFFFF; // clear flags

  // ATDDIEN is product specific initalized in Startup_InitSFR() 

  ATDCMPHT = 0x000; // compare feature not used

  // ATDCTL5
	// -xxxxxxx
	//  |||||||
	//  ||| \\\\__ C        selects channel to convert
	//  || \______ MULT     single channel sampled for each conversion or multiple channels in round-robin
	//  | \_______ SCAN     single conversion or repeat conversion
	//   \________ SC       selects special channel conversion
  #ifdef ADC_ON_DEMAND_CONVERSION
  ATDCTL5 = 0x46; // convert internal test channel
  #elif ADC_CONTINUOUS_CONVERSION_START_CHANNEL == ADC_CONTINUOUS_CONVERSION_END_CHANNEL
  ATDCTL5 = 0x20 | ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #else
  ATDCTL5 = 0x30 | ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #endif
  
	// wait for A/D converter to stabilize
	// this takes one conversion time
  #ifdef ADC_ON_DEMAND_CONVERSION
	(void) ADC_Convert16(0x46);
	#endif
}

uint16 ADC_Convert16(uint8 channel)
{
#ifdef ADC_CONTINUOUS_CONVERSION_START_CHANNEL
  #if ADC_CONTINUOUS_CONVERSION_START_CHANNEL > 0
  channel -= ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #endif
  if (channel <= (uint8)ADC_CONTINUOUS_CONVERSION_END_CHANNEL - ADC_CONTINUOUS_CONVERSION_START_CHANNEL)
    return (&(ATDDR0))[channel];
  return 0;
#else
  
	// range check
	// channels 0x00-0x0F are regular channels
	// channels 0x41 and 0x44-0x46 are special internal channels
	if (channel > 0x0F && channel != 0x41 && !(channel >= 0x44 && channel <= 0x46))
	    return 0;
	
	// attempt conversion until one succeeds that is not interrupted
	for (;;)
	{
  	static volatile uint8 interrupted;
	  uint16 result;
	
		// start A/D conversion
		interrupted = FALSE;	
		ATDCTL5 = channel;

		// wait until conversion is complete or we are interrupted
wait:	if (interrupted)
			continue; // re-start
		if (!ATDSTAT2_CCF0)
			goto wait;

		// get the A/D result
		result = ATDDR0;

		// verify we've successfully read the result
		if (!interrupted)
		{
			// set flag in case this conversion has interrupted another one
			interrupted = TRUE;

			return result;
		}
	}
#endif // ADC_CONTINUOUS_CONVERSION
}

#endif // ADC12B8CV1

//////////
//////////
//////////
//////////

#ifdef ADC12B10C

// ADC12B10C
// 10-bit ADC

// A/D clock frequency is specified between 250kHz and 8MHz
// ADC_CLOCK = FBUS / 2 / (PRS + 1)
//
// PRS = ceil(FBUS / 2 / ADC_CLOCK) - 1

#if FBUS < 1000000
	#error FBUS too slow for proper ADC operation
#endif

// max clock is 8 MHz
// min clock is 250 kHz
#define ADC_CLOCK_MAX (8000000 * 0.95) /* give us 5% headroom to prevent out of spec operation */
#define ADC_CLOCK_MIN ( 250000 * 1.05) /* give us 5% headroom to prevent out of spec operation */

#define CEIL(a, b) ( ((a) + (b) - 1) / (b) )

#define PRS (CEIL(FBUS / 2.0, ADC_CLOCK_MAX) - 1)
#if PRS < 0x00
  #error FBUS too slow for proper ADC operation
#endif
#if PRS > 0x1F
  #error FBUS too fast for proper ADC operation
#endif

#define ADC_CLK   (FBUS / 2.0 / (PRS + 1))

void ADC_Init(void)
{
  // this register specifies the wrap around channel when doing multi-conversions
  ATDCTL0 = 0x0F;

  // ATDCTL1
  // 01010000
	// ||||||||
	// |||| \\\\__ ETRIGGCH3:0   source for external trigger (not used)
	// ||| \______ SMP_DIS        1 = discharge sample capacitor before starting a conversion
	// | \\_______ SRES1:0       10 = 12-bit conversions
	//  \_________ ETRIGSEL      source for external trigger (not used)
  ATDCTL1 = 0x50;

  // ATDCTL2
	// x1000000
	//  |||||||
	//  |||||| \__ ACMPIE     0 = compare interrupt disabled
	//  ||||| \___ ASCIE      0 = sequence complete interrupt disabled
	//  |||| \____ ETRIGE     0 = disable external trigger
	//  ||| \_____ ETRIGP     external trigger polarity (unused)
	//  || \______ ETRIGLE    sensitivity of external trigger (unused)
	//  | \_______ ICLKSTP    0 = conversions disabled in STOP mode
	//   \________ AFFC       1 = fast flag clear sequence (reading result clears flag)
  ATDCTL2 = 0x40;

  // ATDCTL3
  // 0xxxx000
  // ||||||||
  // |||||| \\__ FRZ1:0    00 = continue conversion when breakpoint encountered
  // ||||| \____ FIFO       0 = first conversion in first register, etc...
  // | \\\\_____ SC      xxxx = number of conversion per sequence
	//  \_________ DJM        0 = left justified results
  #ifdef ADC_ON_DEMAND_CONVERSION
  ATDCTL3 = 0x08; // 1 conversion per sequence
  #else
  ATDCTL3 = (((1 + ADC_CONTINUOUS_CONVERSION_END_CHANNEL - ADC_CONTINUOUS_CONVERSION_START_CHANNEL) & 0xF) << 3);
  #endif

  // ATDCTL4
	// 111xxxxx
	// ||||||||
	// ||| \\\\\__ PRS     ADC_CLK = FBUS / 2 / (PRS + 1)
	//  \\\_______ SMP2:0  111 = sample time is 24 ADC_CLK periods
  ATDCTL4 = 0xE0 | (uint8)PRS;

  ATDSTAT0 = 0xFF; // clear flags
 
  ATDCMPE = 0x0000; // automatic compare disabled

  ATDSTAT2 = 0xFFFF; // clear flags

  // ATDDIEN is product specific initalized in Startup_InitSFR() 

  ATDCMPHT = 0x000; // compare feature not used

  // ATDCTL5
	// -xxxxxxx
	//  |||||||
	//  ||| \\\\__ C        selects channel to convert
	//  || \______ MULT     single channel sampled for each conversion or multiple channels in round-robin
	//  | \_______ SCAN     single conversion or repeat conversion
	//   \________ SC       selects special channel conversion
  #ifdef ADC_ON_DEMAND_CONVERSION
  ATDCTL5 = 0x46; // convert internal test channel
  #elif ADC_CONTINUOUS_CONVERSION_START_CHANNEL == ADC_CONTINUOUS_CONVERSION_END_CHANNEL
  ATDCTL5 = 0x20 | ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #else
  ATDCTL5 = 0x30 | ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #endif
  
	// wait for A/D converter to stabilize
	// this takes one conversion time
  #ifdef ADC_ON_DEMAND_CONVERSION
	(void) ADC_Convert16(0x46);
	#endif
}

uint16 ADC_Convert16(uint8 channel)
{
#ifdef ADC_CONTINUOUS_CONVERSION_START_CHANNEL
  #if ADC_CONTINUOUS_CONVERSION_START_CHANNEL > 0
  channel -= ADC_CONTINUOUS_CONVERSION_START_CHANNEL;
  #endif
  if (channel <= (uint8)ADC_CONTINUOUS_CONVERSION_END_CHANNEL - ADC_CONTINUOUS_CONVERSION_START_CHANNEL)
    return (&(ATDDR0))[channel];
  return 0;
#else
  
	// range check
	// channels 0x00-0x0F are regular channels
	// channels 0x41 and 0x44-0x46 are special internal channels
	if (channel > 0x0F && channel != 0x41 && !(channel >= 0x44 && channel <= 0x46))
	    return 0;
	
	// attempt conversion until one succeeds that is not interrupted
	for (;;)
	{
  	static volatile uint8 interrupted;
	  uint16 result;
	
		// start A/D conversion
		interrupted = FALSE;	
		ATDCTL5 = channel;

		// wait until conversion is complete or we are interrupted
wait:	if (interrupted)
			continue; // re-start
		if (!ATDSTAT2_CCF0)
			goto wait;

		// get the A/D result
		result = ATDDR0;

		// verify we've successfully read the result
		if (!interrupted)
		{
			// set flag in case this conversion has interrupted another one
			interrupted = TRUE;

			return result;
		}
	}
#endif // ADC_CONTINUOUS_CONVERSION
}

#endif // ADC12B10C

//////////
//////////
//////////
//////////

// simulate an 8-bit conversion
uint8 ADC_Convert8(uint8 channel)
{
	return ADC_Convert16(channel) >> 8;
}

// simulate a 32 bit conversion
uint32 ADC_Convert32(uint8 channel)
{
  DWORD result;
  result.ui16.hi = ADC_Convert16(channel);
  result.ui16.lo = 0;
  return result.ui32;  
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
