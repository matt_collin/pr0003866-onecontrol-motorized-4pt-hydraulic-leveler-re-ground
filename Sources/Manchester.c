// Manchester.c
// Manchester encoded TX/RX (custom for PowerGear Medical Trailer Leveler Controller)
// (c) 2005, 2006, 2007, 2011 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void Manchester_Init(void);
void Manchester_Task(void);

// receiver #1 (TIMER A IC 1)
const uint8 * Manchester_RX1(void);
void Manchester_RX1FlushMessage(void);

// receiver #2 (TIMER A IC 2)
const uint8 * Manchester_RX2(void);
void Manchester_RX2FlushMessage(void);

// transmitter (TIMER A OC 1)
uint8 Manchester_TX(uint16 len, ...);
uint8 Manchester_TXBuffer(uint8 len, const uint8 * buf);
uint8 Manchester_TXIsBufferFull(void);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define PREAMBLE    0x00

// choose prescaler such that 5.5 bit times will not overflow the timer
#define COUNTS (TIMER_CLOCK / MANCHESTER_BAUD_RATE)

#if (5.5 * COUNTS > 65536)
  #error TIMER_CLOCK is too fast for manchester baud rate
#endif 

// returns upper 8 bits of bit-time
#define BIT_TIME(count) (uint8)((((((1.0 * FBUS) * (count)) / TIMER_PRESCALER) / MANCHESTER_BAUD_RATE / 256) + 0.5))

//#define TIMER        TPM1

//#define TX_INVERT    /* define this if IDLE level is HIGH */

#define RX1_TIMER_CHANNEL 0       
#define RX1_TIMER_IC      TC0Hi       /* timer value */
#define RX1_TIMER_SC      TCTL4       /* status control reg */
#define RX1_FLAG          TFLG1_C0F   /* event flag */
#define RX1_FLAG_REG      TFLG1       /* event flag register */
#define RX1_FLAG_BIT      BIT0        /* flag bit */ 
#define RX1_INVERT                    /* define this if IDLE level is HIGH */

#define RX2_TIMER_CHANNEL 1       
#define RX2_TIMER_IC      TC1Hi       /* timer value */
#define RX2_TIMER_SC      TCTL4       /* status control reg */
#define RX2_FLAG          TFLG1_C1F   /* event flag */
#define RX2_FLAG_REG      TFLG1       /* event flag register */
#define RX2_FLAG_BIT      BIT1        /* flag bit */ 
#define RX2_INVERT                    /* define this if IDLE level is HIGH */

#if defined TX_INVERT
  #define TX_BIT_ON  0x00
  #define TX_BIT_OFF 0x01
#else
  #define TX_BIT_ON  0x01
  #define TX_BIT_OFF 0x00
#endif

// RX1 message buffer
typedef struct {
	uint8 Length;
	struct {
		uint8 Data[MANCHESTER_RX1_MAX_MESSAGE_LENGTH];
	} Buffer;
} RX1_MESSAGE;

// RX2 message buffer
typedef struct {
	uint8 Length;
	struct {
		uint8 Data[MANCHESTER_RX2_MAX_MESSAGE_LENGTH];
	} Buffer;
} RX2_MESSAGE;

static uint8 TxLine = FALSE;

//////////
//////////
//////////
//////////

// lookup table used to manchester encode a nibble of data
static const uint8 ManchesterEncode[16] = {
	0x55, // 0000 -> 01010101
	0x56, // 0001 -> 01010110
	0x59, // 0010 -> 01011001
	0x5A, // 0011 -> 01011010
	0x65, // 0100 -> 01100101
	0x66, // 0101 -> 01100110
	0x69, // 0110 -> 01101001
	0x6A, // 0111 -> 01101010
	0x95, // 1000 -> 10010101
	0x96, // 1001 -> 10010110
	0x99, // 1010 -> 10011001
	0x9A, // 1011 -> 10011010
	0xA5, // 1100 -> 10100101
	0xA6, // 1101 -> 10100110
	0xA9, // 1110 -> 10101001
	0xAA, // 1111 -> 10101010
};

//////////
//////////
//////////
//////////

void Manchester_Init(void)
{
	EnterCriticalSection();

  TxLine = TX_BIT_OFF;
	TP_MANCHESTER_TX_PORT = TX_BIT_OFF;

	
	#define RX1_BIT_SHIFT ((RX1_TIMER_CHANNEL % 3) << 1)
	
	RX1_TIMER_SC &= (uint8)(~(0x03 << RX1_BIT_SHIFT));
  #if defined RX1_INVERT
	  RX1_TIMER_SC |= (uint8) (0x01 << RX1_BIT_SHIFT);   // capture rising edge
  #else
	  RX1_TIMER_SC |= (uint8) (0x02 << RX1_BIT_SHIFT);   // capture falling edge
  #endif

  
  #define RX2_BIT_SHIFT ((RX2_TIMER_CHANNEL % 3) << 1)
	
  RX2_TIMER_SC &= (uint8)(~(0x03 << RX2_BIT_SHIFT));
  #if defined RX2_INVERT
	  RX2_TIMER_SC |= (uint8) (0x01 << RX2_BIT_SHIFT);   // capture rising edge
  #else
	  RX2_TIMER_SC |= (uint8) (0x02 << RX2_BIT_SHIFT);   // capture falling edge
  #endif

	LeaveCriticalSection();
}

//////////
//////////
//////////
//////////

// Transmitter section

static struct {
	uint8 Length;
	uint8 Data[MANCHESTER_TX_MAX_MESSAGE_LENGTH + 1]; // +1 for CRC
} TX = {0};

uint8 Manchester_TXIsBufferFull(void)
{
	return TX.Length;
}

// function commits the temporary message in the buffer for transmit
// returns non-zero on success, zero on failure
uint8 Manchester_TX(uint16 len, ...)
{
	return (uint8) (Manchester_TXBuffer((uint8)len, ((uint8*) &len) + 2));
}

uint8 Manchester_TXBuffer(uint8 len, const uint8 * msg)
{
	uint8 n;

	// make sure message will fit into TX buffer
	if (!len || len > MANCHESTER_TX_MAX_MESSAGE_LENGTH)
		return FALSE;

	// make sure space is available in buffer
	if (TX.Length)
		return FALSE;

	// copy bytes "backwards" into buffer
	// NOTE: index 0 is skipped, it contains the CRC generated by the real time task
	n = len;
	do
	{
		TX.Data[n] = *msg++;
	} while (--n);
	TX.Length = len + 1; // add 1 byte for CRC

	return TRUE;
}

static void Manchester_TxTask(void)
{
	static uint16 timer = 0;          // output compare value
	static uint8 state = 0;           // transmitter state (0 = idle, 1 = transmitting, 2 = send EOM)
	static WORD shift_register = {0}; // holds bits to shift out on transmit line
	static uint8 bits_left = 8;       // bits left in the shift register

	// program the output compare to send the next bit in the stream
	if (bits_left)
	{
		// get next bit from shift register
		//TX_TIMER_SC.byte = (shift_register.ui8.hi & 0x80) ? BIT_ON : BIT_OFF;
		TxLine = (shift_register.ui8.hi & 0x80) ? TX_BIT_ON : TX_BIT_OFF;

		// cycle to next bit
		shift_register.ui16 <<= 1;
		if (--bits_left)
			return; // more bits left to transmit
	}
	else
	{
		// idle
		//TX_TIMER_SC.byte = BIT_OFF;
		TxLine = TX_BIT_OFF;
	}

	// we get here if the shift register is empty

	// determine what to do next
	switch (state)
	{
	case 0: // waiting for new message
		if (TX.Length)
		{
			// start transmit
			state++;

			// reset message CRC
			TX.Data[0] = CRC8_Reset();

			// send tone / preamble
			shift_register.ui8.hi = 0xFF; // tone
			shift_register.ui8.lo = ManchesterEncode[PREAMBLE & 0xF]; // preamble
			bits_left = 16;
		}
		break;

	case 1: // transmit message payload
		// index to next byte
		if (!--TX.Length)
			state++; // done transmitting message

		// queue up the byte for transmit
		shift_register.ui8.hi = ManchesterEncode[TX.Data[TX.Length] >> 4];
		shift_register.ui8.lo = ManchesterEncode[TX.Data[TX.Length] & 0xF];

		// update CRC
		TX.Data[0] = CRC8_Update(TX.Data[0], TX.Data[TX.Length]);

		bits_left = 16;
		break;

	default: // transmit EOM
		state = 0; // restart
		shift_register.ui8.hi = 0; // EOM
		bits_left = 8;
		break;
	}
}

//////////
//////////
//////////
//////////

// Receiver #1 section

#undef RX
#undef RX_TIMER_IC
#undef RX_FLAG
#undef RX_FLAG_REG
#undef RX_FLAG_BIT
#undef RX_MESSAGE
#undef Manchester_RX
#undef Manchester_RXFlushMessage
#undef StreamIn
#undef RxTask

#define RX                          RX1
#define RX_TIMER_IC                 RX1_TIMER_IC
#define RX_FLAG                     RX1_FLAG
#define RX_FLAG_REG                 RX1_FLAG_REG
#define RX_FLAG_BIT                 RX1_FLAG_BIT
#define RX_MESSAGE                  RX1_MESSAGE
#define Manchester_RX               Manchester_RX1
#define Manchester_RXFlushMessage   Manchester_RX1FlushMessage
#define StreamIn                    Rx1StreamIn
#define RxTask                      Rx1Task

static struct
{
	uint8 State;           // receiver state
	uint8 ShiftRegister;   // holds incoming byte
	uint8 LastByte;        // last byte received
	uint8 CRC;             // running CRC
	RX_MESSAGE Message;    // message being received
	RX_MESSAGE LatchedMsg; // latched message recieved
} RX = { 0, 0, 0, 0, {0}, {0} };

const uint8 * Manchester_RX(void)
{
	if (!RX.LatchedMsg.Length)
		return NULL;
	return (const uint8 *) &RX.LatchedMsg;
}

void Manchester_RXFlushMessage(void)
{
	RX.LatchedMsg.Length = 0;
}

static void StreamIn(uint8 bit)
{
	// shift bit into our shift register
	RX.ShiftRegister <<= 1;
	RX.ShiftRegister |= (bit & 1);

	// handle bits received
	switch(++RX.State)
	{
	case 1+4: // check for preamble
		if ((RX.ShiftRegister & 0xF) != PREAMBLE)
			RX.State--; // keep waiting until preamble is received
		break;
	case 1+4+8:
		// byte received

		// update the CRC
		if (!RX.Message.Length)
			RX.CRC = CRC8_Reset();
		else
			RX.CRC = CRC8_Update(RX.CRC, RX.LastByte);
		RX.LastByte = RX.ShiftRegister;

		// save byte if there is room in the buffer
		if (RX.Message.Length < sizeof(RX.Message.Buffer.Data))
			RX.Message.Buffer.Data[RX.Message.Length] = RX.ShiftRegister;

		// increase message length by 1
		if (RX.Message.Length != 0xFF)
			RX.Message.Length++;

		// remove byte
		RX.State = 1 + 4;
		break;
	}
}

static void RxTask(void)
{
	static uint8 edge;

	// signal edge detected?
	if (RX_FLAG)
	{
		// read input capture
		uint8 last = edge; // remember time that last edge was captured
		edge = RX_TIMER_IC; // read the time the edge was captured
		
		RX_FLAG_REG = RX_FLAG_BIT; // clear the edge, release latch of input capture lo byte

		// are we already active or is this the first edge?
		if (RX.State)
		{
			// calculate delta from last edge to this edge
			// we should have between 2 and 4 bit times between edges
			uint8 delta = edge - last - BIT_TIME(1.5);
			if (delta < BIT_TIME(2.5 - 1.5))
			{
				// 2 bits
				StreamIn(RX.ShiftRegister); // copy last bit received
				return;
			}
			else if (delta <= BIT_TIME(4.5 - 1.5))
			{
				// 3 or 4 bits
				StreamIn(0); // a zero was received for sure
				if (delta >= BIT_TIME(3.5 - 1.5) || !(RX.ShiftRegister & 2))
					StreamIn(1);
				return;
			}
		}

		// first edge, or invalid bit count
		RX.State = 1; // restart receiver, reset below
	}
	else
	{
		// no edge... are we active?
		if (!RX.State)
			return; // inactive
		
		// check for a stream timeout
		//(void) TCNTLo;
		if ((uint8)(TCNTHi - edge) < BIT_TIME(5))
			return; // not timed out

		// timeout	
		RX.State = 0; // deactivate receiver, reset below
	}

	// if we get here, then we should reset the reciever

	// clear out any partially received messages
	// save message if ALL of the following are TRUE
	// - RX buffer is empty
	// - at least 1 byte (+ crc) was received
	// - message (without crc) fits within buffer
	// - CRC is valid
	if (!RX.LatchedMsg.Length && RX.Message.Length >= 2 && --RX.Message.Length <= sizeof(RX.Message.Buffer) && RX.CRC == RX.LastByte)
	{
		RX.LatchedMsg.Buffer = RX.Message.Buffer; // save message
		RX.LatchedMsg.Length = RX.Message.Length; // save length (without checksum)
	}
	RX.Message.Length = 0; // clear buffer

	// reset the shift register
	RX.ShiftRegister = 0;
}

//////////
//////////
//////////
//////////

// Receiver #2 section

#undef RX
#undef RX_TIMER_IC
#undef RX_FLAG
#undef RX_FLAG_REG
#undef RX_FLAG_BIT
#undef RX_MESSAGE
#undef Manchester_RX
#undef Manchester_RXFlushMessage
#undef StreamIn
#undef RxTask

#define RX                          RX2
#define RX_TIMER_IC                 RX2_TIMER_IC
#define RX_FLAG                     RX2_FLAG
#define RX_FLAG_REG                 RX2_FLAG_REG
#define RX_FLAG_BIT                 RX2_FLAG_BIT
#define RX_MESSAGE                  RX2_MESSAGE
#define Manchester_RX               Manchester_RX2
#define Manchester_RXFlushMessage   Manchester_RX2FlushMessage
#define StreamIn                    Rx2StreamIn
#define RxTask                      Rx2Task

static struct
{
	uint8 State;           // receiver state
	uint8 ShiftRegister;   // holds incoming byte
	uint8 LastByte;        // last byte received
	uint8 CRC;             // running CRC
	RX_MESSAGE Message;    // message being received
	RX_MESSAGE LatchedMsg; // latched message recieved
} RX = { 0, 0, 0, 0, {0}, {0} };

const uint8 * Manchester_RX(void)
{
	if (!RX.LatchedMsg.Length)
		return NULL;
	return (const uint8 *) &RX.LatchedMsg;
}

void Manchester_RXFlushMessage(void)
{
	RX.LatchedMsg.Length = 0;
}

static void StreamIn(uint8 bit)
{
	// shift bit into our shift register
	RX.ShiftRegister <<= 1;
	RX.ShiftRegister |= (bit & 1);

	// handle bits received
	switch(++RX.State)
	{
	case 1+4: // check for preamble
		if ((RX.ShiftRegister & 0xF) != PREAMBLE)
			RX.State--; // keep waiting until preamble is received
		break;
	case 1+4+8:
		// byte received

		// update the CRC
		if (!RX.Message.Length)
			RX.CRC = CRC8_Reset();
		else
			RX.CRC = CRC8_Update(RX.CRC, RX.LastByte);
		RX.LastByte = RX.ShiftRegister;

		// save byte if there is room in the buffer
		if (RX.Message.Length < sizeof(RX.Message.Buffer.Data))
			RX.Message.Buffer.Data[RX.Message.Length] = RX.ShiftRegister;

		// increase message length by 1
		if (RX.Message.Length != 0xFF)
			RX.Message.Length++;

		// remove byte
		RX.State = 1 + 4;
		break;
	}
}

static void RxTask(void)
{
	static uint8 edge;

	// signal edge detected?
	if (RX_FLAG)
	{
		// read input capture
		uint8 last = edge;  // remember time that last edge was captured
		edge = RX_TIMER_IC; // read the time the edge was captured
		
		RX_FLAG_REG = RX_FLAG_BIT; // clear the edge, release latch of input capture lo byte

		// are we already active or is this the first edge?
		if (RX.State)
		{
			// calculate delta from last edge to this edge
			// we should have between 2 and 4 bit times between edges
			uint8 delta = edge - last - BIT_TIME(1.5);
			if (delta < BIT_TIME(2.5 - 1.5))
			{
				// 2 bits
				StreamIn(RX.ShiftRegister); // copy last bit received
				return;
			}
			else if (delta <= BIT_TIME(4.5 - 1.5))
			{
				// 3 or 4 bits
				StreamIn(0); // a zero was received for sure
				if (delta >= BIT_TIME(3.5 - 1.5) || !(RX.ShiftRegister & 2))
					StreamIn(1);
				return;
			}
		}

		// first edge, or invalid bit count
		RX.State = 1; // restart receiver, reset below
	}
	else
	{
		// no edge... are we active?
		if (!RX.State)
			return; // inactive
		
		// check for a stream timeout
		//(void) TCNTLo;
		if ((uint8)(TCNTHi - edge) < BIT_TIME(5))
			return; // not timed out

		// timeout	
		RX.State = 0; // deactivate receiver, reset below
	}

	// if we get here, then we should reset the reciever

	// clear out any partially received messages
	// save message if ALL of the following are TRUE
	// - RX buffer is empty
	// - at least 1 byte (+ crc) was received
	// - message (without crc) fits within buffer
	// - CRC is valid
	if (!RX.LatchedMsg.Length && RX.Message.Length >= 2 && --RX.Message.Length <= sizeof(RX.Message.Buffer) && RX.CRC == RX.LastByte)
	{
		RX.LatchedMsg.Buffer = RX.Message.Buffer; // save message
		RX.LatchedMsg.Length = RX.Message.Length; // save length (without checksum)
	}
	RX.Message.Length = 0; // clear buffer

	// reset the shift register
	RX.ShiftRegister = 0;
}

//////////
//////////
//////////
//////////

void Manchester_Task(void)
{
	// write next outgoing bit
	if (TxLine)
		TP_MANCHESTER_TX_PORT = 1;
	else
		TP_MANCHESTER_TX_PORT = 0;

	// poll RX1
	Rx1Task();

	// poll RX2
	Rx2Task();

	// determine next bit to write
	Manchester_TxTask();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
