// CAN.c
// CAN driver for MC9S12 / Kinetis microcontrollers
// Version 1.20
// (c) 2011, 2012, 2013, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// history
// 1.0  First revision
//      This is for a very simple controller that only Tx's one message
//      Therefore the transmitter portion is especially simple
// 1.1  Added interrupt functionality and message dispatching
// 1.2  Added support for external crystal timing
//      Module defaults to crystal when present, falls back to FBUS
// 1.3  Added support for TX message queue
//      Added support for TX callback functions (callback after successful TX)
// 1.4  Added support for filtering of TX messages with alternate callback (after successful TX)
//      This simulates "reception" of messages we send out, which is useful for gateway modules
// 1.5  Minor change in the way that extended ID messages are handled
// 1.6  Bug fix
// 1.7  Bug fix
// 1.8  Added support for MC9S12P64, MC9S12P96
// 1.9  Made compatible with multithreaded scheduler
// 1.10 Module detects and responds to bus errors
// 1.11 Added TX/RX error counter reporting
// 1.12 Added CAN_TimeSinceLastRx_ms() and CAN_TimeSinceLastTx_ms() to determine if CAN is healthy
// 1.13 Improved TX prioritization scheme to help ensure that messages are transmitted FIFO
//      NOTE: this does not ensure FIFO operation 100% of the time, but is a dramatic improvement
//            over previous driver operation
// 1.14 Tx callbacks now execute in FIFO order
//      Bugfix to polled mode TX
// 1.15 Unified message callback queue, TX and RX callbacks now occur *mostly* in the order that events occur on the bus
//      Old code would burst TX callbacks, then RX callbacks independently.
//      Small bugfix: if caller forgot to set CAN_ID_EXTENDED when using a 29-bit ID, the tx callback also would not have that flag set
// 1.16 Bugfix
// 1.17 Overhauled message buffer structure - now uses a unified buffer for all messages
//      Linked lists are used to manage how the buffers are being used (free/tx/rx)
//      Improve alignment on 32-bit systems: arrays of structs were changed to separate arrays of each element in the struct
//      Runtime improvement: greatly reduced number of buffer copies
//      Memory improvement: big savings on buffer management, efficient use of free space
//      Preliminary support for 32-bit Kinetis micros
// 1.18 Minor changes to improve cross compatibility between MC9S12 and Kinetis microcontroller families
// 1.19 Instrumented with ASSERT() macros
// 1.20 Renamed CAN_TX_MESSAGE() and CAN_RX_MESSAGE() macros to CAN_TX_MESSAGE_FILTER() and CAN_RX_MESSAGE_FILTER()
//      Lint changes

// USAGE
//
// CAN timing parameters
// ---------------------
// The following parameters define the timing of the physical layer, and must be coordinated
// with other controllers on the network
//     #define CAN_BAUD_RATE   250000.0  /* any value allowed as long as hardware supports it */
//     #define CAN_TSEG1       13        /* 4 to 16 time quanta */
//     #define CAN_TSEG2       2         /* 2 to 8 time quanta */
//     #define CAN_SJW         4         /* 1 to 4 time quanta */
//
// CAN driver operation
// --------------------
// The CAN driver can operate in either POLLED or INTERRUPT mode, by defining either:
//     #define CAN_DRIVER_POLLED
//     #define CAN_DRIVER_INTERRUPT
// Polling is done in the background CAN_Task() routine.
// A data buffer is allocated, holding all TX and RX messages.  Tx messages are held
// in this buffer prior to transmit, and received messages are held in this buffer
// until the background program can dispatch them. The max buffer size is defined by
//     #define CAN_BUFFER_SIZE_BYTES  ###
//
// CAN TX/RX message dispatch
// --------------------------
// TX and RX messages are processed via filters, and any matches are passed to appropriate
// callback functions.  The filters must be defined in the following macro:
/*     #define CAN_MESSAGE_FILTERS \                                            */
/*     CAN_?X_MESSAGE_FILTER( ID(id_1), MASK(mask_1), CALLBACK(callback_1) ) \         */
/*     CAN_?X_MESSAGE_FILTER( ID(id_2), MASK(mask_2), CALLBACK(callback_2) ) \         */
/*     ...                                                                      */
/*     CAN_?X_MESSAGE_FILTER( ID(id_n), MASK(mask_n), CALLBACK(callback_n) )           */
// where:
//   CAN_RX_MESSAGE_FILTER specifies an RX filter
//   CAN_TX_MESSAGE_FILTER specifies a TX filter
//   ID(id)      id = the ID of the message that matches the filter
//   MASK(mask)  mask = represents the bits that must match the ID
//                      1 = bit must match,  0 = don't care
//   CALLBACK(callback)  callback = name of a function to call when a filter match occurs
// A filter match is found when the following expression is true
//     filter_match = ((message_id & mask) == id)
//
// CAN TX callback
// ---------------
// CAN TX callback functions are supported, allowing the application to perform specific
// tasks following confirmed successful hardware transmission of a single message
//
// CAN statistics
// ---------------
// The CAN driver can maintain statistics about bytes and messages sent and received.
// This functionality is optional, and is enabled by defining
//     #define CAN_MAINTAIN_STATISTICS
//

#include "global.h"

//#define DEBUG_ENABLE_CAN_LOOPBACK

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// flags for transmitting special kinds of IDs
#define CAN_ID_EXTENDED                    0x80000000
#define CAN_ID_REMOTE_TRANSMISSION_REQUEST 0x40000000 /* data length must be 0 when sending this request */

typedef enum {
	CAN_ERROR_LEVEL_OK = 0, // no error
	CAN_ERROR_LEVEL_1  = 1, // error counter 1-96
	CAN_ERROR_LEVEL_2  = 2, // error counter 97-127
	CAN_ERROR_LEVEL_3  = 3, // error counter 128-255
	CAN_ERROR_LEVEL_BUS_OFF, // bus off condition
} CAN_ERROR_LEVEL;

// CAN TX message buffer
typedef struct
{
	uint8 Length;
	uint32 ID;
	uint8 Data[8];
} CAN_TX_MESSAGE;

// CAN RX message buffer
typedef struct
{
	uint8 Length;
	uint32 ID;
	uint8 Data[8];
	uint16 Timestamp_ms; // holds timestamp of rx message
} CAN_RX_MESSAGE;

// CAN message callback type
typedef void (*CAN_CALLBACK)(const CAN_RX_MESSAGE *);

// management routines
void CAN_Init(void);
void CAN_Task(void);

// interrupt service routines
#ifdef CAN_DRIVER_INTERRUPT
#if (defined __HC12__) && (defined __HIWARE__) && (__MWERKS__ == 1)
#pragma CODE_SEG __NEAR_SEG NON_BANKED
#endif
INTERRUPT void CAN_OnRxInterrupt(void);
INTERRUPT void CAN_OnTxInterrupt(void);
#if (defined __HC12__) && (defined __HIWARE__) && (__MWERKS__ == 1)
#pragma CODE_SEG DEFAULT
#endif
#endif // CAN_DRIVER_INTERRUPT

// returns TRUE if CAN driver is running and there are no errors
// returns FALSE if CAN driver is disabled or in LISTEN mode or there are errors
uint8 CAN_IsEnabled(void);

// returns time since last actual CAN TX/RX, in milliseconds
uint16 CAN_TimeSinceLastTx_ms(void);
uint16 CAN_TimeSinceLastRx_ms(void);

// reports error level of the tx/rx countes
CAN_ERROR_LEVEL CAN_GetRxErrorLevel(void);
CAN_ERROR_LEVEL CAN_GetTxErrorLevel(void);
CAN_ERROR_LEVEL CAN_GetErrorLevel(void);

// RX routines
uint16 CAN_RxBuffersInUse(void);
uint16 CAN_RxBuffersFree(void);
uint8 CAN_RxIsBufferFull(void);

// TX routines
uint16 CAN_TxBuffersInUse(void);
uint16 CAN_TxBuffersFree(void);
uint8 CAN_TxIsBufferFull(void);
uint8 CAN_Tx(const CAN_TX_MESSAGE * message, CAN_CALLBACK callback);

// statistics
#ifdef CAN_MAINTAIN_STATISTICS
uint32 CAN_GetNumBytesSent(void);
uint32 CAN_GetNumBytesReceived(void);
uint32 CAN_GetNumMessagesSent(void);
uint32 CAN_GetNumMessagesReceived(void);
uint16 CAN_GetTxBufferOverflowCount(void);
uint16 CAN_GetRxBufferOverflowCount(void);
uint16 CAN_GetMaxTxQueueCount(void);
uint16 CAN_GetMaxRxQueueCount(void);
uint16 CAN_GetBusErrorCount(void);
uint8 CAN_GetSoftwareFaultCount(void);
#endif // CAN_MAINTAIN_STATISTICS

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// processor check
#if   defined MC9S12G48
#elif defined MC9S12G64
#elif defined MC9S12G96
#elif defined MC9S12G128
#elif defined MC9S12G192
#elif defined MC9S12G240
#elif defined MC9S12GA192
#elif defined MC9S12GA240
#elif defined MC9S12HY32
#elif defined MC9S12HY48
#elif defined MC9S12HY64
#elif defined MC9S12P64
#elif defined MC9S12P96
#elif defined MCU_MKE06Z4    // Kinetis KE06Z series
	// flags for CAN background task notifications
	#define CANBG_TX_ISR_FIRED   0x01
	#define CANBG_RX_ISR_FIRED   0x02
#else
	#error CAN driver not designed for this processor
#endif

// verify CAN timing parameters
#if CAN_TSEG1 < 4 || CAN_TSEG1 > 16
	#error illegal CAN_TSEG1 value
#endif
#if CAN_TSEG2 < 2 || CAN_TSEG2 > 8
	#error illegal CAN_TSEG2 value
#endif
#if CAN_SJW < 1 || CAN_SJW > 4
	#error illegal CAN_SJW value
#endif

// choose a crystal as our timing reference if one exists
#if defined XTAL && XTAL > 0
	#define USE_XTAL
	#define CAN_CLK XTAL
#else
	#undef USE_XTAL
	#define CAN_CLK FBUS
#endif

// bit time = (prescaler value)/(fCANCLK) * (1 + TSEG1 + TSEG2)
// prescaler value = (bit time) * (fCANCLK) / (1 + TSEG1 + TSEG2)
//                 = (1 / baud rate) * (fCANCLK) / (1 + TSEG1 + TSEG2)
//                 = (fCANCLK / baud rate) / (1 + TSEG1 + TSEG2)
#define BAUD_RATE_PRESCALER   ((CAN_CLK / CAN_BAUD_RATE) / (1 + CAN_TSEG1 + CAN_TSEG2))
#if BAUD_RATE_PRESCALER < 1 || BAUD_RATE_PRESCALER > 64
	#error CAN_BAUD_RATE, CAN_CLK, CAN_TSEG1, CAN_TSEG2 combination not possible
#endif

#define ACCEPTANCE_FILTER1         ((uint32)0x00000000)
#define ACCEPTANCE_FILTER1_MASK    ((uint32)0xFFFFFFFF) /* accept all messages */
#define ACCEPTANCE_FILTER2         ((uint32)0xFFFFFFFF)
#define ACCEPTANCE_FILTER2_MASK    ((uint32)0xFFFFFFFF) /* accept all messages */

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined (MCU_MKE06Z4)
#define CALLBACK_SIZE 4 /* 32-bit micro */
#else
#define CALLBACK_SIZE 2 /* 16-bit micro */
#endif // MCU_MKE06Z4

// determine number of buffers needed -- total storage not to exceed CAN_BUFFER_SIZE_BYTES
// 1 len + 4 ID + 8 data + 2 timestamp + callback + 1 Next link
#define NUM_BUFFERS (CAN_BUFFER_SIZE_BYTES / (1 + 4 + 8 + 2 + CALLBACK_SIZE + 1))

// enforce a minimum/maximum number of buffers
#if NUM_BUFFERS < 8
	#undef NUM_BUFFERS
	#define NUM_BUFFERS 8
#elif NUM_BUFFERS > 255
	#undef NUM_BUFFERS
	#define NUM_BUFFERS 255
#endif

// points to an invalid entry in the queue
#define QUEUE_EMPTY 0xFF

// structure for packing 8 CAN data bytes
typedef struct { uint8 Data[8]; } DATA;

// QUEUE structure
// NOTE: Count is not necessary, but is used as a sanity check elsewhere to catch broken queues
typedef struct { uint8 Count, First, Last; } QUEUE;

// message buffers
static uint8 MsgLen[NUM_BUFFERS];          // length of the CAN message
static uint32 MsgID[NUM_BUFFERS];          // ID of the CAN message
static DATA MsgData[NUM_BUFFERS];          // payload of the CAN message
static uint16 Timestamp_ms[NUM_BUFFERS];   // time that message was tx / rx
static CAN_CALLBACK Callback[NUM_BUFFERS]; // callback for message (mainly used for Tx callback)
static uint8 Next[NUM_BUFFERS];            // points to next link in list

// message queues
static QUEUE Free; // free message buffers
static QUEUE Tx;   // message buffers awaiting transmit
static QUEUE InTx; // message buffers in transmit
static QUEUE Rx;   // message buffers received

#pragma INLINE
static void InitializeBuffers(void)
{
	uint8 n;

	EnterCriticalSection();

	// initialize linked list
	for (n=0; n<NUM_BUFFERS; n++)
		Next[n] = n + 1;
	Next[NUM_BUFFERS-1] = QUEUE_EMPTY;

	// all buffers are part of the free list
	Free.Count = NUM_BUFFERS;
	Free.First = 0;
	Free.Last = NUM_BUFFERS - 1;

	// remaining lists are empty
	Tx.Count = InTx.Count = Rx.Count = 0;
	Tx.First = InTx.First = Rx.First = QUEUE_EMPTY;
	Tx.Last = InTx.Last = Rx.Last = QUEUE_EMPTY;

	LeaveCriticalSection();
}

// removes the oldest item in the queue
static uint8 Dequeue(QUEUE * queue)
{
	uint8 index = QUEUE_EMPTY;

	#ifdef CAN_DRIVER_INTERRUPT
	EnterCriticalSection();
	#endif // CAN_DRIVER_INTERRUPT

	if (queue->First < NUM_BUFFERS)
	{
		index = queue->First;
		queue->First = Next[index];
		Next[index] = QUEUE_EMPTY;
		queue->Count--;
		if (queue->Last == index)
		{
			// first and last index the same, was last message, queue is now empty
			queue->Last = queue->First;
			queue->Count = 0;
		}
	}

	#ifdef CAN_DRIVER_INTERRUPT
	LeaveCriticalSection();
	#endif // CAN_DRIVER_INTERRUPT

	return index;
}

// inserts the item at the end of the queue
static void Enqueue(uint8 index, QUEUE * queue)
{
	if (index < NUM_BUFFERS)
	{
		#ifdef CAN_DRIVER_INTERRUPT
		EnterCriticalSection();
		#endif // CAN_DRIVER_INTERRUPT

		Next[index] = QUEUE_EMPTY;

		if (queue->Last < NUM_BUFFERS)
			Next[queue->Last] = index; // add to end of queue
		else
		{
			queue->First = index; // first in queue
			queue->Count = 0;
		}
		queue->Last = index;
		queue->Count++;

		#ifdef CAN_DRIVER_INTERRUPT
		LeaveCriticalSection();
		#endif // CAN_DRIVER_INTERRUPT
	}
}

// re-inserts the item at the front of the queue
// should only be used to undo a Dequeue()
static void Requeue(uint8 index, QUEUE * queue)
{
	if (index < NUM_BUFFERS)
	{
		#ifdef CAN_DRIVER_INTERRUPT
		EnterCriticalSection();
		#endif // CAN_DRIVER_INTERRUPT

		Next[index] = queue->First;
		queue->First = index;
		if (queue->Last >= NUM_BUFFERS)
		{
			queue->Last = index; // first in queue
			queue->Count = 0;
		}
		queue->Count++;

		#ifdef CAN_DRIVER_INTERRUPT
		LeaveCriticalSection();
		#endif // CAN_DRIVER_INTERRUPT
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef CAN_MAINTAIN_STATISTICS

#define STATISTICS(exp) (exp)

static uint32 BytesSent = 0;
static uint32 BytesReceived = 0;
static uint32 MessagesSent = 0;
static uint32 MessagesReceived = 0;
static uint16 TxOverflowCount = 0;
static uint16 RxOverflowCount = 0;
static uint8 MaxTxQueueCount = 0;
static uint8 MaxRxQueueCount = 0;
static uint16 BusErrorCount = 0;
static uint8 SoftwareFaultCount = 0;

uint32 CAN_GetNumBytesSent(void)          { return AtomicRead32(&BytesSent); }
uint32 CAN_GetNumBytesReceived(void)      { return AtomicRead32(&BytesReceived); }
uint32 CAN_GetNumMessagesSent(void)       { return AtomicRead32(&MessagesSent); }
uint32 CAN_GetNumMessagesReceived(void)   { return AtomicRead32(&MessagesReceived); }
uint16 CAN_GetTxBufferOverflowCount(void) { return AtomicRead16(&TxOverflowCount); }
uint16 CAN_GetRxBufferOverflowCount(void) { return AtomicRead16(&RxOverflowCount); }
uint16 CAN_GetMaxTxQueueCount(void)       { return MaxTxQueueCount; }
uint16 CAN_GetMaxRxQueueCount(void)       { return MaxRxQueueCount; }
uint16 CAN_GetBusErrorCount(void)         { return AtomicRead16(&BusErrorCount); }
uint8 CAN_GetSoftwareFaultCount(void)     { return SoftwareFaultCount; }

#else // CAN_MAINTAIN_STATISTICS

#define STATISTICS(exp)

#endif // CAN_MAINTAIN_STATISTICS

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// bus error detection

static uint16 LastTxTime = 0;
static uint16 LastRxTime = 0;

#define LISTEN_MODE        CANCTL1_LISTEN
#define RX_ERROR_MODE      (CANRFLG_RSTAT >= 2)
#define TX_BUS_OFF_MODE    (CANRFLG_TSTAT == 3)
//#define ANY_ACTIVE_ERRORS  (RX_ERROR_MODE || TX_BUS_OFF_MODE)   this doesn't work, needs debugging
#define ANY_ACTIVE_ERRORS  CANMISC_BOHOLD

#pragma INLINE
uint8 CAN_IsEnabled(void)
{
	return !LISTEN_MODE && !ANY_ACTIVE_ERRORS;
}

uint16 CAN_TimeSinceLastTx_ms(void)
{
	uint16 now = OS_GetElapsedMilliseconds16();
	uint16 delta = now - LastTxTime;
	if (delta < 60000)
		return delta;
	LastTxTime = (uint16)(now - 60000);
	return 60000;
}

uint16 CAN_TimeSinceLastRx_ms(void)
{
	uint16 now = OS_GetElapsedMilliseconds16();
	uint16 delta = now - LastRxTime;
	if (delta < 60000)
		return delta;
	LastRxTime = (uint16)(now - 60000);
	return 60000;
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

static void EnterInitializationMode(void)
{
	// if we aren't in init mode
	if (!CANCTL1_INITAK)
	{
		// we need to enter sleep mode first
		// which avoids causing bus errors upon entering initialization mode
		_FEED_COP();
		CANCTL0_SLPRQ = 1; // request sleep mode
		Wait(1000); // wait 1 millisecond
//		while (!CANCTL1_SLPAK) // wait for sleep ack
//			;
	}

	// enter initialization mode
	_FEED_COP();
	CANCTL0_INITRQ = 1;      // request initialization mode
	while (!CANCTL1_INITAK)  // wait for initialization mode ack
		;
}

static void ExitInitializationMode(void)
{
	CANCTL0_SLPRQ = 0; // exit sleep mode

	// exit initialization mode
	_FEED_COP();
	CANCTL0_INITRQ = 0;    // request exit initialization mode
	while (CANCTL1_INITAK) // wait for initialization mode exit ack
		;
}

void CAN_Init(void)
{
	#ifdef CAN_STANDBY
	Assert_CAN_STANDBY;
	#endif

	// enable CAN module
	#ifdef MCU_MKE06Z4
	SIM_PINSEL1 |= SIM_PINSEL1_MSCANPS_MASK;  // CAN_TX on PTE7, CAN_RX on PTH2
	SIM_SCGC |= (SIM_SCGC_MSCAN_MASK | SIM_SCGC_FTM0_MASK);  // enable clocks to MSCAN
	#ifdef CAN_DRIVER_INTERRUPT
    /* NVIC_IPR7: PRI_31=1,PRI_30=1 */
    NVIC_IPR7 = (uint32)((NVIC_IPR7 & (uint32)~(uint32)(NVIC_IP_PRI_31(0x02) | NVIC_IP_PRI_30(0x02))) | (uint32)(NVIC_IP_PRI_31(0x01) | NVIC_IP_PRI_30(0x01)));
    NVIC_ISER |= NVIC_ISER_SETENA31_MASK | NVIC_ISER_SETENA30_MASK;
	#else
    NVIC_ICER |= NVIC_ISER_SETENA31_MASK | NVIC_ISER_SETENA30_MASK;
	#endif // CAN_DRIVER_INTERRUPT
	#endif // MCU_MKE06Z4
	CANCTL1_CANE = 1;

	EnterInitializationMode();

	// reset buffers
	InitializeBuffers();

	// Now able to modify registers/bits which are only writtable in initialization mode
	// this includes: CANCTL1 CANBTR0 CANBTR1 CANIDAC CANIDAR0-7 CANIDMR0-7

	// CANCTL1 CAN Control Register 1
	// 1?0010xx
	// ||||||||
	// ||||||| \__ INITAK 0 = normal operation, 1 = initialization mode active (handshake flag)
	// |||||| \___ SLPAK  0 = normal operation, 1 = sleep mode active (handshake flag)
	// ||||| \____ WUPM   0 = wakeup low pass disabled, 1 = wakeup low pass enabled
	// |||| \_____ BORM   1 = bus off recovery upon user request, 0 = automatic bus off recovery
	// ||| \______ LISTEN 0 = normal operation, 1 = listen only mode
	// || \_______ LOOPB  0 = loopback disabled, 1 = loopback  enabled
	// | \________ CLKSRC 0 = oscillator clock is source clock, 1 = bus clock is source clock
	//  \_________ CANE   1 = CAN module enabled, 0 = disabled
	#ifdef USE_XTAL
	CANCTL1 = 0x88;
	#else
	CANCTL1 = 0xC8;
	#endif
	#ifdef DEBUG_ENABLE_CAN_LOOPBACK
	CANCTL1_LOOPB = 1;
	#endif

	// CANBTR0 CAN Bus Timing Register 0
	// xxxxxxxx
	// ||||||||
	// || \\\\\\__ BRP[5:0] 00000: baud rate prescaler = 1
	//  \\________ SJW[1:0] 00: Synchronization Jump Width = 1 time quanta
	CANBTR0_BRP = (uint8)(BAUD_RATE_PRESCALER - 1);
	CANBTR0_SJW = (uint8)((CAN_SJW - 1) & 0x3);

	// CANBTR1 CAN Bus Timing Register 1
	// xxxxxxxx
	// ||||||||
	// |||| \\\\__ TSEG1[3:0] 0000: TSEG1 = 1 time quanta
	// | \\\\_____ TSEG2[2:0] 000:  TSEG2 = 1 time quanta
	//  \_________ SAMP       0 = one sample per bit, 1 = 3 samples per bit
	CANBTR1_TSEG_10 = (uint8)((CAN_TSEG1 - 1) & 0xF);
	CANBTR1_TSEG_20 = (uint8)((CAN_TSEG2 - 1) & 0x7);
	CANBTR1_SAMP = 0;

	// CANIDAC CAN ID Acceptance Control Register
	// xx00xxxx
	//   || |||
	//   ||  \\\__ IDHIT[2:0] indicates which ID acceptance filter was hit
	//    \\______ IDAM[1:0]  00 = two 32-bit ID acceptance filters (used for 29-bit extended CAN(J1939) IDs plus CAN's SRR, IDE and RTR bits)
	CANIDAC = 0x00;

	// CANMISC CAN Miscellaneous Register
	// xxxxxxx1
	//        |
	//         \__ BOHOLD 0 = not in bus off or recovery has been requested by user, 1 = bus off and hold
	CANMISC = 0x00; // need to enter bus on state

	// CAN ID Acceptance Registers
	#pragma MESSAGE DISABLE C2705
	/*lint -save -e572 -e835 lint doesn't like shifting a 0 value */
	CANIDAR0 = (uint8)(ACCEPTANCE_FILTER1 >> 24); // CANIDAR0-CANIDAR4 used for 1st 29-bit CAN ID
	CANIDAR1 = (uint8)(ACCEPTANCE_FILTER1 >> 16);
	CANIDAR2 = (uint8)(ACCEPTANCE_FILTER1 >> 8);
	CANIDAR3 = (uint8)(ACCEPTANCE_FILTER1 >> 0);
	CANIDAR4 = (uint8)(ACCEPTANCE_FILTER2 >> 24); // CANIDAR4-CANIDAR6 used for 2nd 29-bit CAN ID
	CANIDAR5 = (uint8)(ACCEPTANCE_FILTER2 >> 16);
	CANIDAR6 = (uint8)(ACCEPTANCE_FILTER2 >> 8);
	CANIDAR7 = (uint8)(ACCEPTANCE_FILTER2 >> 0);
 	/*lint -restore */
 	#pragma MESSAGE DEFAULT C2705

	// CAN ID Mask Registers (0 = must match, 1 = don't care)
	#pragma MESSAGE DISABLE C2705
	/*lint -save -e572 -e835 lint doesn't like shifting a 0 value */
	CANIDMR0 = (uint8)(ACCEPTANCE_FILTER1_MASK >> 24); // CANIDMR0-CANIDMR4 used for 1st 29-bit CAN ID
	CANIDMR1 = (uint8)(ACCEPTANCE_FILTER1_MASK >> 16);
	CANIDMR2 = (uint8)(ACCEPTANCE_FILTER1_MASK >> 8);
	CANIDMR3 = (uint8)(ACCEPTANCE_FILTER1_MASK >> 0);
	CANIDMR4 = (uint8)(ACCEPTANCE_FILTER2_MASK >> 24); // CANIDMR4-CANIDMR7 used for 2nd 29-bit CAN ID
	CANIDMR5 = (uint8)(ACCEPTANCE_FILTER2_MASK >> 16);
	CANIDMR6 = (uint8)(ACCEPTANCE_FILTER2_MASK >> 8);
	CANIDMR7 = (uint8)(ACCEPTANCE_FILTER2_MASK >> 0);
 	/*lint -restore */
	#pragma MESSAGE DEFAULT C2705

	// take transceiver out of standby
	#ifdef CAN_STANDBY
	Deassert_CAN_STANDBY;
	#endif

	ExitInitializationMode();

	// Now modify registers/bits which are only writable when not in initialization mode or writable any time

	// CANCTL0 CAN Control Register 0
	// 11010000
	// ||||||||
	// ||||||| \__ INITRQ 0 = normal operation, 1 = initialize mode active
	// |||||| \___ SLPRQ  0 = normal operation, 1 = sleep mode requested (enter sleep when bus is idle)
	// ||||| \____ WUPE   0 = wakeup disabled, 1 = wakeup from sleep or power down modes enabled
	// |||| \_____ TIME   0 = disable Rx time stamp, 1 = enable time Rx time stamp
	// ||| \______ SYNCH  0 = not synced to bus, 1 = synced to bus (flag)
	// || \_______ CSWAI  0 = CAN active in wait mode, 1 = CAN stops in wait mode
	// | \________ RXACT  0 = transmitting or idle, 1 = Rx'ing a message (flag)
	//  \_________ RXFRM  Rx'd flag (clear by writing 1)
	CANCTL0 = 0xD0;

	// CAN Receiver Flag Register
	CANRFLG = 0xC3;   // clear all Rx flags

	// CANRIER CAN Receiver Interrupt Enable Register
	// 00000011
	// ||||||||
	// ||||||| \__ RXFIE        0 = Rx buffer full interrupt disabled, 1 = Rx buffer full interrupt enabled
	// |||||| \___ OVRIE        0 = overrun interrupt disabled, 1 = overrun interrupt enabled (causes an error interrupt request)
	// |||| \\____ TSTATE[1:0]  00 = do not generate CSCIF interrupt caused by transmitter state changes
	// || \\______ RSTATE[1:0]  00 = do not generate CSCIF interrupt caused by receiver state changes
	// | \________ CSCIE        0 = CAN status change interrupt disabled, 1 = CAN status change interrupt enabled (causes an error interrupt request)
	//  \_________ WUPIE        0 = wakeup interrupt disabled, 1 = wakeup interrupt enabled
	CANRIER = 0x00;

	// CANTIER CAN Transmitter Interrupt Enable Register
	// xxxxx000
	//      |||
	//      || \__ TXEIE0 0 = Tx buffer 0 empty interrupt disabled, 1 = Tx buffer 0 empty interrupt enabled (causes a CAN Tx interrupt)
	//      | \___ TXEIE1 1 = Tx buffer 1 empty interrupt disabled, 1 = Tx buffer 1 empty interrupt enabled (causes a CAN Tx interrupt)
	//       \____ TXEIE2 2 = Tx buffer 2 empty interrupt disabled, 1 = Tx buffer 2 empty interrupt enabled (causes a CAN Tx interrupt)
	CANTIER = 0x00;

	// CANTARQ CAN Transmitter Message Abort Request Register
	// xxxxx111
	//      |||
	//      || \__ ABTRQ0 0 = no abort requested, 1 = abort Tx buffer 0 transmission
	//      | \___ ABTRQ1 0 = no abort requested, 1 = abort Tx buffer 1 transmission
	//       \____ ABTRQ2 0 = no abort requested, 1 = abort Tx buffer 2 transmission
	CANTARQ = 0x07;

	#ifdef CAN_DRIVER_INTERRUPT
	CANRIER_RXFIE = 1;
	#endif
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// helper functions for converting between CAN ID format and CAN peripheral IDR register

// converts a CAN ID to IDR format used by CAN cell
#pragma INLINE
static uint32 ID_to_IDR(uint32 id)
{
	DWORD idr;
	uint8 temp;

	// convert message ID into the format that the transmission hardware requires
	if (id & (CAN_ID_EXTENDED | 0x1FFFF800))
	{
		// 29-bit message identifier

		idr.ui32 = id << 1;                // --876543 21098765 43210987 6543220-
		temp = idr.word.hi.ui8.lo & 0x7;   //               ^^^
		idr.ui16.hi <<= 2;                 // 87654321 098765-- 43210987 6543220-
		idr.word.hi.ui8.lo &= 0xF8;        // xxxxxxxx xxxxx000 xxxxxxxx xxxxxxx0
		idr.word.hi.ui8.lo |= 0x18;        // xxxxxxxx xxx11000 xxxxxxxx xxxxxxx0
		idr.word.hi.ui8.lo |= temp;        // xxxxxxxx xxxxx765 xxxxxxxx xxxxxxx0
		if (id & CAN_ID_REMOTE_TRANSMISSION_REQUEST)
			idr.word.lo.ui8.lo++;
	}
	else
	{
		// 11-bit message identifier
		idr.ui16.hi = (uint16)id << 5;
		idr.ui16.lo = 0;
		if (id & CAN_ID_REMOTE_TRANSMISSION_REQUEST)
			idr.word.hi.ui8.lo |= 0x10;
	}

	#if defined (MCU_MKE06Z4)
	// cortex m0 is little endian, so bytes must be reversed
	asm("rev %[swap], %[swap]" : [swap] "=r" (idr.ui32) : "0" (idr.ui32));
	#endif // MCU_MKE06Z4

	return idr.ui32;
}

// converts the IDR format used by the CAN cell back to a CAN ID
#pragma INLINE
static uint32 IDR_to_ID(uint32 _idr)
{
	#define idr (*(DWORD*)(&_idr))
	DWORD id;

	#if defined (MCU_MKE06Z4)
	// cortex m0 is little endian, so bytes must be reversed
	asm("rev %[swap], %[swap]" : [swap] "=r" (_idr) : "0" (_idr));
	#endif // MCU_MKE06Z4

	// convert received ID to something readable
	if (idr.ui32 & 0x00080000)
	{
		// 29-bit identifier
		// xxxxxxxx xxx11xxx xxxxxxxx xxxxxxxx
		// |||||||| |||  ||| |||||||| ||||||||
		// |||||||| |||  ||| |||||||| ||||||| \__ remote transmission request
		//  \\\\\\\\_\\\__\\\_\\\\\\\\_\\\\\\\___ 29 bit identifier
		id.ui32 = (idr.ui32 >> 1); // -------- ------GG GGGGGGGG GGGGGGGG
		id.ui16.hi &= 0x0003;
		id.ui16.hi |= ((idr.ui16.hi >> 3) & 0xFFFC);
		id.ui32 |= CAN_ID_EXTENDED;
		if (idr.word.lo.ui8.lo & 0x01)
			id.ui32 |= CAN_ID_REMOTE_TRANSMISSION_REQUEST;
	}
	else
	{
		// 11-bit identifier
		// xxxxxxxx xxxx0--- -------- --------
		// |||||||| ||||
		// |||||||| ||| \________________________ remote transmission request
		//  \\\\\\\\_\\\_________________________ 11 bit identifier
		id.ui32 = idr.ui16.hi >> 5;
		if (idr.ui32 & 0x00100000)
			id.ui32 |= CAN_ID_REMOTE_TRANSMISSION_REQUEST;
	}
	return id.ui32;
	#undef idr
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// message dispatching via user defined filters

// dispatch transmitted messages to the appropriate callbacks
#undef ID
#undef MASK
#undef CALLBACK
#undef CAN_TX_MESSAGE_FILTER
#undef CAN_RX_MESSAGE_FILTER
#define ID(id)             (id)
#define MASK(mask)         (mask)
#define CALLBACK(callback) (callback)
#define CAN_TX_MESSAGE_FILTER(id, mask, callback) TRUE |
#define CAN_RX_MESSAGE_FILTER(id, mask, callback)
#if CAN_MESSAGE_FILTERS FALSE
	#pragma MESSAGE DISABLE C4000
	#pragma MESSAGE DISABLE C5703 // message may not be used if no TX callbacks
	#pragma INLINE
	static void DispatchTxMessage(const CAN_RX_MESSAGE * message)
	{
		ASSERT(message->Length <= 8);

		// check filters and execute appropriate callbacks
		#undef ID
		#undef MASK
		#undef CALLBACK
		#undef CAN_TX_MESSAGE_FILTER
		#undef CAN_RX_MESSAGE_FILTER
		#define ID(id)             (id)
		#define MASK(mask)         (mask)
		#define CALLBACK(callback) (callback)
		#define CAN_TX_MESSAGE_FILTER(id, mask, callback) if ((message->ID & (mask)) == (id)) callback(message);
		#define CAN_RX_MESSAGE_FILTER(id, mask, callback)
		CAN_MESSAGE_FILTERS
	}
	#pragma MESSAGE DEFAULT C4000
	#pragma MESSAGE DEFAULT C5703
#else
	#define DispatchTxMessage(arg)
#endif


// dispatch received messages to the appropriate callbacks
#pragma MESSAGE DISABLE C4000
/*lint -save -e587 -e774 -e835 Depending on how masks are setup, they may always evaluate to TRUE or FALSE and contain zeroes */
#pragma INLINE
static void DispatchRxMessage(const CAN_RX_MESSAGE * message)
{
	ASSERT(message->Length <= 8);

	// check filters and execute appropriate callbacks
	#undef ID
	#undef MASK
	#undef CALLBACK
	#undef CAN_TX_MESSAGE_FILTER
	#undef CAN_RX_MESSAGE_FILTER
	#define ID(id)             (id)
	#define MASK(mask)         (mask)
	#define CALLBACK(callback) (callback)
	#define CAN_TX_MESSAGE_FILTER(id, mask, callback)
	#define CAN_RX_MESSAGE_FILTER(id, mask, callback) if ((message->ID & (mask)) == (id)) callback(message);
	CAN_MESSAGE_FILTERS
}
#pragma MESSAGE DEFAULT C4000
/*lint -restore */

// dispatches all enqueued RX messages to callbacks
#pragma INLINE
static void DispatchMessages(void)
{
	// dispatch all messages received to their destination
	for (;;)
	{
		CAN_RX_MESSAGE message;
		CAN_CALLBACK callback;
		uint8 index;

		// any messages in Rx queue?
		index = Dequeue(&Rx);
		if (index >= NUM_BUFFERS)
			return;

		// unload the message from the queue into a local struct
		message.Length = MsgLen[index];
		ASSERT(message.Length <= 8);
		message.ID = MsgID[index];
		*(DATA*)(&message.Data[0]) = MsgData[index];
		message.Timestamp_ms = Timestamp_ms[index];
		callback = Callback[index];

		// return buffer to Free queue
		Enqueue(index, &Free);

		// is this an Rx or Tx message?
		if (callback == DispatchRxMessage)
		{
			// RX message
			DispatchRxMessage(&message);
		}
		else
		{
			// TX message (echo)

			// execute the tx callback, if any
			if (callback != NULL)
				callback(&message);

			// and dispatch
			DispatchTxMessage(&message);
		}
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// RECEIVER SECTION

#pragma INLINE
uint16 CAN_RxBuffersInUse(void)
{
	return Rx.Count;
}

#pragma INLINE
uint16 CAN_RxBuffersFree(void)
{
	return Free.Count;
}

#pragma INLINE
uint8 CAN_RxIsBufferFull(void)
{
	return CAN_RxBuffersFree() == 0;
}

#pragma MESSAGE DISABLE C4000 // condition always TRUE
#pragma INLINE
static void CANCellUnloadRx(void)
{
	if (CANRFLG_RXF)
	{
		LastRxTime = OS_GetElapsedMilliseconds16();

		do
		{
			uint32 canid;

			STATISTICS(MessagesReceived++);
			ASSERT(CANRXDLR_DLC <= 8);
			STATISTICS(BytesReceived += (uint8)CANRXDLR_DLC);

			// get the message ID
			canid = IDR_to_ID(*(uint32*)(&CANRXIDR0)); /*lint !e826 CANRXIDR0 is the first byte of an array of 4 bytes.  This pointer cast is safe */

			// check to see if the ID matches our message filters
			// we simply toss any messages we don't care about
			/*lint -save -e506 -e587 -e773 -e774 -e835 We can't parenthesize this macro due to the way we need the || to carry over. Also
			                                           parts below will always be TRUE/FALSE and may contain zeroes */

			#undef ID
			#undef MASK
			#undef CAN_TX_MESSAGE_FILTER
			#undef CAN_RX_MESSAGE_FILTER
			#define ID(id)             (id)
			#define MASK(mask)         (mask)
			#define CAN_TX_MESSAGE_FILTER(id, mask, callback)
			#define CAN_RX_MESSAGE_FILTER(id, mask, callback)  (canid & mask) == id ||
			if (CAN_MESSAGE_FILTERS FALSE)
			{
				// we want to keep the message

				// get a free buffer for RX
				uint8 index = Dequeue(&Free);

				if (index < NUM_BUFFERS)
				{
					// save the message
					MsgLen[index] = (uint8)CANRXDLR_DLC;
					ASSERT(MsgLen[index] <= 8);
					MsgID[index] = canid;
					MsgData[index] = *(DATA*)(&CANRXDSR_ARR[0]); /*lint !e826 CANRXDSR_ARR is an array of 8 bytes.  This pointer cast is safe */
					Timestamp_ms[index] = LastRxTime;
					Callback[index] = DispatchRxMessage; // this flags the message as an Rx message
					Enqueue(index, &Rx);

					#ifdef CAN_MAINTAIN_STATISTICS
					if (MaxRxQueueCount < Rx.Count)
						MaxRxQueueCount = Rx.Count;
					#endif // CAN_MAINTAIN_STATISTICS
				}
				else
					STATISTICS(RxOverflowCount++); // no space available
			}
			/*lint -restore */

			// clear Rx flag, allowing next message from FIFO buffer to be shifted into the foreground buffer
			CANRFLG_RXF = 1;
		} while (CANRFLG_RXF);
	}

	// check for overruns
	if (CANRFLG_OVRIF)
	{
		CANRFLG_OVRIF = 1;
		STATISTICS(RxOverflowCount++);
	}
}
#pragma MESSAGE DEFAULT C4000


#ifdef CAN_DRIVER_INTERRUPT

#pragma MESSAGE DISABLE C4000 /* condition always true */
#pragma MESSAGE DISABLE C4001 /* condition always false */
#ifdef MCU_MKE06Z4
void CAN_OnRxInterrupt(void)
#else
#pragma CODE_SEG __NEAR_SEG NON_BANKED
INTERRUPT void CAN_OnRxInterrupt(void)
#endif // MCU_MKE06Z4
{
	#ifdef DEBUG_CAN_RX_INTERRUPT_TASK_PORT
	DEBUG_CAN_RX_INTERRUPT_TASK_PORT = 1;
	#endif

	CANCellUnloadRx();

	#ifdef DEBUG_CAN_RX_INTERRUPT_TASK_PORT
	DEBUG_CAN_RX_INTERRUPT_TASK_PORT = 0;
	#endif

#ifdef MCU_MKE06Z4
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	TaskHandle_t CANBgTask = xAppCANGetBgTaskHandle();

	if (CANBgTask)
	{
		// Wake the CAN background task
		xTaskNotifyFromISR( CANBgTask,
							CANBG_RX_ISR_FIRED,
							eSetBits,
							&xHigherPriorityTaskWoken );
	}

	portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
#endif // MCU_MKE06Z4
}
#pragma CODE_SEG DEFAULT
#pragma MESSAGE DEFAULT C4000
#pragma MESSAGE DEFAULT C4001

#endif // CAN_DRIVER_INTERRUPT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// TRANSMITTER SECTION

// hardware buffer info
// messages held in hardware buffers are remembered here for later tx callback processing
static uint8 HardwareTxFlags = 0x00; // same bits as CANTFLG, when set indicates a message was queued for transmit

// used to ensure messages are transmitted FIFO, lowest number wins priority scheme
static uint8 TxPriority = 0;

#pragma INLINE
uint16 CAN_TxBuffersInUse(void)
{
	return Tx.Count;
}

#pragma INLINE
uint16 CAN_TxBuffersFree(void)
{
	// don't let the Tx buffer hold more than 1/2 of the available buffers
	uint16 txfree = (NUM_BUFFERS / 2) - Tx.Count;
	if (txfree > (NUM_BUFFERS / 2))
		return 0; // negative number wrap around, no free buffers
	return (txfree < Free.Count) ? txfree : Free.Count;
}

#pragma INLINE
uint8 CAN_TxIsBufferFull(void)
{
	return CAN_TxBuffersFree() == 0;
}

// handles the oldest finished transmission
static void HandleFinishedTransmission(void)
{
	uint8 index = Dequeue(&InTx);
	if (index < NUM_BUFFERS)
	{
		STATISTICS(MessagesSent++);
		STATISTICS(BytesSent += MsgLen[index]);
		MsgID[index] = IDR_to_ID(MsgID[index]); // convert from CAN cell IDR format back to CAN ID format
		Timestamp_ms[index] = LastTxTime;
		Enqueue(index, &Rx); // enqueue for background dispatch
	}
}

// check for sucessfully transmitted messages, and queue
// them for background callback dispatching
// MUST be called within a critical section
static void HandleFinishedTransmissions(void)
{
	// check if any transmissions have finished
	// flags are set for any hardware buffers that we queued for transmit, and are now empty
	uint8 finished = HardwareTxFlags & CANTFLG;
	if (finished)
	{
		// clear the buffers we are handling
		HardwareTxFlags &= ~finished;

		// remember the last time messages were transmitted
		LastTxTime = OS_GetElapsedMilliseconds16();

		// handle finished transmissions as they are found
		// NOTE: this only works because TX messages are sent FIFO by CAN cell, the same order as in InTx
		if (finished & 1) HandleFinishedTransmission();
		if (finished & 2) HandleFinishedTransmission();
		if (finished & 4) HandleFinishedTransmission();
	}
}

// must be called from within a critical section
// if TRUE is returned, then the message was enqueued for transmit and was placed in the InTx queue
static uint8 TransmitNow(uint8 index)
{
	// range check
	if (index >= NUM_BUFFERS)
		return 0; // huh?

	// select the first empty transmit buffer
	CANTBSEL = CANTFLG; // hardware automatically selects the buffer of the lowest order bit set

	// did we get a buffer?
	if (!CANTBSEL)
		return 0;

	// we have a buffer

	// before we use the CAN buffer, post-handle any recent transmissions that were using that buffer
	HandleFinishedTransmissions();

	// determine transmit priority scheme
	// ensure messages go out FIFO whenever possible
	// the CANTXTBPR sets outgoing mesage priority (lowest value is highest priority)
	// when multiple buffers have same CANTXTBPR, the lowest indexed buffer wins prioritiziation
	if (CANTBSEL <= HardwareTxFlags)
	{
		// the selected buffer is a lower index than another buffer awaiting transmit
		// therefore, the selected buffer will take precedence, unless we decrease it's priority
		// so we need to increment the TxPriority register, decreasing the priority
		if (TxPriority == 0xFF)
			return 0; // priority can't decrease any more, we need to wait until interrupt catches all buffers are empty and resets TxPriority
		TxPriority++; // decrease priority
	}

	// copy message into buffer
	CANTXTBPR = TxPriority; // set message priority
	CANTXDLR = MsgLen[index]; // set message length
	// ID was pre-converted to IDR format earlier
	*(uint32*)(&CANTXIDR0) = MsgID[index]; /*lint !e826 CANTXIDR0 is the first byte of an array of 4 bytes. This pointer cast is safe */
	// set message data
	*(DATA*)(&CANTXDSR_ARR[0]) = MsgData[index]; /*lint !e826 CANTXDSR_ARR is an array of 8 bytes.  This pointer cast is safe */

	// clearing the selected buffer's transmit flag schedules transmission
	CANTFLG = CANTBSEL; // writing a 1 clears the flag

	// remember the buffer we are using
	HardwareTxFlags |= CANTBSEL;

	// put the buffer we just used into the InTx queue so we can recover the message later
	Enqueue(index, &InTx);

	// interrupt us when the transmission completes
	#ifdef CAN_DRIVER_INTERRUPT
	CANTIER |= CANTBSEL;
	#endif

	return TRUE;
}

// transmit a CAN message
uint8 CAN_Tx(const CAN_TX_MESSAGE * message, CAN_CALLBACK callback)
{
	uint8 result;
	uint8 index;

	ASSERT(message->Length <= 8);

	// don't let the user transmit if we are not enabled
	if (!CAN_IsEnabled())
		return FALSE;

	// get a free message buffer to store the message in
	index = (CAN_TxBuffersFree() ? Dequeue(&Free) : QUEUE_EMPTY);
	if (index >= NUM_BUFFERS)
	{
		STATISTICS(TxOverflowCount++);
		return FALSE;// no space available
	}

	// store the message in the queue
	MsgLen[index] = message->Length;
	MsgID[index] = ID_to_IDR(message->ID); // pre convert to CAN cell IDR format
	MsgData[index] = *(DATA*)(&(message->Data[0]));
	Callback[index] = callback;

	EnterCriticalSection();

	// if no other messages are pending, then attempt to transmit the message immediately
	result = ((Tx.Count == 0) ? TransmitNow(index) : FALSE);

	// did we transmit?
	if (!result)
	{
		// unable to transmit right now
		// we are guaranteed that a TX empty interrupt is pending
		// need to enqueue for later transmission
		Enqueue(index, &Tx);

		#ifdef CAN_MAINTAIN_STATISTICS
		if (MaxTxQueueCount < Tx.Count)
			MaxTxQueueCount = Tx.Count;
		#endif // CAN_MAINTAIN_STATISTICS
	}

	LeaveCriticalSection();

	// return TRUE, as we either started transmission, or enqueued a later transmission
	return TRUE;
}

#pragma INLINE
static void CANCellLoadTx(void)
{
	// unload any finished transmission buffers
	HandleFinishedTransmissions();

	// reset tx priority whenever all buffers are empty
	if (CANTFLG == 7)
		TxPriority = 0;

	// transmit any/all pending messages while there are empty TX buffers
	while (CANTFLG)
	{
		uint8 index = Dequeue(&Tx);
		if (index >= NUM_BUFFERS)
			break;
		if (!TransmitNow(index))
		{
			Requeue(index, &Tx); // need to re-queue the message at the front of the queue
			break;
		}
	}
}

#ifdef CAN_DRIVER_INTERRUPT
#ifdef MCU_MKE06Z4 // Don't care about memory map on Kinetis parts
void CAN_OnTxInterrupt(void)
#else
#pragma CODE_SEG __NEAR_SEG NON_BANKED
INTERRUPT void CAN_OnTxInterrupt(void)
#endif // MCU_MKE06Z4
{
#ifdef DEBUG_CAN_TX_INTERRUPT_TASK_PORT
	DEBUG_CAN_TX_INTERRUPT_TASK_PORT = 1;
#endif

	// we got here because one or more buffers are empty
	// so we can safely disable interrupts on any empty buffers
	CANTIER = ~CANTFLG;

	CANCellLoadTx();

#ifdef DEBUG_CAN_TX_INTERRUPT_TASK_PORT
	DEBUG_CAN_TX_INTERRUPT_TASK_PORT = 0;
#endif

#ifdef MCU_MKE06Z4
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	TaskHandle_t CANBgTask = xAppCANGetBgTaskHandle();

	if (CANBgTask)
	{
		// Wake the CAN background task
		xTaskNotifyFromISR( CANBgTask,
							CANBG_TX_ISR_FIRED,
							eSetBits,
							&xHigherPriorityTaskWoken );
	}

	portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
#endif // MCU_MKE06Z4

}
#pragma CODE_SEG DEFAULT
#endif // CAN_DRIVER_INTERRUPT

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// bus error detection and handling

CAN_ERROR_LEVEL CAN_GetRxErrorLevel(void)
{
	switch (CANRFLG_RSTAT)
	{
	case 0: return CAN_ERROR_LEVEL_OK; // actually this might be CAN_ERROR_LEVEL_1 but we can't tell the difference
	case 1: return CAN_ERROR_LEVEL_2;
	case 2: return CAN_ERROR_LEVEL_3;
	case 3:
	default: return CAN_ERROR_LEVEL_BUS_OFF;
	}
}

CAN_ERROR_LEVEL CAN_GetTxErrorLevel(void)
{
	switch (CANRFLG_TSTAT)
	{
	case 0: return CAN_ERROR_LEVEL_OK; // actually this might be CAN_ERROR_LEVEL_1 but we can't tell the difference
	case 1: return CAN_ERROR_LEVEL_2;
	case 2: return CAN_ERROR_LEVEL_3;
	case 3:
	default: return CAN_ERROR_LEVEL_BUS_OFF;
	}
}

CAN_ERROR_LEVEL CAN_GetErrorLevel(void)
{
	CAN_ERROR_LEVEL rx = CAN_GetRxErrorLevel();
	CAN_ERROR_LEVEL tx = CAN_GetTxErrorLevel();
	return (rx > tx) ? rx : tx;
}

// return TRUE if there is an error preventing us from running normally
// return FALSE if we can run normally (OK)
static uint8 BusErrorBackgroundTask(void)
{
	static uint16 Timer = 0;

	// is the bus in listen mode?
	if (!LISTEN_MODE)
	{
		// bus is running normally
		// check for BUS_OFF condition
		if (!ANY_ACTIVE_ERRORS)
		{
			Timer = OS_GetElapsedMilliseconds16();
			return FALSE;
		}

		// wait 1 second
		if ((uint16)(OS_GetElapsedMilliseconds16() - Timer) > 1000)
		{
			// disable interrupts before entering LISTEN mode
			#ifdef CAN_DRIVER_INTERRUPT
			CANRIER_RXFIE = 0;
			CANTIER = 0;
			#endif

			// enter listen mode
			EnterInitializationMode(); // this clears most registers, RX and TX pending, etc
			LISTEN_MODE = 1;
			ExitInitializationMode();

			#ifdef CAN_MAINTAIN_STATISTICS
			if (BusErrorCount != 0xFFFF)
				BusErrorCount++;
			#endif // CAN_MAINTAIN_STATISTICS

			// flush the queues
			InitializeBuffers();
		}
	}
	else
	{
		// bus is listening mode

		// acknowledge bus off events
		if (CANMISC_BOHOLD)
			CANMISC_BOHOLD = 1; // clear bus off condition

		// check for reception of message
		// very unlikely that the system can recover, but if it can (maybe terminator problem or similar?)
		// then we will attempt to go back online
		// most modules will go to listen mode and stay there
		if (CANRFLG_RXF || (uint16)(OS_GetElapsedMilliseconds16() - Timer) > 60000) // auto-recover every minute
		{
			// clear the queues before restarting
			InitializeBuffers();

			// enter normal mode
			EnterInitializationMode();
			LISTEN_MODE = 0;
			ExitInitializationMode();

			// re-enable interrupts when leaving LISTEN mode
			#ifdef CAN_DRIVER_INTERRUPT
			CANRIER_RXFIE = 1;
			#endif

			Timer = OS_GetElapsedMilliseconds16();
		}
	}

	return TRUE; // don't run other background tasks
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void CAN_Task(void)
{
	#ifdef DEBUG_CAN_BACKGROUND_TASK_PORT
	DEBUG_CAN_BACKGROUND_TASK_PORT = 1;
	#endif // DEBUG_CAN_BACKGROUND_TASK_PORT

	// check for errors
	// if no errors, then the regular background tasks can run
	if (!BusErrorBackgroundTask())
	{
		// sanity check -- make sure linked lists are OK
		// this is mainly here to check for memory errors that corrupt the queues
		EnterCriticalSection();
		if (Free.Count + Tx.Count + InTx.Count + Rx.Count != NUM_BUFFERS)
		{
			#ifdef CAN_MAINTAIN_STATISTICS
			if (SoftwareFaultCount != 0xFF)
				SoftwareFaultCount++;
			#endif // CAN_MAINTAIN_STATISTICS
			CAN_Init();
		}
		LeaveCriticalSection();

		#ifdef CAN_DRIVER_POLLED
		CANCellUnloadRx();
		CANCellLoadTx();
		#endif // CAN_DRIVER_POLLED

		// forward all messages on to the proper callbacks
		DispatchMessages();
	}

	#ifdef DEBUG_CAN_BACKGROUND_TASK_PORT
	DEBUG_CAN_BACKGROUND_TASK_PORT = 0;
	#endif // DEBUG_CAN_BACKGROUND_TASK_PORT
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
