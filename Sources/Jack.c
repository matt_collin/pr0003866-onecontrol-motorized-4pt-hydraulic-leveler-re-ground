#include "Global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER
// define jack positions
typedef enum {
	JACKS_NONE = 0,
	JACKS_LF = BIT0,
	JACKS_LR = BIT1,
	JACKS_RF = BIT2,
	JACKS_RR = BIT3,
	JACKS_LEFT = JACKS_LF | JACKS_LR,
	JACKS_RIGHT = JACKS_RF | JACKS_RR,
	JACKS_FRONT = JACKS_LF | JACKS_RF,
	JACKS_REAR = JACKS_LR | JACKS_RR,
	JACKS_ALL = JACKS_LF | JACKS_LR | JACKS_RF | JACKS_RR
} JACKS;

// define movement directions
typedef enum {
	DIRECTION_HALT = 0,
	DIRECTION_EXTEND,
	DIRECTION_RETRACT
} DIRECTION;

// management tasks
void Jack_Init(void);
void Jack_Task10ms(void);

// jack idle time
uint16 Jack_GetIdleTime(void);

// pump control
void Jack_SetPumpCmd(DIRECTION dir);
DIRECTION Jack_GetPumpCmd(void);
DIRECTION Jack_GetPumpAct(void);
void SetValveOnDelayOffset(uint16 delay);

// jack control
void Jack_ActivateJacks(JACKS jacks);
void Jack_DeactivateJacks(JACKS jacks);
JACKS Jack_GetJacksCmd(void);
JACKS Jack_GetJacksAct(void);
#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#undef SECONDS
#define SECONDS(s) ((uint16) ((s) * 100.0 + 0.5))	// use only w/ 10ms program

#define JACK_VALVE_ON_DELAY 				SECONDS(0.100)
#define JACK_VALVE_EXTEND_OFF_DELAY 	SECONDS(0.100)
#define JACK_VALVE_RETRACT_OFF_DELAY 	SECONDS(0.100)
#define _4_WAY_VALVE_OFF_DELAY			SECONDS(0.500)

#define JACK_DEFINITIONS \
JACK_DEFINITION(LF)      \
JACK_DEFINITION(LR)      \
JACK_DEFINITION(RF)      \
JACK_DEFINITION(RR)      \

// pump control variables
static struct { DIRECTION Command; uint16 ActiveTime; } Pump = { DIRECTION_HALT, 0xFFFF };

// jack control variables
static struct {
	JACKS Command; // jack command
	JACKS Active;  // current active jacks

	// jack specific variables
	#undef JACK_DEFINITION
	#define JACK_DEFINITION(jack) struct { uint16 ActiveTime; } jack;
	JACK_DEFINITIONS
} Jacks = {
	JACKS_NONE, // Command
	JACKS_NONE, // Active

	// jack specific variables
	#undef JACK_DEFINITION
	#define JACK_DEFINITION(jack) { 0xFFFF },
	JACK_DEFINITIONS
};

static uint16 ValveOnDelayOffset = 0;
void SetValveOnDelayOffset(uint16 delay) { ValveOnDelayOffset = delay; } 

//////////
//////////
//////////
//////////

// determines how long the jack valves have been off
static uint16 JackOffTime(void)
{
	if (!Jacks.Active)
	{
		uint16 idle = 0xFFFF;

		#undef JACK_DEFINITION
		#define JACK_DEFINITION(jack) if (idle > Jacks.jack.ActiveTime) idle = Jacks.jack.ActiveTime;
		JACK_DEFINITIONS

		return idle;
	}

	return 0; // jacks are moving!
}

uint16 Jack_GetIdleTime(void)
{
	if (PUMP_CONTROL) return 0;
	return JackOffTime();
}

//////////
//////////
//////////
//////////

static void DriveJacks(JACKS jacks)
{
	// activate/deactivate jacks
	#undef JACK_DEFINITION
	#define JACK_DEFINITION(jack)                   \
	if (jacks & JACKS_##jack)                       \
		Assert_##jack##_CONTROL;                    \
	else                                            \
		Deassert_##jack##_CONTROL;                  \
	if ((jacks ^ Jacks.Active) & JACKS_##jack)      \
		Jacks.jack.ActiveTime = 0;                  \

	JACK_DEFINITIONS

	// set active jacks
	Jacks.Active = jacks;
}

static void PumpOff(void)
{
	// 
	DriveJacks(JACKS_NONE);
	
	switch (Jack_GetPumpAct())
	{
		case DIRECTION_EXTEND:
			if (JackOffTime() > JACK_VALVE_EXTEND_OFF_DELAY)
				Deassert_PUMP_CONTROL;
			break;
		case DIRECTION_RETRACT:
	   		if (JackOffTime() > JACK_VALVE_RETRACT_OFF_DELAY)
				Deassert_PUMP_CONTROL;
			break;
		case DIRECTION_HALT:	
			return;
		default:
			break;
	}		

	if (JackOffTime() >= _4_WAY_VALVE_OFF_DELAY)
		Deassert_4WAY_VALVE_CONTROL;
}

static void PumpOn(DIRECTION dir)
{
	uint16 offset = 0;
	
	// EXTEND 
	// turn on pump && 4_way_valve
	// wait JACK_VALVE_ON_DELAY
	// turn on valve

	// RETRACT
	// turn on pump
	// wait JACK_VALVE_ON_DELAY
	// turn on valve	
	switch (dir)
	{
	default:
	case DIRECTION_HALT:
		Pump.ActiveTime = 0; // reset timer when valve is enabled
		Deassert_PUMP_CONTROL;
		Deassert_4WAY_VALVE_CONTROL;
		break;
	case DIRECTION_EXTEND:
		Assert_PUMP_CONTROL;
		Assert_4WAY_VALVE_CONTROL;
		offset = ValveOnDelayOffset;
		break;
	case DIRECTION_RETRACT:
		Assert_PUMP_CONTROL;
		Deassert_4WAY_VALVE_CONTROL;
		break;
	}

	if (Pump.ActiveTime >= JACK_VALVE_ON_DELAY + offset)
		DriveJacks(Jacks.Command);
}

static void DriveOutputs(void)
{
	// here's the sequence to control the pump and valves
	//
	// turning ON:
	//   - turn on extend/retract valve
	//   - wait VALVE_DELAY
	//   - enable individual jack valves
	//
	// turning OFF:
	//   - leg valves off
	//   - wait VALVE_DELAY
	//   - turn off ext/ret valve

	// determine direction that pump should be running in
	DIRECTION cmd_dir = Pump.Command;
	
	// shut pump down if no jacks are commanded to move
	if (!(Jacks.Command & JACKS_ALL))
		cmd_dir = DIRECTION_HALT;

	if (Jack_GetPumpAct())
	{
		if (Pump.ActiveTime != 0xFFFF)
			Pump.ActiveTime++;
	}
	else
		Pump.ActiveTime = 0;

	#undef JACK_DEFINITION
	#define JACK_DEFINITION(jack) if (Jacks.jack.ActiveTime != 0xFFFF) Jacks.jack.ActiveTime++;
	JACK_DEFINITIONS

	switch (cmd_dir)
	{
	default:
	case DIRECTION_HALT:
		PumpOff();
		break;
	
	case DIRECTION_EXTEND:
		PumpOn(DIRECTION_EXTEND);
		break;

	case DIRECTION_RETRACT:
		PumpOn(DIRECTION_RETRACT);
		break;
	}
}

//////////
//////////
//////////
//////////

void Jack_Init(void)
{
	// turn off all outputs
	#undef JACK_DEFINITION
	#define JACK_DEFINITION(jack) Deassert_##jack##_CONTROL;

	JACK_DEFINITIONS
}

void Jack_Task10ms(void)
{
	if (Manufacturing_IsActive())
		return;
	
	DriveOutputs();
}

//////////
//////////
//////////
//////////

// set pump direction
void Jack_SetPumpCmd(DIRECTION dir) { Pump.Command = dir; }
DIRECTION Jack_GetPumpCmd(void)     { return Pump.Command; }
DIRECTION Jack_GetPumpAct(void)     { if (!PUMP_CONTROL && !_4WAY_VALVE_CONTROL) return DIRECTION_HALT; return (DIRECTION) (_4WAY_VALVE_CONTROL ? DIRECTION_EXTEND : DIRECTION_RETRACT); }

// jack control
void Jack_ActivateJacks(JACKS jacks)   { Jacks.Command |= jacks; }
void Jack_DeactivateJacks(JACKS jacks) { Jacks.Command &= (JACKS)(~(uint8)jacks); }
JACKS Jack_GetJacksCmd(void)           { return ((JACKS) (Jacks.Command & JACKS_ALL)); }
JACKS Jack_GetJacksAct(void)           { return ((JACKS) (Jacks.Active & JACKS_ALL)); }

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
