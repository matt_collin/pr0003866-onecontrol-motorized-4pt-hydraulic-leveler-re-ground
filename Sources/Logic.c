// Logic.c
// Lippert Motorized Wheel Leveler Logic
// (c) 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved                                                                                          

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void Logic_Init(void);
void Logic_Task10ms(void);

// return battery voltage in a 8.8 fixed point format
uint16 Logic_GetVBatt(void);

// 3 axis leveling
#define X_AXIS  Sensor.X.Angle
#define Y_AXIS  Sensor.Y.Angle  
#define SENSOR_VALID  Sensor.Valid

extern uint16 PSI_Switch_Debounce;

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// debugging support
// should be commented out when releasing code
//#define DEBUG_DISABLE_EXCESSIVE_TILT_DETECTION
//#define DEBUG_DISABLE_SLEEP_TIMEOUT

//////////
//////////
//////////
//////////

#define SETUP_BUTTON        0x1000
#define HITCH_BUTTON        0x0800
#define MENU_UP_BUTTON      0x0400
#define BACK_BUTTON         0x0200
#define EXTEND_BUTTON       0x0100
#define MENU_DOWN_BUTTON    0x0080
#define ENTER_BUTTON        0x0040
#define RETRACT_BUTTON      0x0020
#define LEVEL_BUTTON        0x0010
#define FRONT_BUTTON        0x0008
#define REAR_BUTTON         0x0004
#define LEFT_BUTTON         0x0002
#define RIGHT_BUTTON        0x0001
#define BUTTON_NONE         0x0000
#define D_PAD_BUTTONS       (LEFT_BUTTON | RIGHT_BUTTON | FRONT_BUTTON | REAR_BUTTON)
#define MENU_BUTTONS            (ENTER_BUTTON | MENU_UP_BUTTON | MENU_DOWN_BUTTON)
#define ALL_BUTTONS         0x1FFF

#define DEFINE_STATES \
DEFINE_STATE(OFF,                       LEVELER_TYPE_3_SCREEN_IDLE_MOTORIZED,   (BUTTON_NONE))\
DEFINE_STATE(IDLE,                      LEVELER_TYPE_3_SCREEN_IDLE_MOTORIZED,   (MENU_BUTTONS | SETUP_BUTTON | LEVEL_BUTTON))\
DEFINE_STATE(GROUND_JACKS_1,            LEVELER_TYPE_3_SCREEN_AUTO,             (BACK_BUTTON))\
DEFINE_STATE(AUTO_LEVEL_FIRST_AXIS,     LEVELER_TYPE_3_SCREEN_AUTO,             (BACK_BUTTON))\
DEFINE_STATE(GROUND_JACKS_2,            LEVELER_TYPE_3_SCREEN_AUTO,             (BACK_BUTTON))\
DEFINE_STATE(AUTO_LEVEL,                LEVELER_TYPE_3_SCREEN_AUTO,             (BACK_BUTTON))\
DEFINE_STATE(MANUAL_LEVEL,              LEVELER_TYPE_3_SCREEN_MANUAL,           (BACK_BUTTON | EXTEND_BUTTON | RETRACT_BUTTON | D_PAD_BUTTONS))\
DEFINE_STATE(AUTO_RETRACT,              LEVELER_TYPE_3_SCREEN_AUTO,             (BACK_BUTTON))\
DEFINE_STATE(EMERGENCY_RETRACT,         LEVELER_TYPE_3_SCREEN_AUTO,             (BACK_BUTTON))\
DEFINE_STATE(ZERO_MODE,                 LEVELER_TYPE_3_SCREEN_MANUAL,           (BACK_BUTTON | EXTEND_BUTTON | RETRACT_BUTTON | D_PAD_BUTTONS | ENTER_BUTTON))\
DEFINE_STATE(FAULT,                     LEVELER_TYPE_3_SCREEN_ALERT,            (SETUP_BUTTON | ENTER_BUTTON))\
DEFINE_STATE(SETUP,                     LEVELER_TYPE_3_SCREEN_SELECT,           (BACK_BUTTON | ENTER_BUTTON))\
DEFINE_STATE(MENU,                      LEVELER_TYPE_3_SCREEN_SELECT,           (BACK_BUTTON | MENU_UP_BUTTON | MENU_DOWN_BUTTON))\
DEFINE_STATE(AIRBAG_TIME_CONFIG,        LEVELER_TYPE_3_SCREEN_SELECT,           (BACK_BUTTON | MENU_UP_BUTTON | MENU_DOWN_BUTTON | ENTER_BUTTON))\
DEFINE_STATE(GROUND_AFTER_LEVEL,        LEVELER_TYPE_3_SCREEN_AUTO,             (BACK_BUTTON))\

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#undef SECONDS
#define SECONDS(s) ((uint16) ((s) * 100.0 + 0.5))   // use only w/ 10ms program

#define ANGLE(a)  ((int16) ((a * 256.0) + 0.5))

// angle limits
#define EXCESS_ANGLE_LIMIT          ANGLE(5.0)

// leveling parameters
#define LEVEL_X_AXIS                ANGLE(0.3125)       /* X axis is level if within this tolerance */
#define LEVEL_Y_AXIS                ANGLE(0.3125)       /* Y axis is level if within this tolerance */
#define LEVEL_HYSTERESIS            ANGLE(0.250)        /* we attempt to level to this precision */
#define LEVEL_LED_HYSTERESIS        ANGLE(0.125)        /* level LED does not turn off until this much out of level tolerance */
#define SENSOR_STABILIZE_TIME       SECONDS(3.0)        /* jacks must halt for this long to let sensor reading stabilize */
#define MAX_LEVEL_ATTEMPTS          10                  /* iterate this many times */

// system timeouts
#define AUTO_RETRACT_TIMEOUT            SECONDS(67)
#define AUTO_LEVEL_TIMEOUT              SECONDS(105)
#define STROKE_LIMIT_TIMEOUT            SECONDS(6)          /* no movement in 6 seconds */
#define TOUCHPAD_SLEEP_TIMEOUT          SECONDS(7 * 60.0)

// low voltage detection parameters
#define LOW_VOLTAGE_DEBOUNCE            SECONDS(4.0)

// ground detect parameters
#define GROUND_DETECT_THRESHOLD         ANGLE(0.250)
#define GROUND_DETECT_DEBOUNCE_1        SECONDS(0.250)
#define GROUND_DETECT_DEBOUNCE_2        SECONDS(0.050)
#define GROUND_DETECT_TIMEOUT           SECONDS(50)         
#define GROUND_SENSOR_DELAY             SECONDS(3.0)
#define REGROUND_DETECT_THRESHOLD       ANGLE(0.100)
#define REGROUND_DETECT_DEBOUNCE        SECONDS(0.125)

// reground bump parameters
#define BUMP_PERIOD                     SECONDS(1.000)
#define BUMP_ON_TIME                    SECONDS(0.250)

// manual mode parameters
#define MANUAL_MODE_HYSTERESIS          ANGLE(0.250)
#define MANUAL_MODE_X_ANGLE_LIMIT       ANGLE(1.5)
#define MANUAL_MODE_JACK_TIMEOUT        SECONDS(30)

// PSI Switch parameters
#define ER_PSI_SWITCH_DEBOUNCE          SECONDS(2.0)
#define PSI_SWTICH_DEBOUNCE_LONG        SECONDS(6.0)

// air timers
#define AIRBAG_DUMP_TIME                SECONDS(22)
#define AIRBAG_PREFILL_TIME_SHORT       SECONDS(22)
#define AIRBAG_PREFILL_TIME_LONG        SECONDS(42)

// enum level state
typedef enum { LEVEL_OK = 0, LEVEL_LEFT = BIT1, LEVEL_FRONT = BIT2, LEVEL_RIGHT = BIT3, LEVEL_REAR = BIT4} LEVEL;

void Callback_OnConfigUserBlockLoadEvent(uint8 success)
{
	if (!success)
	{
		Config.UserBlock.XZero = 0x7FFF;
		Config.UserBlock.YZero = 0x7FFF;
		Config.UserBlock.ZeroValid = FALSE;
		Config.UserBlock.AirBagPrefillTime = AIRBAG_PREFILL_TIME_SHORT;
		Config.UserBlock.TouchPadPresent = FALSE;
	}
}

void Callback_OnConfigDiagnosticBlockLoadEvent(uint8 success)
{
	if (!success)
	{
		Config.DiagnosticBlock.LatchedRetractError = FALSE;
		Config.DiagnosticBlock.FullRetractByUser = FALSE;
		Config.DiagnosticBlock.AirFeaturesEnabled = FALSE;
		Config.DiagnosticBlock.TouchpadErrorDisplayed = FALSE;
	}
}

//////////
//////////
//////////
//////////

// sub-routines to help process tilt information
typedef struct { int16 X, Y; } ANGLE_STATE;

// return all angle info in one structure
static ANGLE_STATE GetAngle(void)
{
	ANGLE_STATE angle;
	angle.X = X_AXIS;
	angle.Y = Y_AXIS;
	return angle;
}

// determine the absolute value of the delta between two numbers
static uint16 AbsDelta(int16 a, int16 b)
{
	if (a > b)
		return (uint16) (a - b);
	return (uint16) (b - a);
}

// determine the largest absolute delta between two sets of angle information
static uint16 GetMaxAngleDelta(ANGLE_STATE angle1, ANGLE_STATE angle2)
{
	uint16 result, delta;

	result = AbsDelta(angle1.X, angle2.X);

	delta = AbsDelta(angle1.Y, angle2.Y);
	if (result < delta) result = delta;

	return result;
}

//////////
//////////
//////////
//////////

// state machine information
typedef enum {
	#undef DEFINE_STATE
	#define DEFINE_STATE(state, screen, buttons) STATE_##state,
	DEFINE_STATES
} STATE;

// prototype state functions
#undef DEFINE_STATE
#define DEFINE_STATE(state, screen, buttons) static void state##_OnEnter(STATE prev); static void state(void);
DEFINE_STATES

// state machine variables
static STATE State = (STATE) 0;  // current state
static uint16 StateTimer = 0; // time spent in current state
	
// these structs hold variables that are unique to each state
static struct { uint8 textState; } Idle;
static struct { DIRECTION newPumpDirection; uint8 timer; uint16 runTimeout;} Manual;
static struct { uint8 State, Attempts; int8 Approach; JACKS Jacks; int16 StopAngle; uint16 ElapsedTime; } AutoLevel;
static struct { uint16 buzzer; IDS_CAN_DTC activeDTC; } FaultMode;
static struct {
   uint8 state, airState;
   uint16 sampleCount;
   struct { int16 min, max; int32 avg; } X, Y;
} ZeroMode;
static struct { int8 state; } Menu;
static struct { STATE next; } ER;
static struct { uint8 preFill; } AutoRetract;
static struct { uint8 State; uint16 Set; } AirBagTimeConfig;
	
//////////
//////////
//////////
//////////
// debugging info

typedef enum {
	EVENT_NONE                      = 0x00,
	EVENT_RESET                     = 0x01,
	EVENT_SLEEP                     = 0x02,
	EVENT_LOW_VOLTAGE               = 0x04,
	EVENT_TOUCH_PAD_ERROR           = 0x05,
	EVENT_BAD_CALIBRATION           = 0x06,
	EVENT_STATE_RECOVERY            = 0x07,
	EVENT_NOT_CONFIGURED            = 0x08,
	EVENT_JACKS_ON_GROUND           = 0x09,
	EVENT_JACKS_OFF_GROUND          = 0x0A,
	EVENT_JACKS_RETRACTED           = 0x0B,
	EVENT_JACK_TIMEOUT              = 0x0C,
	EVENT_PANIC_STOP                = 0x0D,
	EVENT_SEQUENCE_COMPLETE         = 0x0E,
	EVENT_AUTO_LEVEL_FAILED         = 0x0F,
	EVENT_OUT_OF_STROKE             = 0x10,
	EVENT_MOVEMENT                  = 0x11,
	EVENT_EXT_SENSOR_PROBLEM        = 0x13,
	EVENT_FEATURE_DISABLED          = 0x14,
	EVENT_EXCESS_ANGLE_AUTO         = 0x15,
	EVENT_AUTO_REQUEST              = 0x16,
	EVENT_UI_REQUEST                = 0x17,
	EVENT_TP_REQUEST                = 0x18,
	EVENT_PARK_BRAKE_NOT_ON         = 0x1B,
	EVENT_LATCHED_RETRACT_ERROR     = 0x1C,
	EVENT_FAULT                     = 0x1D,
	
	EVENT_GENERIC                   = 0xFF
} EVENT;

typedef struct { STATE State; EVENT Event; } CHANGE_STATE_EVENT;
static CHANGE_STATE_EVENT EventHistory[3];

static void SetState(STATE next, EVENT event);
static void NewState(STATE state, EVENT event);

// auto-sleep timer
static uint16 TouchPadSleepTimeout = 0;

// low voltage detect
static uint8 LowVoltage = FALSE;

// switch panel state
static LEVELER_TYPE_3_BUTTONS UISwitchOnEvent; // switches that have changed 0->1 since last latch
static PANEL_INPUTS PanelSwitchOnEvent; // switches that have changed 0->1 since last latch

static LEVEL LevelStatus = (LEVEL) 0;
static DIRECTION LatchDirection = DIRECTION_HALT;
static uint16 LeakyHoseTimer = 0;
static uint8 LeakyHoseCounter = 0;
uint16 PSI_Switch_Debounce = PSI_SWTICH_DEBOUNCE_LONG;

static JACKS JacksOnGround = JACKS_NONE; // set to true once jacks grounded, false if manual retract occurs
static JACKS JacksToGroundAfterLevel = JACKS_NONE;

typedef struct { JACKS Jacks; DIRECTION Dir; } JACK_STRUCT;

typedef enum {
	SOURCE_UNKNOWN = 0,
	SOURCE_TOUCHPAD,
	SOURCE_VIRTUAL_UI,
	SOURCE_PARK_BRAKE
} AUTO_SOURCE;

static AUTO_SOURCE AutoSource = SOURCE_UNKNOWN;

typedef enum { AIRBAG_STATE_OFF = 0, AIRBAG_STATE_FILL, AIRBAG_STATE_DUMP } AIRBAG_STATE;
static struct {
	AIRBAG_STATE state;
	uint16 fillTimer;
	uint16 dumpTimer;
} Airbags = { AIRBAG_STATE_OFF, 0, 0 };

//////////
//////////
//////////
//////////

// stroke limit detection
// jacks are out of stroke if extending and no change of angle is seen

static JACKS OutOfStroke = JACKS_NONE;
static struct {
	ANGLE_STATE Angle;
	uint16 Timer;
} StrokeLimit;

static void ResetStrokeLimitDetection(void)
{
	StrokeLimit.Angle = GetAngle();
	StrokeLimit.Timer = 0;

	OutOfStroke = JACKS_NONE;
}

#pragma MESSAGE DISABLE C12056
static void StrokeLimitDetection(void)
{
	JACKS jacks;

	// runs AUTO mode, and hookup mode
	if ((State != STATE_AUTO_LEVEL_FIRST_AXIS) && (State != STATE_AUTO_LEVEL))
		return;

	jacks = Jack_GetJacksAct();
	if (!OutOfStroke // actively checking
		&& jacks // jacks are moving
		&& Jack_GetPumpAct() == DIRECTION_EXTEND // extending
		&& GetMaxAngleDelta(StrokeLimit.Angle, GetAngle()) < ANGLE(0.05)) // movement not detected
	{
		// debounce stroke limit
		if (StrokeLimit.Timer < STROKE_LIMIT_TIMEOUT)
		{
			StrokeLimit.Timer++;
			return;
		}

		OutOfStroke = jacks;
	}

	// reset detection if we get here
	StrokeLimit.Angle = GetAngle();
	StrokeLimit.Timer = 0;
}
#pragma MESSAGE DEFAULT C12056

static IDS_CAN_DTC GetOutOfStrokeDTC(void)
{
	// what if there's more than 1 jack stroked out?
	if (OutOfStroke & JACKS_LR) IDS_CAN_SetDTC(DTC_JACK_LR_OUT_OF_STROKE);
	if (OutOfStroke & JACKS_RR) IDS_CAN_SetDTC(DTC_JACK_RR_OUT_OF_STROKE);
	if (OutOfStroke & JACKS_LF) IDS_CAN_SetDTC(DTC_JACK_LF_OUT_OF_STROKE);
	if (OutOfStroke & JACKS_RF) IDS_CAN_SetDTC(DTC_JACK_RF_OUT_OF_STROKE);

	if (OutOfStroke & JACKS_LR) return DTC_JACK_LR_OUT_OF_STROKE;
	if (OutOfStroke & JACKS_RR) return DTC_JACK_RR_OUT_OF_STROKE;
	if (OutOfStroke & JACKS_LF) return DTC_JACK_LF_OUT_OF_STROKE;
	
	return DTC_JACK_RF_OUT_OF_STROKE;
}

//////////
//////////
//////////
//////////

// function to actuate jacks
static void DriveJacks(JACKS jacks, DIRECTION direction)
{
	if (jacks)
	{
		if ((direction == DIRECTION_RETRACT) && (State != STATE_AUTO_LEVEL))
			JacksOnGround = JACKS_NONE; // jacks are no longer on the ground!
		
		if ((direction == DIRECTION_EXTEND) && Config.DiagnosticBlock.FullRetractByUser)
		{
			// jacks no longer retracted (used to determine when to auto retract vs emergency retract)
			Config.DiagnosticBlock.FullRetractByUser = FALSE;
			Config_DiagnosticBlock_Flush();
		}
	}
	
	// activate the jacks
	Jack_SetPumpCmd((DIRECTION) (jacks ? direction : DIRECTION_HALT));
	Jack_ActivateJacks(jacks);

	// if jacks are moving
	if (direction)
	{
		// illuminate appropriate LEDs
		switch (jacks)
		{
		case JACKS_LEFT:    LevelerType3.Status.Outputs.LeftLED = (uint8) LEVELER_TYPE_3_OUTPUT_ON;     Panel_SetLeftLED(FAULT_BLINK_RATE);     break;
		case JACKS_RIGHT: LevelerType3.Status.Outputs.RightLED = (uint8) LEVELER_TYPE_3_OUTPUT_ON;  Panel_SetRightLED(FAULT_BLINK_RATE);    break;
		case JACKS_FRONT: LevelerType3.Status.Outputs.FrontLED = (uint8) LEVELER_TYPE_3_OUTPUT_ON;  Panel_SetFrontLED(FAULT_BLINK_RATE);    break;
		case JACKS_REAR:    LevelerType3.Status.Outputs.RearLED = (uint8) LEVELER_TYPE_3_OUTPUT_ON;     Panel_SetRearLED(FAULT_BLINK_RATE);     break;
		
		default:
		case JACKS_NONE:
		case JACKS_LF:
		case JACKS_RF:
		case JACKS_LR:
		case JACKS_RR:
		case JACKS_ALL: //lint -fallthrough
			break;
		}
	}
}

//////////
//////////
//////////
//////////

// enable Leveler Type 3 UI buttons for each state
static void SetChosenButtonsToDefaultState(STATE s, uint16 mask)
{
	uint16 show_buttons = 0;

	switch (s)
	{
	#undef DEFINE_STATE
	#define DEFINE_STATE(state, screen, buttons) case STATE_##state: show_buttons = buttons; break;
	DEFINE_STATES

	default:
		break;
	}

	// default buttons grey
	if (mask & SETUP_BUTTON)        LevelerType3.Status.ButtonsToGrey.EnterSetup = 1;
	if (mask & HITCH_BUTTON)        LevelerType3.Status.ButtonsToGrey.AutoHitch = 1;
	if (mask & MENU_UP_BUTTON)      LevelerType3.Status.ButtonsToGrey.MenuUp = 1;
	if (mask & BACK_BUTTON)         LevelerType3.Status.ButtonsToGrey.Back = 1;
	if (mask & EXTEND_BUTTON)       LevelerType3.Status.ButtonsToGrey.Extend = 1;
	if (mask & MENU_DOWN_BUTTON)    LevelerType3.Status.ButtonsToGrey.MenuDown = 1;
	if (mask & ENTER_BUTTON)        LevelerType3.Status.ButtonsToGrey.Enter = 1;
	if (mask & RETRACT_BUTTON)      LevelerType3.Status.ButtonsToGrey.Retract = 1;
	if (mask & LEVEL_BUTTON)        LevelerType3.Status.ButtonsToGrey.AutoLevel = 1;
	if (mask & FRONT_BUTTON)        LevelerType3.Status.ButtonsToGrey.Front = 1;
	if (mask & REAR_BUTTON)         LevelerType3.Status.ButtonsToGrey.Rear = 1;
	if (mask & LEFT_BUTTON)         LevelerType3.Status.ButtonsToGrey.Left = 1;
	if (mask & RIGHT_BUTTON)        LevelerType3.Status.ButtonsToGrey.Right = 1;

	// ungrey buttons
	if (mask & show_buttons & SETUP_BUTTON)     LevelerType3.Status.ButtonsToGrey.EnterSetup = 0;
	if (mask & show_buttons & HITCH_BUTTON)     LevelerType3.Status.ButtonsToGrey.AutoHitch = 0;
	if (mask & show_buttons & MENU_UP_BUTTON)       LevelerType3.Status.ButtonsToGrey.MenuUp = 0;
	if (mask & show_buttons & BACK_BUTTON)          LevelerType3.Status.ButtonsToGrey.Back = 0;
	if (mask & show_buttons & EXTEND_BUTTON)        LevelerType3.Status.ButtonsToGrey.Extend = 0;
	if (mask & show_buttons & MENU_DOWN_BUTTON)  LevelerType3.Status.ButtonsToGrey.MenuDown = 0;
	if (mask & show_buttons & ENTER_BUTTON)     LevelerType3.Status.ButtonsToGrey.Enter = 0;
	if (mask & show_buttons & RETRACT_BUTTON)       LevelerType3.Status.ButtonsToGrey.Retract = 0;
	if (mask & show_buttons & LEVEL_BUTTON)     LevelerType3.Status.ButtonsToGrey.AutoLevel = 0;
	if (mask & show_buttons & FRONT_BUTTON)     LevelerType3.Status.ButtonsToGrey.Front = 0;
	if (mask & show_buttons & REAR_BUTTON)          LevelerType3.Status.ButtonsToGrey.Rear = 0;
	if (mask & show_buttons & LEFT_BUTTON)          LevelerType3.Status.ButtonsToGrey.Left = 0;
	if (mask & show_buttons & RIGHT_BUTTON)     LevelerType3.Status.ButtonsToGrey.Right = 0;
}

static void SetLevelerScreen(STATE s)
{
	LEVELER_TYPE_3_SCREEN ui_screen = LEVELER_TYPE_3_SCREEN_IDLE_TOWABLE;

	switch (s)
	{
	#undef DEFINE_STATE
	#define DEFINE_STATE(state, screen, buttons) case STATE_##state: ui_screen = screen; break;
	DEFINE_STATES

	default:
		break;
	}

	LevelerType3_SetScreen(ui_screen);
	SetChosenButtonsToDefaultState(s, ALL_BUTTONS); // lets change buttons now, instead of waiting till next 10ms loop
}

// change state
static void SetState(STATE next, EVENT event)
{
	STATE prev;
	uint8 n;

	// reset latched retract everytime a state changes
	LatchDirection = DIRECTION_HALT;

	// adjust history buffer
	n = elementsof(EventHistory) - 1;
	while (n)
	{
		EventHistory[n] = EventHistory[n-1];
		n--;
	}
	EventHistory[0].State = next;
	EventHistory[0].Event = event;

	// OnExit() code goes here

	// enter new state, reset timers
	prev = State;     // save old state
	State = next;     // set new state
	StateTimer = 0;   // reset state timer
	if (TouchPadSleepTimeout != 0) TouchPadSleepTimeout = TOUCHPAD_SLEEP_TIMEOUT; // keep TP alive

	// jacks motion not carried from state to state
	Jack_SetPumpCmd(DIRECTION_HALT);
	Jack_DeactivateJacks(JACKS_ALL);

	// reset tilt detection algorithms
	ResetStrokeLimitDetection();

	// clear touch pad text
	ConsoleDriver_Reset();

	// change to appropriate screen
	SetLevelerScreen(State);
	
	// special processing when entering a particular state
	switch (State)
	{
	#undef DEFINE_STATE
	#define DEFINE_STATE(state, screen, buttons) case STATE_##state: state##_OnEnter(prev); break;
	DEFINE_STATES

	default:
		break;
	}
}

// switch to a new state, do not reset current state
static void NewState(STATE state, EVENT event)
{
	if (State != state)
		SetState(state, event);
}

//////////
//////////
//////////
//////////

// return battery voltage in a 8.8 fixed point format
uint16 Logic_GetVBatt(void)
{
	// Vadc = lsbs * VREF / 65535
	// Vactual = Vadc * (221 + 95.3) / 95.3
	// fixed point = Vactual * 256
	//
	// fixed point = lsbs * 256 * (221 + 95.3) / 95.3 * VREF / 65535
	//             = lsbs * 4248 / 256
	DWORD result;
	result.ui32 = (uint32) 4248 * Input.Analog.VBatt;
	return result.ui16.hi;
}

static void CheckLowVoltage(void)
{
	static uint16 timer = 0;
	static uint16 LowBattTimer_u1 = 0;
	static uint16 LowBattTimer_u2 = 0;
	static uint16 LowBattTimer_u3 = 0;

	uint8 low;

	// detect low voltage with hysteresis
	if (LowVoltage)
		low = (uint8)(Input.Analog.VBatt < (Config.ProductionBlock.VBATT_Low + VBATT_LSB(0.5)));
	else
		low = (uint8)(Input.Analog.VBatt < Config.ProductionBlock.VBATT_Low);

	// debounce low voltage
	if (++timer >= LOW_VOLTAGE_DEBOUNCE)
		LowVoltage = low;

	if (LowVoltage == low)
		timer = 0;

	// UNIVERSAL Vbatt CHECKS
	// these are long duration checks, so we are using
	// the INPUT averaged value of vbatt instead of doing
	// a instantaneous new read of the a2d channel.
	// It also means we are not competing with 1ms loop to read the value.
	if (IDS_CAN_IsDTCActive(DTC_BATTERY_VOLTAGE_LOW) == FALSE)
	{ // SEE IF ENTERing the state
		if (Input.Analog.VBatt < VBATT_LSB(UNIVERSAL_LOW_BATT1))
		{
			if (++LowBattTimer_u1 > UNIVERSAL_LOW_BATT1_DEBOUNCE)
				IDS_CAN_SetDTC(DTC_BATTERY_VOLTAGE_LOW);
		}
		else
		{
			LowBattTimer_u1 = 0;
		}

		if (Input.Analog.VBatt < VBATT_LSB(UNIVERSAL_LOW_BATT2))
		{
			if (++LowBattTimer_u2 > UNIVERSAL_LOW_BATT2_DEBOUNCE)
				IDS_CAN_SetDTC(DTC_BATTERY_VOLTAGE_LOW);
		}
		else
		{
			LowBattTimer_u2 = 0;
		}

		LowBattTimer_u3 = 0;
	}
	else
	{ // SEE IF EXITing the state
		if (Input.Analog.VBatt > VBATT_LSB(UNIVERSAL_LOW_BATT1))
		{
			if (++LowBattTimer_u3 > UNIVERSAL_LOW_BATT1_DEBOUNCE)
				IDS_CAN_ClearDTC(DTC_BATTERY_VOLTAGE_LOW);
		}
		else
		{
			LowBattTimer_u3 = 0;
		}

		LowBattTimer_u1 = 0;
		LowBattTimer_u2 = 0;
	}
}

//////////
//////////
//////////
//////////

#ifdef DEBUG_DISABLE_EXCESSIVE_TILT_DETECTION
	#define ExcessAngle() FALSE
#else
static uint8 ExcessAngle(void)
{
	uint16 angle = Abs(Sensor.X.Angle);

	if (Abs(Sensor.Y.Angle) > angle) angle = Abs(Sensor.Y.Angle); 
	return (uint8) (angle > EXCESS_ANGLE_LIMIT);
}
#endif

static void StartAutoFunction(STATE next, AUTO_SOURCE source)
{
	switch (next)
	{
	// nothing to do from these states
	default:
	case STATE_OFF:
	case STATE_IDLE:
	case STATE_MANUAL_LEVEL:
	case STATE_FAULT:
	case STATE_ZERO_MODE:
	case STATE_SETUP:
	case STATE_MENU:
	case STATE_AIRBAG_TIME_CONFIG:
		return;

	// continue checking from these states
	case STATE_EMERGENCY_RETRACT:
	case STATE_AUTO_RETRACT:
		break;
	
	// check park brake from these states
	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_GROUND_AFTER_LEVEL:
		if (!Input.Digital.ParkBrakeIsOn)
		{
			IDS_CAN_SetDTC(DTC_PARK_BRAKE_NOT_ENGAGED);
			return;
		}
		break;
	}

	// voltage needs to be high enough to start an auto function
	if ((Input.Analog.VBatt < Config.ProductionBlock.VBATT_EngineRun) && (source != SOURCE_PARK_BRAKE))
	{
		IDS_CAN_SetDTC(DTC_AUTO_START_VOLTAGE_LOW);
	}
	else
	{
		AutoSource = source;
		NewState(next, EVENT_AUTO_REQUEST);
	}
}

static void StartAutoLevel(AUTO_SOURCE source)
{
	if (!Config.UserBlock.ZeroValid)
	{
		IDS_CAN_SetDTC(DTC_LEVELER_ZERO_POINT_NOT_CONFIGURED);
		return;
	}

	if (JacksOnGround == JACKS_ALL) StartAutoFunction(STATE_AUTO_LEVEL, source);
	else StartAutoFunction(STATE_GROUND_JACKS_1, source);
}

//////////
//////////
//////////
//////////

static void UpdateLevelerType3Status(void)
{
	LevelerType3_SetAllStatusOutputs(LEVELER_TYPE_3_OUTPUT_OFF);
	
	// control the ext/retract LEDs
	switch (State)
	{
	default:
	case STATE_OFF:
	case STATE_SETUP:
	case STATE_MENU:
	case STATE_EMERGENCY_RETRACT:
	case STATE_AIRBAG_TIME_CONFIG:
		return;

	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_AUTO_LEVEL:
	case STATE_AUTO_RETRACT:
	case STATE_FAULT:
	case STATE_GROUND_AFTER_LEVEL:
		break;
	
	case STATE_IDLE:
	case STATE_ZERO_MODE:
	case STATE_MANUAL_LEVEL:
		if (UISwitchOnEvent.Extend) LatchDirection = DIRECTION_EXTEND;
		else if (UISwitchOnEvent.Retract) LatchDirection = DIRECTION_RETRACT;

		if (LatchDirection == DIRECTION_RETRACT) LevelerType3.Status.Outputs.RetractLED = (uint8) LEVELER_TYPE_3_OUTPUT_ON;
		else if (LatchDirection == DIRECTION_EXTEND) LevelerType3.Status.Outputs.ExtendLED = (uint8) LEVELER_TYPE_3_OUTPUT_ON;
		break;
	}

	if(LevelStatus == LEVEL_OK)
	{
		LevelerType3.Status.Outputs.LevelLED = (uint8) LEVELER_TYPE_3_OUTPUT_ON;
		return;
	}

	if(LevelStatus & LEVEL_RIGHT)   LevelerType3.Status.Outputs.RightLED = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;
	else if(LevelStatus & LEVEL_LEFT) LevelerType3.Status.Outputs.LeftLED = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;

	if(LevelStatus & LEVEL_FRONT) LevelerType3.Status.Outputs.FrontLED = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;
	else if(LevelStatus & LEVEL_REAR) LevelerType3.Status.Outputs.RearLED = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;
}

static void UpdateTouchPadStatus(void)
{
	// turn outputs off, let the state machine turn them back on as necessary
	Panel_SetOutputs(0);

	if (LowVoltage)
	{
		Panel_SetLowVoltageLED(TRUE);
		return;
	}

	if (TouchPadSleepTimeout == 0)
		return;
	
	// if we get here, panel is on
	Panel_SetOnOffLED(TRUE);
	
	switch (State)
	{
	default:
	case STATE_OFF:
	case STATE_FAULT:
	case STATE_AIRBAG_TIME_CONFIG:
		return;
	
	case STATE_SETUP:
	case STATE_MENU:
		Panel_SetWaitLED(FAULT_BLINK_RATE);
		return;
	
	case STATE_ZERO_MODE:
		Panel_SetOutputs(ATTN_BLINK_RATE);
		Panel_SetBuzzer(FALSE);
		Panel_SetOnOffLED(TRUE); // indicate this button is available
		return;

	case STATE_IDLE:
	case STATE_EMERGENCY_RETRACT:
		break;
	
	case STATE_MANUAL_LEVEL:
		Panel_SetManualLED(TRUE);
		break;
	
	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL:
	case STATE_AUTO_RETRACT:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_GROUND_AFTER_LEVEL:
		Panel_SetOnOffLED(TRUE);
		Panel_SetAutoLED(TRUE);
		Panel_SetAllLevelLED(AUTO_BLINK_RATE);
		Panel_SetWaitLED(ATTN_BLINK_RATE);
		if (!Input.Digital.JacksAllUp) Panel_SetJacksDownLED(TRUE); 
		return;
	}

	// drive normal panel LEDs
	if (!Input.Digital.JacksAllUp) Panel_SetJacksDownLED(TRUE); 
	if (!Input.Digital.ParkBrakeIsOn) Panel_SetParkBrakeLED(TRUE);
	
	if (LevelStatus == LEVEL_OK) Panel_SetAllLevelLED(TRUE);
	else
	{
		if(LevelStatus & LEVEL_RIGHT)   Panel_SetRightLED(ATTN_BLINK_RATE);
		else if(LevelStatus & LEVEL_LEFT) Panel_SetLeftLED(ATTN_BLINK_RATE);

		if(LevelStatus & LEVEL_FRONT) Panel_SetFrontLED(ATTN_BLINK_RATE);
		else if(LevelStatus & LEVEL_REAR) Panel_SetRearLED(ATTN_BLINK_RATE);
	}
}

//////////
//////////
//////////
//////////

static JACK_STRUCT GetVirtualUIJackCommand(void)
{
	JACK_STRUCT j = { JACKS_ALL, DIRECTION_HALT };

	// determine jack(s) to drive
	if ((uint8) LatchDirection)
	{
		// let's (re)enable the D-PAD buttons
		// shouldn't be necessary, as this is done every 10ms
		// however maybe someone disabled them along the way, which we are now saying was the wrong thing to do, and they should be on
		// so we turn them back on
		SetChosenButtonsToDefaultState(State, D_PAD_BUTTONS);
		
		// can only retract rear in idle mode
		if ((LatchDirection == DIRECTION_EXTEND) && (State == STATE_IDLE))
			LevelerType3.Status.ButtonsToGrey.Rear = 1;
		
		if (LevelerType3.Input.Buttons.Left)  j.Jacks &= JACKS_LEFT;
		if (LevelerType3.Input.Buttons.Right) j.Jacks &= JACKS_RIGHT;
		if (LevelerType3.Input.Buttons.Front) j.Jacks &= JACKS_FRONT;
		if (LevelerType3.Input.Buttons.Rear)  j.Jacks &= JACKS_REAR;
		j.Dir = LatchDirection;
	}
	else
	{
		LevelerType3.Status.ButtonsToGrey.Front = 1;
		LevelerType3.Status.ButtonsToGrey.Left = 1;
		LevelerType3.Status.ButtonsToGrey.Right = 1;
		LevelerType3.Status.ButtonsToGrey.Rear = 1;
	}

	if (j.Jacks == JACKS_ALL)
	{
		j.Jacks = JACKS_NONE;
		j.Dir = DIRECTION_HALT;
	}
		
	return j;
}

static JACK_STRUCT GetTPJackCommand(void)
{
	JACK_STRUCT j = { JACKS_ALL, DIRECTION_HALT };

	// determine jack(s) to drive
	if (TouchPadSleepTimeout != 0)
	{
		if (Panel.Input.Left)  j.Jacks &= JACKS_LEFT;
		if (Panel.Input.Right) j.Jacks &= JACKS_RIGHT;
		if (Panel.Input.Front) j.Jacks &= JACKS_FRONT;
		if (Panel.Input.Rear)  j.Jacks &= JACKS_REAR;
	}   
	
	if (j.Jacks == JACKS_ALL)
		j.Jacks = JACKS_NONE;
	else
		j.Dir = (Panel.Input.RetractAll) ? DIRECTION_RETRACT : DIRECTION_EXTEND;
	
	return j;
}

// deterine proper jacks to drive from all UI sources (app, TP, RF, ect..)
static JACK_STRUCT DetermineManualJacksToDrive(void)
{
	JACK_STRUCT j = { JACKS_NONE, DIRECTION_HALT };

	// there is an implied priority here, be careful
	j = GetVirtualUIJackCommand();
	if (j.Jacks == JACKS_NONE) j = GetTPJackCommand();

	return j;
}

// get local copy of panel switch
static void CheckLevelerType3Switches(void)
{
	static LEVELER_TYPE_3_BUTTONS level_ui = {0};
	union { LEVELER_TYPE_3_BUTTONS Switch; uint16 Bits; } old, new;

	// clear bits, because we don't know how many bits will be set
	old.Bits = new.Bits = 0;

	old.Switch = level_ui;  // save last state of switches
	level_ui = LevelerType3.Input.Buttons; // read new state of switches
	new.Switch = level_ui;

	// determine if any switches have changed state
	old.Bits ^= new.Bits; // XOR to determine which switches have changed state

	// determine which switches have gone 0->1
	new.Bits &= old.Bits; // AND to determine which switches have changed to "on"
	UISwitchOnEvent = new.Switch;

	switch (State)
	{
	// abort/panic stop if back button pressed during these states
	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL:
	case STATE_AUTO_RETRACT:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_GROUND_AFTER_LEVEL:
		if (UISwitchOnEvent.Back)
			IDS_CAN_SetDTC(DTC_USER_PANIC_STOP);
		return;

	// these states can be used to enter setup, if commanded
	case STATE_OFF:
	case STATE_IDLE:
	case STATE_FAULT:
		break;

	// nothing to do from these states
	default:
	case STATE_ZERO_MODE:
	case STATE_SETUP:
	case STATE_MANUAL_LEVEL:
	case STATE_MENU:
	case STATE_EMERGENCY_RETRACT:
	case STATE_AIRBAG_TIME_CONFIG:
		return;
	}

	if (UISwitchOnEvent.EnterSetup)
		NewState(STATE_SETUP, EVENT_UI_REQUEST);
}

static void CheckTouchPadSwitches(void)
{
	static union { PANEL_INPUTS Switch; uint8 Bits; } last = { 0 };
	union { PANEL_INPUTS Switch; uint8 Bits; } current, event, turn_on_event;
	static struct { uint8 front, rear, timeout; } Off = { 0,0,0 };

	// clear bits, because we don't know how many bits will be set
	event.Bits = turn_on_event.Bits = 0;

	// determine if any switches have changed state
	current.Switch = Panel.Input;                               // get new (needed for bit-wise operations
	event.Bits = last.Bits ^ current.Bits;                  // any change
	turn_on_event.Bits = current.Bits & event.Bits;     // 0->1 change
	last.Switch = Panel.Input;                                  // save previous

	PanelSwitchOnEvent = turn_on_event.Switch;

	switch (State)
	{
	// treat any button press as a panic stop from these states
	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_AUTO_LEVEL:
	case STATE_AUTO_RETRACT:
	case STATE_GROUND_AFTER_LEVEL:
		if (turn_on_event.Bits != 0)
			IDS_CAN_SetDTC(DTC_USER_PANIC_STOP);
		return;

	// nothing to do from these modes
	default:
	case STATE_FAULT:
	case STATE_ZERO_MODE:
	case STATE_SETUP: 
	case STATE_MENU:
	case STATE_OFF:
	case STATE_AIRBAG_TIME_CONFIG:
		break;

	//ignore TP operation during ER
	case STATE_EMERGENCY_RETRACT:
		return;

		// auto functions are available from these states
	case STATE_IDLE:
		if (TouchPadSleepTimeout == 0)
			break;
		if (turn_on_event.Switch.RetractAll) 
		{
			StartAutoFunction(STATE_AUTO_RETRACT, SOURCE_TOUCHPAD);
			return;
		}
		//lint -fallthrough
	
	case STATE_MANUAL_LEVEL:
		if (TouchPadSleepTimeout == 0)
			break;
		if (turn_on_event.Switch.Auto) 
		{
			StartAutoLevel(SOURCE_TOUCHPAD);
			return;
		}
		else if (turn_on_event.Switch.Manual)
		{
			if (Input.Digital.ParkBrakeIsOn)
			{
				NewState(STATE_MANUAL_LEVEL, EVENT_TP_REQUEST);
				return;
			}
			else
			{
				IDS_CAN_SetDTC(DTC_PARK_BRAKE_NOT_ENGAGED);
			}
		}
		break;
	}

	// zero mode button sequence: 5x front, 5x rear with panel off
	if (TouchPadSleepTimeout == 0)
	{
		if (Off.timeout && !--Off.timeout) Off.front = Off.rear = 0;
		if (PanelSwitchOnEvent.Front) { Off.timeout = 255; Off.front++; Off.rear = 0;   }
		if (PanelSwitchOnEvent.Rear)
		{ 
			Off.timeout = 255; 
			if (Off.front >= 5) Off.rear++; 
			else Off.front = 0; 
		
			if ((Off.front >= 5) && (Off.rear >= 5))
			{
				TouchPadSleepTimeout = TOUCHPAD_SLEEP_TIMEOUT;
				NewState(STATE_ZERO_MODE, EVENT_UI_REQUEST);
				return;
			}
		}
	}

	if (turn_on_event.Switch.OnOff) 
	{
		// turn panel on/off
		if (TouchPadSleepTimeout == 0) TouchPadSleepTimeout = TOUCHPAD_SLEEP_TIMEOUT;
		else TouchPadSleepTimeout = 0;
	}
	else if (turn_on_event.Bits && (TouchPadSleepTimeout != 0))
		TouchPadSleepTimeout = TOUCHPAD_SLEEP_TIMEOUT;                  // if on, keep panel alive 
}

//////////
//////////
//////////
//////////

static void CheckLossOfUI(void)
{
	switch (State)
	{
	// these states are a don't care
	default:
	case STATE_OFF:
	case STATE_FAULT:
	case STATE_IDLE:
	case STATE_EMERGENCY_RETRACT:
	case STATE_MANUAL_LEVEL:
	case STATE_ZERO_MODE:
	case STATE_AIRBAG_TIME_CONFIG:
		return;

	// these states are only accessable from the UI
	// we'll keep them alive for a few seconds after a session closes, in case another mobile device has been waiting to open a session
	case STATE_SETUP:
	case STATE_MENU:
		if (LevelerType3_TimeSinceSessionClosed_ms() > 5000)
			NewState(STATE_OFF, EVENT_TOUCH_PAD_ERROR); // turn off the leveler
		else if (LevelerType3_GetInputAge_ms() > 10000)
			NewState(STATE_IDLE, EVENT_TOUCH_PAD_ERROR); // something wierd is going on, return to IDLE state
		break;

	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_AUTO_LEVEL:
	case STATE_AUTO_RETRACT:
	case STATE_GROUND_AFTER_LEVEL:
		// only exit auto modes if the source of the command was the UI that is closing
		if (AutoSource == SOURCE_VIRTUAL_UI)
		{
			// OK, we're in an AUTO function started by the UI
			// close it if:
			// -- session closes
			// -- it's been 3 seconds since we've received valid input info
			if (LevelerType3_TimeSinceSessionClosed_ms() > 0 || LevelerType3_GetInputAge_ms() > 3000)
				NewState(STATE_OFF, EVENT_TOUCH_PAD_ERROR);
		}
		break;
	}
}

static void CheckLossOfTouchpad(void)
{
	if (!Panel_Error())
	{
		if (Config.DiagnosticBlock.TouchpadErrorDisplayed)
		{
			Config.DiagnosticBlock.TouchpadErrorDisplayed = FALSE;
			Config_DiagnosticBlock_Flush();
		}

		IDS_CAN_ClearDTC(DTC_TOUCH_PAD_COMM_FAILURE);
		return;
	}
	else if (Config.UserBlock.TouchPadPresent)
	{
		IDS_CAN_SetDTC(DTC_TOUCH_PAD_COMM_FAILURE);
	}
	
	// clear the sleep timeout
	TouchPadSleepTimeout = 0;
	
	// system should be off when there is no touchpad
	switch (State)
	{
	// illegal states?
	default:
		break;

	case STATE_OFF:
	case STATE_IDLE:
	case STATE_FAULT:
	case STATE_EMERGENCY_RETRACT:
	case STATE_MANUAL_LEVEL: // UI only state
	case STATE_ZERO_MODE: // UI only state
	case STATE_SETUP: // UI only state
	case STATE_MENU:
	case STATE_AIRBAG_TIME_CONFIG:
		break;

	// AUTO states
	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL:
	case STATE_AUTO_RETRACT:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_GROUND_AFTER_LEVEL:
		// if the source of the last auto command was the touchpad, make sure and stop the AUTO function
		if (AutoSource == SOURCE_TOUCHPAD)
			NewState(STATE_OFF, EVENT_TOUCH_PAD_ERROR);
		break;
	}
}

//////////
//////////
//////////
//////////

// enforce sleep mode requirements
static void HandleSleepTimeout(void)
{

#if !defined DEBUG_DISABLE_SLEEP_TIMEOUT
	// reset timeout if pump is running (jacks are moving) and touchpad is already on
	if (Jack_GetPumpAct() && TouchPadSleepTimeout)
	{
		TouchPadSleepTimeout = TOUCHPAD_SLEEP_TIMEOUT;
		return;
	}

	// handle timeout in "on" state
	switch (State)
	{
	case STATE_OFF:
	case STATE_EMERGENCY_RETRACT:
		return;
		
	case STATE_AUTO_RETRACT:
		// ALWAYS auto retract if it is due to park brake
		if (AutoSource == SOURCE_PARK_BRAKE)
			return;
		//lint -fallthrough
	
	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_AUTO_LEVEL:
	case STATE_GROUND_AFTER_LEVEL:
		if (TouchPadSleepTimeout) TouchPadSleepTimeout = TOUCHPAD_SLEEP_TIMEOUT;
		break;
	
	case STATE_FAULT:
	case STATE_IDLE:
	case STATE_MANUAL_LEVEL:
	case STATE_ZERO_MODE:
	case STATE_SETUP:
	case STATE_MENU:
	case STATE_AIRBAG_TIME_CONFIG:
	default:
		// switch system off when timeout expires
		if (TouchPadSleepTimeout) TouchPadSleepTimeout--;
		break;
	}

	// check for deactivation of all sources
	// - Leveler UI is gone
	// - Touch pad has errored, or hasn't been used in 7 minutes
	if (LevelerType3_TimeSinceSessionClosed_ms() > 5000 && (TouchPadSleepTimeout == 0))
		NewState(STATE_OFF, EVENT_SLEEP);
#endif
}

//////////
//////////
//////////
//////////

static void CheckErrors(void)
{
	if (State == STATE_FAULT)
		return; // wait till current error is acknowledged before checking for new errors

	// check for bad calibration / factory settings
	if (Config.ProductionBlock.ProductionByte)
		IDS_CAN_SetDTC(DTC_ECU_IS_DEFECTIVE);
	else
		IDS_CAN_ClearDTC(DTC_ECU_IS_DEFECTIVE);

	if (Config.DiagnosticBlock.LatchedRetractError)
		IDS_CAN_SetDTC(DTC_PSI_SWITCH_TIMEOUT);
	else
		IDS_CAN_ClearDTC(DTC_PSI_SWITCH_TIMEOUT);
	
	// no need to check for additional errors unless in an active state
	switch (State)
	{
	case STATE_OFF:
	case STATE_FAULT:
	case STATE_ZERO_MODE:
	case STATE_SETUP:
	case STATE_MENU:
	case STATE_AIRBAG_TIME_CONFIG:
	// no need to check from these states
		return;

	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_GROUND_AFTER_LEVEL:
		// verify auto features are available
		if (!Config.UserBlock.ZeroValid)
			IDS_CAN_SetDTC(DTC_LEVELER_ZERO_POINT_NOT_CONFIGURED);
		break; // continue checking

	// let these states finish, unless there is a problem with PSI switch
	case STATE_AUTO_RETRACT:
	case STATE_EMERGENCY_RETRACT:
		if (AutoSource == SOURCE_PARK_BRAKE)
		{
			if (IDS_CAN_IsDTCActive(DTC_AUTO_RETRACT_TIMEOUT) || IDS_CAN_IsDTCActive(DTC_PSI_SWITCH_TIMEOUT))
				break;
			return;
		}
		break;

	default:
	case STATE_IDLE:
	case STATE_MANUAL_LEVEL:
		break; // continue checking
	}

	// abort certain modes if low voltage detected
	if (LowVoltage)
	{
		switch (State)
		{
		case STATE_OFF:
		case STATE_IDLE:
		case STATE_FAULT:
		case STATE_ZERO_MODE:
		case STATE_SETUP:
		case STATE_MENU:
		case STATE_EMERGENCY_RETRACT:
		case STATE_AIRBAG_TIME_CONFIG:
			// ignore condition
			break;

		default:
		case STATE_GROUND_JACKS_1:
		case STATE_AUTO_LEVEL_FIRST_AXIS:
		case STATE_GROUND_JACKS_2:
		case STATE_AUTO_LEVEL:
		case STATE_MANUAL_LEVEL:
		case STATE_AUTO_RETRACT:
		case STATE_GROUND_AFTER_LEVEL:
			// can't get into these states
			// they should be aborted
			IDS_CAN_SetDTC(DTC_OPERATING_VOLTAGE_DROPOUT);
			break;
		}
	}

	// faults occur on DTC set, put into fault mode
	if (IDS_CAN_AreAnyDTCsActive())
	{
		// exception - don't care about touchpad comm fault if already 
		// displayed once and it hasn't reconnected since
		if ((IDS_CAN_GetNumActiveDTCs() == 1) && IDS_CAN_IsDTCActive(DTC_TOUCH_PAD_COMM_FAILURE))
		{
			if (Config.DiagnosticBlock.TouchpadErrorDisplayed)
				return;
		}

		NewState(STATE_FAULT, EVENT_FAULT);
		return;
	}
}

static void CheckForEmergencyRetract(void)
{
	// if already in ER mode, no need to reset
	if (State == STATE_EMERGENCY_RETRACT)
		return;
	
	if (Config.DiagnosticBlock.LatchedRetractError)
		return;
	
	// if we get here -- ign acc is NOT on so ign run must be, otherwise we'd have no power 
	switch (State)
	{
	case STATE_OFF:
	case STATE_IDLE:
	case STATE_FAULT:
	case STATE_SETUP:
	case STATE_MENU:
		if (!Input.Digital.JacksAllUp && !Input.Digital.ParkBrakeIsOn)
		{
			TouchPadSleepTimeout = TOUCHPAD_SLEEP_TIMEOUT;

			if (Config.DiagnosticBlock.FullRetractByUser)
			{
				StartAutoFunction(STATE_EMERGENCY_RETRACT, SOURCE_PARK_BRAKE);
			}
			else
			{
				StartAutoFunction(STATE_AUTO_RETRACT, SOURCE_PARK_BRAKE);
				AutoRetract.preFill = FALSE;
			}
		}
		break;

	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_AUTO_LEVEL:
	case STATE_MANUAL_LEVEL:
	case STATE_GROUND_AFTER_LEVEL:
		if (!Input.Digital.ParkBrakeIsOn)
		{
			if (Config.DiagnosticBlock.FullRetractByUser)
			{
				StartAutoFunction(STATE_EMERGENCY_RETRACT, SOURCE_PARK_BRAKE);
			}
			else
			{
				StartAutoFunction(STATE_AUTO_RETRACT, SOURCE_PARK_BRAKE);
				AutoRetract.preFill = FALSE;
			}
		}
		break;

	default:
	case STATE_AUTO_RETRACT:
	case STATE_EMERGENCY_RETRACT:
	case STATE_ZERO_MODE:
	case STATE_AIRBAG_TIME_CONFIG:
		// ignore condition
		break;
	}
}

//////////
//////////
//////////
//////////

static void UpdateLevelState(void)
{
	int16 angle;
	static uint8 debounce = 0;
	static LEVEL level = (LEVEL) 0;
	LEVEL last = level;

	// determine left-right level state
	angle = X_AXIS;
	if ((level & LEVEL_LEFT) && angle < LEVEL_X_AXIS)
		level &= (LEVEL)((uint8) (~(uint8)LEVEL_LEFT)); // lifting the left makes X smaller (X is too big)
	if ((level & LEVEL_RIGHT) && angle > -LEVEL_X_AXIS)
		level &= (LEVEL)((uint8) (~(uint8)LEVEL_RIGHT)); // lifting the right makes X larger (X is too small)
	if (!(level & (LEVEL_LEFT | LEVEL_RIGHT)))
	{
		if (angle < -LEVEL_X_AXIS-LEVEL_LED_HYSTERESIS)
			level |= LEVEL_RIGHT; // X is too small, lift the right to make it larger
		else if (angle > LEVEL_X_AXIS+LEVEL_LED_HYSTERESIS)
			level |= LEVEL_LEFT; // X is too large, lift the left to make it smaller
	}

	// determine front-rear level state
	angle = Y_AXIS;
	if ((level & LEVEL_REAR) && angle < LEVEL_Y_AXIS)
		level &= (LEVEL)((uint8) (~(uint8)LEVEL_REAR)); // lifting the rear makes Y smaller (Y is too big)
	if ((level & LEVEL_FRONT) && angle > -LEVEL_Y_AXIS)
		level &= (LEVEL)((uint8) (~(uint8)LEVEL_FRONT)); // lifting the front makes Y larger (Y is too small)
	if (!(level & (LEVEL_FRONT | LEVEL_REAR)))
	{
		if (angle < -LEVEL_Y_AXIS-LEVEL_LED_HYSTERESIS)
			level |= LEVEL_FRONT; // Y is too small, lift the front to make it larger
		else if (angle > LEVEL_Y_AXIS+LEVEL_LED_HYSTERESIS)
			level |= LEVEL_REAR; // Y is too large, lift the rear to make it smaller
	}

	// debounce
	if (level != last)
		debounce = 0;
	else if (++debounce >= 50) { LevelStatus = level; debounce = 0; }
}

//////////
//////////
//////////
//////////
#pragma MESSAGE DISABLE C5703
static void OFF_OnEnter(STATE prev)
{
	
	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void OFF(void)
{
	// disable all buttons
	LevelerType3_SetAllGreyButtons(TRUE);
	
	// turn system on when UI activates
	if ((LevelerType3_TimeSinceSessionClosed_ms() == 0 && LevelerType3_GetInputAge_ms() < 500) || TouchPadSleepTimeout)
	{
		if (Config.UserBlock.XZero == 0x7FFF || Config.UserBlock.YZero == 0x7FFF || !Config.UserBlock.ZeroValid)
			NewState(STATE_ZERO_MODE, EVENT_UI_REQUEST);
		else
			NewState(STATE_IDLE, EVENT_UI_REQUEST);
	}
}

//////////
//////////
//////////
//////////

// IDLE mode code

static uint8 IDLE_DisplayMenuAndCheckForNewState(void)
{
	uint8 new_state = FALSE;
	#define NUM_MENU_ITEMS 3

	if (UISwitchOnEvent.MenuUp) { Idle.textState--; ConsoleDriver_ClearAllLines();}
	else if (UISwitchOnEvent.MenuDown) { Idle.textState++; ConsoleDriver_ClearAllLines();}

	if (LowVoltage)
	{
		LevelerType3_SetAllGreyButtons(0xFF);
		ConsoleDriver_ClearAllLines();
		ConsoleDriver_AddLine(0, "Low Battery");
		return TRUE;
	}
	else
	{
		ConsoleDriver_AddLine(4, "\x10 \x14 to scroll");
		ConsoleDriver_AddLine(5, "ENTER to select");
	}
	
	switch (Idle.textState)
	{
	default:
		// advance text state, if wrap around depending on direction
		if (Idle.textState > 0) Idle.textState = 0;
		else Idle.textState = NUM_MENU_ITEMS - 1;
		break;

	case 0:
		ConsoleDriver_AddLine(0, "\x07 Info");
		ConsoleDriver_AddLine(1, "   Auto Retract");
		ConsoleDriver_AddLine(2, "   Manual Mode");

		if (UISwitchOnEvent.Enter)
		{
			NewState(STATE_MENU, EVENT_UI_REQUEST);
			new_state = TRUE;
		}
		break;

	case 1:
		ConsoleDriver_AddLine(0, "   Info");
		ConsoleDriver_AddLine(1, "\x07 Auto Retract");
		ConsoleDriver_AddLine(2, "   Manual Mode");

		if (UISwitchOnEvent.Enter)
		{
			StartAutoFunction(STATE_AUTO_RETRACT, SOURCE_VIRTUAL_UI);
			new_state = TRUE;
		}
		break;

	case 2:
		ConsoleDriver_AddLine(0, "   Info");
		ConsoleDriver_AddLine(1, "   Auto Retract");
		ConsoleDriver_AddLine(2, "\x07 Manual Mode");

		if (UISwitchOnEvent.Enter)
		{
			if (Input.Digital.ParkBrakeIsOn)
			{
				NewState(STATE_MANUAL_LEVEL, EVENT_UI_REQUEST);
				new_state = TRUE;
			}
			else
			{
				IDS_CAN_SetDTC(DTC_PARK_BRAKE_NOT_ENGAGED);
			}
		}
		break;
	}

	return new_state;
}

#pragma MESSAGE DISABLE C5703
static void IDLE_OnEnter(STATE prev)
{
	Idle.textState = 0;

	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void IDLE(void)
{
	// display the user menu
	if (IDLE_DisplayMenuAndCheckForNewState())
		return;

    ConsoleDriver_AddLine(6, blankline);   

	// Auto Level Button disabled if zero is not set
	if (Config.UserBlock.XZero == 0x7FFF || Config.UserBlock.YZero == 0x7FFF || !Config.UserBlock.ZeroValid)
	{
	    LevelerType3.Status.ButtonsToGrey.AutoLevel = 1;
	    ConsoleDriver_AddLine(6, "Zero Point Not Set");   
	}
	else if (!Input.Digital.ParkBrakeIsOn)
	{
	    LevelerType3.Status.ButtonsToGrey.AutoLevel = 1;
	    ConsoleDriver_AddLine(6, "Apply Park Brake to Level");   
	}
	else if (UISwitchOnEvent.AutoLevel)
	{
		StartAutoLevel(SOURCE_VIRTUAL_UI);
		return;
	}
}

//////////
//////////
//////////
//////////

static struct {
	ANGLE_STATE StartAngle;
	uint8 Debounce, Timer;
} GroundDetection = { {0,0},0,0 };

static void ResetGroundDetection(uint8 debounce)
{
	// get current angle
	GroundDetection.Timer = debounce;
	GroundDetection.StartAngle = GetAngle();
	GroundDetection.Debounce = debounce;
}

#pragma MESSAGE DISABLE C12056
static uint8 GroundDetected(uint16 angle_threshold)
{
	ANGLE_STATE current_angle = GetAngle();

	// see if threshold has been exceeded
	if (GetMaxAngleDelta(GroundDetection.StartAngle, current_angle) < angle_threshold)
		GroundDetection.Timer = 0;
	else if (++GroundDetection.Timer >= GroundDetection.Debounce)
		return TRUE;
	
	return FALSE;
}
#pragma MESSAGE DEFAULT C12056

static JACKS GetFirstJack(JACKS jacks)
{
	// if front/rear jack pair, lift lowest side first
	if (jacks == JACKS_FRONT)
	{
		if (Sensor.X.Angle > 0) jacks = JACKS_LF; else jacks = JACKS_RF;
	
	}
	else if (jacks == JACKS_REAR)
	{
	if (Sensor.X.Angle > 0) jacks = JACKS_LR;   else jacks = JACKS_RR;
	}
	
	// drive jack text
	switch (jacks)
	{
	
	// only 1 jack to bump
	case JACKS_LF: ConsoleDriver_AddLine(2, "Left-Front leveling jack"); break;
	case JACKS_LR: ConsoleDriver_AddLine(2, "Left-Rear leveling jack"); break;
	case JACKS_RF: ConsoleDriver_AddLine(2, "Right-Front leveling jack"); break;
	case JACKS_RR: ConsoleDriver_AddLine(2, "Right-Rear leveling jack"); break;
		
	// these don't make sense, don't bump anything
	default:
	case JACKS_FRONT:
	case JACKS_REAR:
	case JACKS_NONE:
	case JACKS_LEFT:
	case JACKS_RIGHT:
	case JACKS_ALL:
		return JACKS_NONE;
	}

	return jacks;
}

static struct { uint8 state, finished; JACKS finishedJackFlags, jacksToGround, jacksToBump; uint16 delay, bumpTimer; } Ground;

#pragma MESSAGE DISABLE C5703
static void GroundJacks_OnEnter(JACKS jacks_to_ground)
{
	Ground.state = 0; // reset state
	Ground.finished = FALSE;
	
	Ground.jacksToGround = jacks_to_ground;
	
	if (jacks_to_ground == JACKS_FRONT)
	{
		Ground.jacksToBump = JACKS_NONE;
		ConsoleDriver_AddLine(1, "Grounding Front jacks");
	}
	else
	{
		Ground.jacksToBump = jacks_to_ground;
		ConsoleDriver_AddLine(1, "Grounding Rear jacks");
	}
	
	Ground.finishedJackFlags = Ground.jacksToBump | Ground.jacksToGround;
	
	ConsoleDriver_AddLine(0, autoleveltext);
	ConsoleDriver_AddLine(4, remainstill);

}
#pragma MESSAGE DEFAULT C5703

static void GroundJacks(void)
{
	enum { GROUND_DELAY = 0, GROUND_JACKS, BUMP_DELAY, BUMP_JACKS };
	static JACKS JacksBeingMoved = JACKS_NONE;

	IDS_CAN_DTC fault = NUM_IDS_CAN_DTCS;

	// control UI Display
	LevelerType3.Status.Outputs.LevelLED = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;

	if (Ground.finished) // nothing to do if finished, make sure the state code below is not executed
		return;
	
	if (ExcessAngle()) fault = DTC_EXCESS_ANGLE;                        // stop if we encounter an excessive angle
	else if (OutOfStroke) fault = GetOutOfStrokeDTC();
	else if (StateTimer > GROUND_DETECT_TIMEOUT) fault = DTC_GROUND_JACKS_TIMEOUT;  // time out if we do not detect ground
	
	if (fault < NUM_IDS_CAN_DTCS)
	{
		IDS_CAN_SetDTC(fault);
		return;
	}
	
	// wait for airbags to empty prior to extending
	if (Config.DiagnosticBlock.AirFeaturesEnabled && (Airbags.dumpTimer < AIRBAG_DUMP_TIME))
	{
		ConsoleDriver_AddLine(3, "Dumping air bags...");
		ConsoleDriver_ShowCountdown(4, AIRBAG_DUMP_TIME, Airbags.dumpTimer); 

		Panel_SetWaitLED(ATTN_BLINK_RATE);
		StateTimer = 0; 
		return;
	}
	else 
	{
		ConsoleDriver_AddLine(3, blankline);
		ConsoleDriver_AddLine(4, blankline);
	}
		
	// drive appropriate jacks (wait for sensor to stablize)
	switch (Ground.state)
	{
	default: NewState(STATE_FAULT, EVENT_STATE_RECOVERY); return;

	// get initial sensor reading
	case GROUND_DELAY:
		if (Ground.jacksToGround == JACKS_NONE)
		{
			// we're done grounding - move to bumping
			Ground.state = BUMP_DELAY;
			StateTimer = 0;
			return;
		}
		
		if (StateTimer < GROUND_SENSOR_DELAY)
			return;

		ResetGroundDetection(GROUND_DETECT_DEBOUNCE_1);

		// if more than 1 jack is selected, we either send both jacks to ground, then bump each to verify (this saves time, but uses more stroke)
		// or ground jacks one by one with no bump (this saves stroke but takes longer)
		
		// so... if not bumping, ground jacks one by one with lower sensitivity
		// else if bumping, ground both jacks with lower sensitivity until one hits.  Then "bump" each = ground with greater sensitvity
		if (Ground.jacksToBump == JACKS_NONE) JacksBeingMoved = GetFirstJack(Ground.jacksToGround);
		else JacksBeingMoved = Ground.jacksToGround;

		break; // next state

	// ground any ungrounded jacks
	case GROUND_JACKS:
		DriveJacks(JacksBeingMoved, DIRECTION_EXTEND); 
		
		// Noise/shaking may occur at pump startup, so drive jacks for at least 3 seconds prior to checking for ground.  
		if (StateTimer < SECONDS(3.0))
			return;
		
		if (JacksBeingMoved && !GroundDetected(GROUND_DETECT_THRESHOLD))
			return; // keep checking

		// done... the jack is bumped
		Ground.jacksToGround &= (JACKS)(~(uint8)(JacksBeingMoved));
		
		// move to previous state
		Ground.state--;
		StateTimer = 0;
		return;
				
	// determine next jack to bump
	case BUMP_DELAY:

		if (JacksBeingMoved == JACKS_NONE)
		{
			// we're done bumping
			Ground.finished = TRUE;
			JacksOnGround |= Ground.finishedJackFlags;
			StateTimer = 0;
			return;
		}

		// wait before bumping the jack
		if (StateTimer < GROUND_SENSOR_DELAY) return;
		ResetGroundDetection(GROUND_DETECT_DEBOUNCE_2);
		JacksBeingMoved = GetFirstJack(Ground.jacksToBump); // get the jack name

		ConsoleDriver_AddLine(1, "Verifying ground contact");
	
		// next state
		break;

	// bump the jack
	case BUMP_JACKS:
		if (JacksBeingMoved)
		{
			if (!GroundDetected(GROUND_DETECT_THRESHOLD))
			{
				DriveJacks(JacksBeingMoved, DIRECTION_EXTEND);
				return;
			}

			// done... the jack is bumped
			Ground.jacksToBump &= (JACKS)(~(uint8)(JacksBeingMoved));
		}

		// move to next state
		Ground.state--;
		StateTimer = 0;
		return;
	}

	Ground.state++; // go to the next state when we get here
	StateTimer = 0; // make sure to clear the state timer
}

//////////
//////////

#pragma MESSAGE DISABLE C5703
static void GROUND_JACKS_1_OnEnter(STATE prev)
{
	JACKS jacks = (Y_AXIS < 0) ? JACKS_FRONT : JACKS_REAR;
	GroundJacks_OnEnter(jacks);
	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void GROUND_JACKS_1(void)
{
	GroundJacks();
	if (Ground.finished)
		NewState(STATE_AUTO_LEVEL_FIRST_AXIS, EVENT_JACKS_ON_GROUND);
}

#pragma MESSAGE DISABLE C5703
static void GROUND_JACKS_2_OnEnter(STATE prev)
{
	JACKS jacks = (JACKS)(~(uint8)JacksOnGround) & JACKS_ALL;
	GroundJacks_OnEnter(jacks);
	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void GROUND_JACKS_2(void)
{
	GroundJacks();
	if (Ground.finished)
		NewState(STATE_AUTO_LEVEL, EVENT_JACKS_ON_GROUND);
}

//////////
//////////
//////////
//////////

#pragma MESSAGE DISABLE C5703
static void GROUND_AFTER_LEVEL_OnEnter(STATE prev)
{
	ConsoleDriver_AddLine(0, autoleveltext);
	ConsoleDriver_AddLine(1, "Verifying Ground Contact");
	ConsoleDriver_AddLine(4, remainstill);
	
	Ground.state = 0;
	Ground.bumpTimer = 0;
	ResetGroundDetection(REGROUND_DETECT_DEBOUNCE);

	if (JacksToGroundAfterLevel == JACKS_NONE)
		Ground.delay = GROUND_SENSOR_DELAY;
	else Ground.delay = 0;

	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void GROUND_AFTER_LEVEL(void)
{
	// control UI Display
	LevelerType3.Status.Outputs.LevelLED = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;
	
	// handle timeout
	if (StateTimer > (GROUND_DETECT_TIMEOUT * 1.5))
	{
		IDS_CAN_SetDTC(DTC_GROUND_JACKS_TIMEOUT);
		return;
	}

	// delay gets set when ground detection completes
	if (Ground.delay)
	{
		ConsoleDriver_Reset();
		ConsoleDriver_AddLine(0, autoleveltext);
		ConsoleDriver_AddLine(2, "Success!");

		// change state when delay expires
		if (!--Ground.delay)
			NewState(STATE_IDLE, EVENT_SEQUENCE_COMPLETE);

		// return so we don't continue to drive the jack
		return;
	}

	if (++Ground.bumpTimer < BUMP_ON_TIME)
		DriveJacks(JacksToGroundAfterLevel, DIRECTION_EXTEND);
	else if (Ground.bumpTimer > BUMP_PERIOD)
		Ground.bumpTimer = 0;
	
	// do ground detection of 1 jack, which could have been lifted off the ground during level
	if (GroundDetected(REGROUND_DETECT_THRESHOLD))
	{
		Ground.delay = GROUND_SENSOR_DELAY;
	}
}



//////////
//////////
//////////
//////////

static uint16 GetHysteresis(void)
{
	uint16 hysteresis = LEVEL_HYSTERESIS;
	if (AutoLevel.Attempts <= 4)
		hysteresis >>= 1;
	return hysteresis;
}

static void GetLevelAxisAndDetermineStopAngle(void)
{
	int16 angle = Y_AXIS;

	AutoLevel.Jacks = JACKS_NONE;
	
	// get worst case angle/axis
	if (State == STATE_AUTO_LEVEL_FIRST_AXIS)
	{
		AutoLevel.Jacks = JacksOnGround;    
	}
	else if (Abs(X_AXIS) > Abs(angle))
	{
		angle = X_AXIS;
		
		
		// the first axis to lift may cause the opposite side rear jack to come off the ground.  At the end of level, 
		// need to reground the opposite side rear jack.
		if (X_AXIS < 0)
		{
			AutoLevel.Jacks = JACKS_RIGHT;
			
			if (JacksToGroundAfterLevel == JACKS_NONE)
				JacksToGroundAfterLevel= JACKS_LR; 
		}
		else
		{
			AutoLevel.Jacks = JACKS_LEFT;
			
			if (JacksToGroundAfterLevel == JACKS_NONE)
				JacksToGroundAfterLevel= JACKS_RR;
		}
	}
	else 
	{
		if (Y_AXIS < 0) AutoLevel.Jacks = JACKS_FRONT;
		else AutoLevel.Jacks = JACKS_REAR;   
	}
	
	// determine target stop angle
	if (angle < 0)
	{
		angle = -angle;
		// angle must be increased
		if ((uint16)angle > GetHysteresis())
		{
			angle >>= 1;
			if (angle > LEVEL_HYSTERESIS) angle = ANGLE(0.250); // limit the target angle
			AutoLevel.StopAngle = -angle; // target angle depends on direction
		}
	}
	else if (angle > 0)
	{
		// angle must be decreased
		if ((uint16)angle > GetHysteresis())
		{
			angle >>= 1;
			if (angle > LEVEL_HYSTERESIS) angle = ANGLE(0.250); // limit the target angle
			AutoLevel.StopAngle = angle; // target angle depends on direction
		}
	}

	// display jacks that will move, determine zero approach direction
	switch (AutoLevel.Jacks)
	{
	//recovery
	default: //lint -fallthrough
	case JACKS_NONE:
	case JACKS_LF:
	case JACKS_RF:
	case JACKS_LR:
	case JACKS_RR:
	case JACKS_ALL:
		AutoLevel.State = 0;
		return;
	case JACKS_LEFT:  ConsoleDriver_AddLine(2, "Extending left jacks");  AutoLevel.Approach = 1; break;
	case JACKS_RIGHT: ConsoleDriver_AddLine(2, "Extending right jacks"); AutoLevel.Approach = -1; break;
	case JACKS_REAR:  ConsoleDriver_AddLine(2, "Extending rear jacks");  AutoLevel.Approach = 1; break;
	case JACKS_FRONT: ConsoleDriver_AddLine(2, "Extending front jacks"); AutoLevel.Approach = -1; break;
	}
}

static uint8 AxisIsLevel(int16 angle)
{
	// check to see if we've exceeded the stopping point?
	if (AutoLevel.Approach <= 0)
	{
		// angle is less than zero, and increasing
		if (angle >= AutoLevel.StopAngle)
			return TRUE; // not done
	}
	else
	{
		// angle is greater than zero, and decreasing
		if (angle < AutoLevel.StopAngle)
			return TRUE; // not done
	}

	return FALSE;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma MESSAGE DISABLE C5703
static void AUTO_LEVEL_FIRST_AXIS_OnEnter(STATE prev)
{
	AutoLevel.State = 0;

	if (ExcessAngle())
		NewState(STATE_FAULT, EVENT_EXCESS_ANGLE_AUTO);

	ConsoleDriver_AddLine(0, autoleveltext);
	ConsoleDriver_AddLine(2, analyzingtext);
	
	//lint -e(715)   prev is not used in the function       
}
#pragma MESSAGE DEFAULT C5703

static void AUTO_LEVEL_FIRST_AXIS(void)
{
	IDS_CAN_DTC fault = NUM_IDS_CAN_DTCS;
	
	// control UI Display
	LevelerType3.Status.Outputs.LevelLED = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;
	
	if (OutOfStroke) fault = GetOutOfStrokeDTC();
	else if (ExcessAngle()) fault = DTC_EXCESS_ANGLE;                       // stop if we encounter an excessive angle

	if (fault < NUM_IDS_CAN_DTCS)
	{
		IDS_CAN_SetDTC(fault);
		return;
	}
	
	// drive appropriate motors (wait for sensor to stablize)
	switch (AutoLevel.State)
	{
	default: // restart / iterate
		AutoLevel.State = 0;
		StateTimer = 0;
		return;

	case 0: // init auto level
		// let tilt sensor stabilize
		if (Jack_GetIdleTime() < SENSOR_STABILIZE_TIME)
			return;

		// start leveling
		break;

/////////////
/////////////
/////////////
/////////////

	case 1: 
		// which jacks are on ground?
		GetLevelAxisAndDetermineStopAngle();
		break;
		
	case 2: 
		// level axis
		// drive proper jacks
		DriveJacks(AutoLevel.Jacks, DIRECTION_EXTEND);
		if (AxisIsLevel(Y_AXIS)) break;     // done. Advance state
		return; // not done
		
	case 3: // done
		ConsoleDriver_AddLine(2, analyzingtext);
		NewState(STATE_GROUND_JACKS_2, EVENT_SEQUENCE_COMPLETE);
		return;
	}

	AutoLevel.State++;
	StateTimer = 0;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma MESSAGE DISABLE C5703
static void AUTO_LEVEL_OnEnter(STATE prev)
{
	AutoLevel.State = 0;
	AutoLevel.Attempts = 0;
	AutoLevel.ElapsedTime = 0;
	JacksToGroundAfterLevel = JACKS_NONE;
	
	if (ExcessAngle())
		NewState(STATE_FAULT, EVENT_EXCESS_ANGLE_AUTO);

	ConsoleDriver_AddLine(0, autoleveltext);
	ConsoleDriver_AddLine(2, analyzingtext);
	
	//lint -e(715)   prev is not used in the function       
}
#pragma MESSAGE DEFAULT C5703

static void AUTO_LEVEL(void)
{
	IDS_CAN_DTC fault = NUM_IDS_CAN_DTCS;
	uint8 XisLevel, YisLevel;
	uint16 XLimit, YLimit;
	
	// control UI Display
	LevelerType3.Status.Outputs.LevelLED = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;
	
	// mode timeout
	if (++AutoLevel.ElapsedTime > AUTO_LEVEL_TIMEOUT) fault = DTC_AUTO_LEVEL_TIMEOUT;
	// too many attempts, something is wrong
	else if (AutoLevel.Attempts > MAX_LEVEL_ATTEMPTS) fault = DTC_AUTO_LEVEL_FAIL;
	// check for loss of stroke
	else if (OutOfStroke) fault = GetOutOfStrokeDTC();
	// make sure we have not reached an extreme angle
	else if (ExcessAngle()) fault = DTC_EXCESS_ANGLE;

	if (fault < NUM_IDS_CAN_DTCS)
	{
		IDS_CAN_SetDTC(fault);
		return;
	}
	
	// determine level limits
	XLimit = LEVEL_X_AXIS;
	YLimit = LEVEL_Y_AXIS;
	// first 4 attempts -- try to level very precisely
	if (AutoLevel.Attempts < 4) { XLimit >>= 1; YLimit >>= 1; }
		
	// determine how much error we have
	XisLevel = (uint8) (Abs(X_AXIS) < XLimit);
	YisLevel = (uint8) (Abs(Y_AXIS) < YLimit);
	
	if (XisLevel && YisLevel && (AutoLevel.State < 0x80))
		AutoLevel.State = 0x80; // "finished state"
	
	// drive appropriate motors (wait for sensor to stablize)
	switch (AutoLevel.State)
	{
	default: // restart / iterate
		AutoLevel.State = 0;
		StateTimer = 0;
		return;

	// init auto level
	 case 0:
		// let tilt sensor stabilize
		if (Jack_GetIdleTime() < SENSOR_STABILIZE_TIME)
			return;

		// start leveling
		break;

	// check worst case angle
	case 1: 
		GetLevelAxisAndDetermineStopAngle();
		break;
		
	// level axis
	case 2: 
		{
			// which angle do we need to monitor?
			int16 angle = Y_AXIS;
			
			if ((AutoLevel.Jacks == JACKS_LEFT) || (AutoLevel.Jacks == JACKS_RIGHT))
				angle = X_AXIS;

			// done. Advance state
			if (AxisIsLevel(angle))
				break;
			
			// drive proper jacks
			DriveJacks(AutoLevel.Jacks, DIRECTION_EXTEND);

			return; // not done
		}
	
	// let sensor stabilize 
	case 3:
		ConsoleDriver_AddLine(2, analyzingtext);
		AutoLevel.Attempts++; // increment attempt counter
		AutoLevel.State = 0; // restart the leveling sequence
		return;

	case 0x80: // done!
	case 0x81: // done!
		LevelStatus = LEVEL_OK; // ensure that proper LEDs light
		break;
	
	case 0x82: // wait 4 seconds
		if (StateTimer < SECONDS(1))
			LevelerType3.Status.Outputs.Buzzer = (uint8) LEVELER_TYPE_3_OUTPUT_ON;
		if (StateTimer > SECONDS(4))
			NewState(STATE_GROUND_AFTER_LEVEL, EVENT_SEQUENCE_COMPLETE);
		return;
	}

	AutoLevel.State++;
	StateTimer = 0;
}

//////////
//////////
//////////
//////////

#pragma MESSAGE DISABLE C5703
static void MANUAL_LEVEL_OnEnter(STATE prev)
{
	Manual.newPumpDirection = DIRECTION_EXTEND;
	Manual.timer = 0;
	Manual.runTimeout = MANUAL_MODE_JACK_TIMEOUT;

	ConsoleDriver_AddLine(0, "MANUAL MODE");
	LatchDirection = DIRECTION_EXTEND;

	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void MANUAL_LEVEL(void)
{
	JACK_STRUCT j = { JACKS_NONE, DIRECTION_HALT };
	static int16 Hysteresis = 0;
	uint8 over_limit = FALSE; 

	// control LEDs
	if (UISwitchOnEvent.Back)
	{
		NewState(STATE_IDLE, EVENT_UI_REQUEST);
		return;
	}

	// don't allow any manual operation if low voltage
	if (LowVoltage)
	{
		ConsoleDriver_AddLine(1, "Low Voltage");
		return;
	}

	// get the jack request
	j = DetermineManualJacksToDrive();
	
	if (j.Dir) Manual.newPumpDirection = j.Dir;
	
	// if extending need to dump airbags, if retracting need to fill
	if (Config.DiagnosticBlock.AirFeaturesEnabled)
	{
		if ((Manual.newPumpDirection == DIRECTION_EXTEND) && (Airbags.dumpTimer < AIRBAG_DUMP_TIME))
		{
			ConsoleDriver_AddLine(4,"Dumping airbag");
			ConsoleDriver_ShowCountdown(5, AIRBAG_DUMP_TIME, Airbags.dumpTimer); 
			
			Panel_SetWaitLED(ATTN_BLINK_RATE);
			return;
		}
		else if ((Manual.newPumpDirection == DIRECTION_RETRACT) && (Airbags.fillTimer < Config.UserBlock.AirBagPrefillTime))
		{
			ConsoleDriver_AddLine(4,"Filling airbag");
			ConsoleDriver_ShowCountdown(5, Config.UserBlock.AirBagPrefillTime, Airbags.fillTimer); 
			Panel_SetWaitLED(ATTN_BLINK_RATE);
			return;
		}
		else
		{
			ConsoleDriver_AddLine(4, blankline);
			ConsoleDriver_AddLine(5, blankline);
		}
	}
	
	
	// determine if we are past angle limits
	// y-axis = front to back, x-axis = side to side
	if (X_AXIS > ((int16) (MANUAL_MODE_X_ANGLE_LIMIT - Hysteresis)))
		over_limit = (uint8) (((j.Dir == DIRECTION_EXTEND) && (j.Jacks == JACKS_RIGHT)) || ((j.Dir == DIRECTION_RETRACT) && (j.Jacks == JACKS_LEFT)));
	else if (X_AXIS < ((int16) (-MANUAL_MODE_X_ANGLE_LIMIT + Hysteresis)))
		over_limit = (uint8) (((j.Dir == DIRECTION_EXTEND) && (j.Jacks == JACKS_LEFT)) || ((j.Dir == DIRECTION_RETRACT) && (j.Jacks == JACKS_RIGHT)));
	
	ConsoleDriver_AddLine(5, blankline);

	if (over_limit)
	{
		j.Jacks = JACKS_NONE;
		Hysteresis = MANUAL_MODE_HYSTERESIS; 
		ConsoleDriver_AddLine(5, "Excessive Angle");
		LevelerType3.Status.Outputs.Buzzer = (uint8) LEVELER_TYPE_3_OUTPUT_BLINK;
		Panel_SetSlopeLED(FAULT_BLINK_RATE);
		return;
	}
	else Hysteresis = 0;

	// determine direction to drive jacks 
	if (j.Jacks == JACKS_NONE)
		j.Dir = DIRECTION_HALT;

	// prevent user from running jacks for too long
	if (j.Jacks)
	{
		if (Manual.runTimeout) Manual.runTimeout--;
		else j.Jacks = JACKS_NONE; 
	}
	else Manual.runTimeout = MANUAL_MODE_JACK_TIMEOUT;

	// drive jacks
	DriveJacks(j.Jacks, j.Dir);
	ConsoleDriver_DisplayJacksThatAreMoving(1);
	ConsoleDriver_ShowAngle(2);
}

//////////
//////////
//////////
//////////

#pragma MESSAGE DISABLE C5703
static void AUTO_RETRACT_OnEnter(STATE prev)
{
	ConsoleDriver_AddLine(0, "AUTO RETRACT");
	ConsoleDriver_AddLine(2, "Retracting all jacks");
	AutoRetract.preFill = TRUE;
	
	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void AUTO_RETRACT(void)
{
	uint16 timeout = AUTO_RETRACT_TIMEOUT;
	
	if (Config.DiagnosticBlock.AirFeaturesEnabled && AutoRetract.preFill) timeout += Config.UserBlock.AirBagPrefillTime;

	// error on timeout occurs
	if (StateTimer > timeout)
	{
		IDS_CAN_SetDTC(DTC_AUTO_RETRACT_TIMEOUT);

		// set the error here - but let CheckErrors() handle error mode enter/exit
		Config.DiagnosticBlock.LatchedRetractError = TRUE;
		Config_DiagnosticBlock_Flush();
		return;
	}

	if (AutoSource == SOURCE_PARK_BRAKE)
	{
		Panel_SetOutputs(AUTO_BLINK_RATE);
		Panel_SetBuzzer(ATTN_BLINK_RATE);
	}
	
	if (Config.DiagnosticBlock.AirFeaturesEnabled && AutoRetract.preFill && (Airbags.fillTimer < Config.UserBlock.AirBagPrefillTime))
	{
		ConsoleDriver_AddLine(3,"Filling airbag");
		ConsoleDriver_ShowCountdown(4, Config.UserBlock.AirBagPrefillTime, Airbags.fillTimer); 
		return;
	}

	ConsoleDriver_AddLine(3, blankline);
	ConsoleDriver_AddLine(4, blankline);

	if (!Input.Digital.ParkBrakeIsOn)
		ConsoleDriver_AddLine(4, "Park brake not set");
	
	// retract all jacks
	DriveJacks(JACKS_ALL, DIRECTION_RETRACT);

	// wait before checking the PSI switch.  It is possible that the psi switch has opened without all jacks being retracted.
	// allow the system to depressureize before making the assumption that all jacks are up
	if (StateTimer < SECONDS(4.0))
		return;

	// check for all jacks up
	if (Input.Digital.JacksAllUp)
	{
		Config.DiagnosticBlock.FullRetractByUser = TRUE;
		Config_DiagnosticBlock_Flush();
		NewState(STATE_IDLE, EVENT_SEQUENCE_COMPLETE);
	}
}

//////////
//////////
//////////
//////////

#pragma MESSAGE DISABLE C5703
static void FAULT_OnEnter(STATE prev)
{
	IDS_CAN_DTC dtc;

	FaultMode.buzzer = 0;
	FaultMode.activeDTC = NUM_IDS_CAN_DTCS;

	ConsoleDriver_AddLine(0, "**** ERROR  ****");
	ConsoleDriver_AddLine(2, "Press OK");
	ConsoleDriver_AddLine(3, "to acknowledge");

	for (dtc = (IDS_CAN_DTC)0; dtc < NUM_IDS_CAN_DTCS; dtc++)
	{
		if (IDS_CAN_IsDTCActive(dtc))
		{
			FaultMode.activeDTC = dtc;
			break;
		}
	}

	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void FAULT(void)
{
	static uint16 timer = 0;
	const char * line1 = "";

	LevelerType3_SetAllStatusOutputs(LEVELER_TYPE_3_OUTPUT_OFF);
	Panel_SetManualLED(ATTN_BLINK_RATE);
	Panel_SetAutoLED(ATTN_BLINK_RATE);
	Panel_SetWaitLED(ATTN_BLINK_RATE);
	Panel_SetFrontLED(ATTN_BLINK_RATE);
	Panel_SetRearLED(ATTN_BLINK_RATE);

	switch (FaultMode.activeDTC)
	{
		default:
		case NUM_IDS_CAN_DTCS:
			break;
															   //1234567890123456
		case DTC_ECU_IS_DEFECTIVE:                      line1 = "Bad Calibration "; break;
		case DTC_BATTERY_VOLTAGE_LOW:                   line1 = "Low Voltage     "; break;
		case DTC_LEVELER_ZERO_POINT_NOT_CONFIGURED:     line1 = "No Zero Pt set  "; break;
		case DTC_JACK_LF_OUT_OF_STROKE:                 line1 = "LF Out of stroke"; break;
		case DTC_JACK_LR_OUT_OF_STROKE:                 line1 = "LR Out of stroke"; break;
		case DTC_JACK_RF_OUT_OF_STROKE:                 line1 = "RF Out of stroke"; break;
		case DTC_JACK_RR_OUT_OF_STROKE:                 line1 = "RR Out of stroke"; break;
		case DTC_AUTO_LEVEL_TIMEOUT:                    line1 = "Auto Timeout    "; break;
		case DTC_AUTO_LEVEL_FAIL:                       line1 = "Auto Level Fail "; break;
		case DTC_AUTO_RETRACT_TIMEOUT:                  line1 = "Retract Timeout "; break;
		case DTC_GROUND_JACKS_TIMEOUT:                  line1 = "Ground Timeout  "; break;
		case DTC_EXCESS_ANGLE:                          line1 = "Excess Angle    "; break;
		case DTC_AUTO_START_VOLTAGE_LOW:                line1 = "Low Voltage     "; break;
		case DTC_OPERATING_VOLTAGE_DROPOUT:             line1 = "Low Voltage     "; break;

		case DTC_TOUCH_PAD_COMM_FAILURE:
			line1 = "Touchpad Comm   ";

			if (!Config.DiagnosticBlock.TouchpadErrorDisplayed)
			{
				Config.DiagnosticBlock.TouchpadErrorDisplayed = TRUE;
				Config_DiagnosticBlock_Flush();
			}
			break;

		case DTC_USER_PANIC_STOP:
			ConsoleDriver_AddLine(0, "Function Aborted");
			line1 = "Check Components";
			break;

		case DTC_PARK_BRAKE_NOT_ENGAGED:
			ConsoleDriver_AddLine(0, "Apply park brake to");
			line1 = "use this feature";
			break;

		case DTC_PSI_SWITCH_TIMEOUT:
									//1234567890123456789012345
			ConsoleDriver_AddLine(1, "Retract error! Hoses or");
			ConsoleDriver_AddLine(2, "pressure switch may have");
			ConsoleDriver_AddLine(3, "malfunctioned. Leveling");
			ConsoleDriver_AddLine(4, "system may require");
			ConsoleDriver_AddLine(5, "service");

			Panel_SetWaitLED(FAULT_BLINK_RATE);
			Panel_SetJacksDownLED(FAULT_BLINK_RATE);
			Panel_SetParkBrakeLED(FAULT_BLINK_RATE);
			Panel_SetSlopeLED(FAULT_BLINK_RATE);

			// assert buzzer only for first miunute of this error
			if (StateTimer > SECONDS(60))
				LevelerType3.Status.Outputs.Buzzer = (uint8)LEVELER_TYPE_3_OUTPUT_OFF;

			{
				EVENT clear = EVENT_NONE;

				if (Panel.Input.Left && Panel.Input.Right && Panel.Input.Front && Panel.Input.Rear)
					clear = EVENT_TP_REQUEST;

				if (LevelerType3.Input.Buttons.Enter)
				{
					// clear the retract error with a "special" button press
					if (++timer > SECONDS(4.0))
						clear = EVENT_UI_REQUEST;
				}
				else timer = 0;

				if (clear)
				{
					Config.DiagnosticBlock.LatchedRetractError = FALSE;
					Config_DiagnosticBlock_Flush();
					LeakyHoseTimer = 0;
					LeakyHoseCounter = 0;
					NewState(STATE_IDLE, clear);
				}
			}

			return;
	}

	ConsoleDriver_AddLine(1, line1);

	// periodically buzz to get the users attention
	// beep for 1 second out of every 10
	if (++FaultMode.buzzer < SECONDS(1.0))
		LevelerType3.Status.Outputs.Buzzer = (uint8) LEVELER_TYPE_3_OUTPUT_ON;
	else if (FaultMode.buzzer > SECONDS(10.0))
		FaultMode.buzzer = 0;

	// enter button (error mode) enabled 1 second after entering mode
	if (StateTimer > SECONDS(1.0))
	{
		if (UISwitchOnEvent.Enter || PanelSwitchOnEvent.RetractAll)
		{
			IDS_CAN_ClearDTC(DTC_LEVELER_ZERO_POINT_NOT_CONFIGURED);
			IDS_CAN_ClearDTC(DTC_TOUCH_PAD_COMM_FAILURE);
			IDS_CAN_ClearDTC(DTC_JACK_LF_OUT_OF_STROKE);
			IDS_CAN_ClearDTC(DTC_JACK_LR_OUT_OF_STROKE);
			IDS_CAN_ClearDTC(DTC_JACK_RF_OUT_OF_STROKE);
			IDS_CAN_ClearDTC(DTC_JACK_RR_OUT_OF_STROKE);
			IDS_CAN_ClearDTC(DTC_AUTO_LEVEL_TIMEOUT);
			IDS_CAN_ClearDTC(DTC_AUTO_LEVEL_FAIL);
			IDS_CAN_ClearDTC(DTC_AUTO_RETRACT_TIMEOUT);
			IDS_CAN_ClearDTC(DTC_GROUND_JACKS_TIMEOUT);
			IDS_CAN_ClearDTC(DTC_EXCESS_ANGLE);
			IDS_CAN_ClearDTC(DTC_USER_PANIC_STOP);
			IDS_CAN_ClearDTC(DTC_PARK_BRAKE_NOT_ENGAGED);
			IDS_CAN_ClearDTC(DTC_PSI_SWITCH_TIMEOUT);
			IDS_CAN_ClearDTC(DTC_AUTO_START_VOLTAGE_LOW);
			IDS_CAN_ClearDTC(DTC_OPERATING_VOLTAGE_DROPOUT);

			NewState(STATE_IDLE, EVENT_UI_REQUEST);
		}
	}
}

//////////
//////////
//////////
//////////

#pragma MESSAGE DISABLE C5703
static void ZERO_MODE_OnEnter(STATE prev)
{
	ZeroMode.state = 0xFF; // invalid - restart
	ZeroMode.airState = 0;
	
	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static uint8 ConfigureAirFeatures(void)
{
	switch(ZeroMode.airState)
	{
	default: ZeroMode.airState = 0;
	//lint -fallthrough
	case 0: 
		Deassert_AIR_DUMP_CONTROL;
		Deassert_AIR_FILL_CONTROL;
		
		ConsoleDriver_AddLine(0, "Configuring air features");
		ConsoleDriver_AddLine(1, pleasewait);
		ZeroMode.airState++;
		//lint -fallthrough
	
	case 1:
		Assert_AIR_FILL_CONTROL;
		Deassert_AIR_DUMP_CONTROL;
		
		if (StateTimer < SECONDS(5.0)) return FALSE;
		
		if (Input.Digital.AirFillStatus)    ZeroMode.airState++;
		else ZeroMode.airState = 4;
		break;
	
	case 2: 
		Assert_AIR_DUMP_CONTROL;
		Deassert_AIR_FILL_CONTROL;
		
		if (StateTimer < SECONDS(5.0)) return FALSE;
		
		if (Input.Digital.AirDumpStatus)    ZeroMode.airState++;
		else ZeroMode.airState = 4;
		break;
	
	case 3:
		Deassert_AIR_DUMP_CONTROL;
		Deassert_AIR_FILL_CONTROL;
		Config.DiagnosticBlock.AirFeaturesEnabled = TRUE;
		Config_UserBlock_Flush();
		return TRUE;
	
	case 4:
		Deassert_AIR_DUMP_CONTROL;
		Deassert_AIR_FILL_CONTROL;
		Config.DiagnosticBlock.AirFeaturesEnabled = FALSE;
		Config_UserBlock_Flush();
		return TRUE;
	}

	StateTimer = 0;
	return FALSE;
}

static void ZERO_MODE(void)
{
	JACK_STRUCT j = { JACKS_NONE, DIRECTION_HALT };
	
	switch (ZeroMode.state)
	{
	default: // recovery / restart
		ConsoleDriver_AddLine(0, "** ZERO POINT **");
		ConsoleDriver_AddLine(1, "* CALIBRATION **");
		ConsoleDriver_AddLine(2, "Press Enter");
		ConsoleDriver_AddLine(3, "to set");
		ZeroMode.state = 0;
		StateTimer = 0;
		//lint -fallthrough
	
	case 0: // start/restart
		LevelerType3.Status.Outputs.Buzzer = (uint8) LEVELER_TYPE_3_OUTPUT_ON;
		Panel_SetBuzzer(TRUE);
		if (StateTimer < SECONDS(0.75))
			return;
		break; // next sub state

	case 1: // wait for ENTER button
		if (UISwitchOnEvent.Back)
		{
			NewState(STATE_OFF, EVENT_UI_REQUEST);
			return;
		}

		LevelerType3_SetAllStatusOutputs(LEVELER_TYPE_3_OUTPUT_BLINK);
		LevelerType3.Status.Outputs.Buzzer = (uint8) LEVELER_TYPE_3_OUTPUT_OFF;
		LevelerType3.Status.Outputs.ExtendLED = (uint8) ((LatchDirection == DIRECTION_EXTEND) ? LEVELER_TYPE_3_OUTPUT_ON : LEVELER_TYPE_3_OUTPUT_OFF);
		LevelerType3.Status.Outputs.RetractLED = (uint8) ((LatchDirection == DIRECTION_RETRACT) ? LEVELER_TYPE_3_OUTPUT_ON : LEVELER_TYPE_3_OUTPUT_OFF);

		// allow user to level the coach
		j = DetermineManualJacksToDrive();

		if (j.Jacks == JACKS_NONE)
			j.Dir = DIRECTION_HALT;

		DriveJacks(j.Jacks, j.Dir);
		
		if (((PanelSwitchOnEvent.Auto && Panel.Input.RetractAll) || (PanelSwitchOnEvent.RetractAll && Panel.Input.Auto)) || UISwitchOnEvent.Enter)
		{
			// prepare for stability check
			ZeroMode.sampleCount = 0;
			ZeroMode.Y.avg = 0; ZeroMode.Y.min = ZeroMode.Y.max = Y_AXIS;
			ZeroMode.X.avg = 0; ZeroMode.X.min = ZeroMode.X.max = X_AXIS;

			ConsoleDriver_Reset();
			ConsoleDriver_AddLine(0, "Zero point");
			ConsoleDriver_AddLine(1, "stability check ");
			ConsoleDriver_AddLine(2, pleasewait);
			break; // next sub state
		}
		return;
		
	case 2: // take samples
		LevelerType3_SetAllStatusOutputs(LEVELER_TYPE_3_OUTPUT_OFF);
		Panel_SetOutputs(FALSE);
		Panel_SetWaitLED(ATTN_BLINK_RATE); // indicate we are thinking
		
		#define UPDATE_SAMPLE(name, reading)       \
		{                                          \
			int16 val = reading;                   \
			ZeroMode.name.avg += val;    \
			if (ZeroMode.name.min > val) \
				ZeroMode.name.min = val; \
			if (ZeroMode.name.max < val) \
				ZeroMode.name.max = val; \
		}

		UPDATE_SAMPLE(X, Sensor.X.Filter)
		UPDATE_SAMPLE(Y, Sensor.Y.Filter)

		// take samples
		if (++ZeroMode.sampleCount < 512)
			return;

		// sanity check
		if (ZeroMode.sampleCount != 512)
		{
			ZeroMode.state = 0xFF; // start over!
			return;
		}

		// analysis

		// calculate average
		ZeroMode.X.avg >>= 9;
		ZeroMode.Y.avg >>= 9;

		// store zero point
		Config.UserBlock.XZero = (int16) ZeroMode.X.avg;
		Config.UserBlock.YZero = (int16) ZeroMode.Y.avg;
		Config.UserBlock.ZeroValid = TRUE;
		Config_UserBlock_Flush();

		// indicate success to user
		ConsoleDriver_Reset();
		ConsoleDriver_AddLine(0, "Zero point");
		ConsoleDriver_AddLine(1, "set!");
		break; // next sub state

	case 3: // delay to show text before exiting mode
		if (StateTimer < SECONDS(0.75))
		{
			LevelerType3.Status.Outputs.Buzzer = (uint8) LEVELER_TYPE_3_OUTPUT_ON;
			Panel_SetBuzzer(TRUE);
		}
		if (StateTimer > SECONDS(1.0))  break;
		return;

	// determine if airdump/airfill are present
	case 4:
		Panel_SetOutputs(FALSE);
		Panel_SetWaitLED(ATTN_BLINK_RATE); // indicate we are thinking
		if (ConfigureAirFeatures())
		{
			Airbags.state = AIRBAG_STATE_OFF;
			
			if (Config.DiagnosticBlock.AirFeaturesEnabled)
				NewState(STATE_AIRBAG_TIME_CONFIG, EVENT_SEQUENCE_COMPLETE);
			else
				NewState(STATE_OFF, EVENT_SEQUENCE_COMPLETE);
		}
		return;
			
	}
	// next sub-state
	ZeroMode.state++;
	StateTimer = 0;
}

//////////
//////////
//////////
//////////

#pragma MESSAGE DISABLE C5703
static void SETUP_OnEnter(STATE prev)
{
	ConsoleDriver_AddLine(0, "SET UP");
	ConsoleDriver_AddLine(1, linebreak);
	ConsoleDriver_AddLine(2, "Zero Mode");
	ConsoleDriver_AddLine(3, "Press Enter");
	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void SETUP(void)
{
	if (UISwitchOnEvent.Back)
	{
		NewState(STATE_IDLE, EVENT_UI_REQUEST);
		return;
	}

	ConsoleDriver_AddLine(2, "Zero Mode");

	if (UISwitchOnEvent.Enter)
		NewState(STATE_ZERO_MODE, EVENT_UI_REQUEST);
}

//////////
//////////
//////////
//////////

#pragma MESSAGE DISABLE C5703
static void MENU_OnEnter(STATE prev)
{
	Menu.state = 0;

	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void MENU(void)
{
	enum { MENU_SHOW_ANGLE = 0, MENU_SHOW_BUILD, MENU_NUM_MENU_ITEMS };

	if (UISwitchOnEvent.Back)
	{
		NewState(STATE_IDLE, EVENT_UI_REQUEST);
		return;
	}

	if (UISwitchOnEvent.MenuUp) { Menu.state++; ConsoleDriver_ClearAllLines(); }
	else if (UISwitchOnEvent.MenuDown) { Menu.state--; ConsoleDriver_ClearAllLines(); }

	switch (Menu.state)
	{
	default:
		// advance text state, if wrap around depending on direction
		if (Menu.state > 0)
			Menu.state = 0;
		else Menu.state = MENU_NUM_MENU_ITEMS - 1;
		break;

	case MENU_SHOW_ANGLE:
		ConsoleDriver_AddLine(0, "Angle");
		ConsoleDriver_ShowAngle(1);

		if (Input.Digital.JacksAllUp) { ConsoleDriver_AddLine(4, "Jacks up"); ConsoleDriver_AddLine(5, "PSI switch open"); }
		else { ConsoleDriver_AddLine(4, "Jacks down"); ConsoleDriver_AddLine(5, "PSI switch closed"); }

		break;

	case MENU_SHOW_BUILD:
		ConsoleDriver_AddLine(0, &IDSSoftwarePartNumberString[0]);
		ConsoleDriver_AddLine(1, IDSCore_GetBuildDateString());
		ConsoleDriver_AddLine(2, IDSCore_GetBuildTimeString());
#if IDS_CAN_VERSION_NUMBER_LATEST == IDS_CAN_VERSION_NUMBER_3_0
		ConsoleDriver_AddLine(3, "IDS-CAN v3.0");
#else
	#define STRING2(x) #x
	#define STRING(x) STRING2(x)
		ConsoleDriver_AddLine(3, "IDS-CAN ver = " STRING(IDS_CAN_VERSION_NUMBER_LATEST));
#endif
		break;
	}
}

//////////
//////////
//////////
//////////

static void EMERGENCY_RETRACT_OnEnter(STATE prev)
{
	ConsoleDriver_AddLine(0, "Jacks up verification");
	ConsoleDriver_AddLine(2, "Retracting all jacks");
	ConsoleDriver_AddLine(4, "Park brake not set");

	PSI_Switch_Debounce = ER_PSI_SWITCH_DEBOUNCE;
	
	// if timer is active, increment the error counter
	if (LeakyHoseTimer)
	{
		// check to see if hose is broken (will cause recurring ER)
		if (++LeakyHoseCounter > 2)
		{
			// latch the error, to alert the user and stop pump use
			Config.DiagnosticBlock.LatchedRetractError = TRUE;
			Config_DiagnosticBlock_Flush();
			return;
		}   
	}
	
	LeakyHoseTimer = SECONDS(90);   
	
	// if off, remain off when complete
	ER.next = (STATE) ((prev == STATE_OFF) ? STATE_OFF : STATE_IDLE);
}

static void EMERGENCY_RETRACT(void)
{
	// allow 0.250s prior to alarming, so condition might "self-correct"
	if (StateTimer < SECONDS(0.250))
		return;
	
	// retract all jacks, blink all leds
	Panel_SetOutputs(AUTO_BLINK_RATE);
	Panel_SetBuzzer(FALSE);
	DriveJacks(JACKS_ALL, DIRECTION_RETRACT);
	
	if (Input.Digital.JacksAllUp)
	{
		NewState(ER.next, EVENT_SEQUENCE_COMPLETE);
		return;
	}

	// lockout pump if timeout
	if (StateTimer > SECONDS(5.0))
	{
		IDS_CAN_SetDTC(DTC_AUTO_RETRACT_TIMEOUT);

		// set the error here - but let CheckErrors() handle error mode enter/exit
		Config.DiagnosticBlock.LatchedRetractError = TRUE;
		Config_DiagnosticBlock_Flush();
		return;
	}
	
	// exit mode if user engages the park brake and acknowledges the error
	if (UISwitchOnEvent.Back && Input.Digital.ParkBrakeIsOn)
		NewState(STATE_IDLE, EVENT_UI_REQUEST);
}

//////////
//////////
//////////
//////////


#pragma MESSAGE DISABLE C5703
static void AIRBAG_TIME_CONFIG_OnEnter(STATE prev)
{
	AirBagTimeConfig.State = 0;
	AirBagTimeConfig.Set = AIRBAG_PREFILL_TIME_SHORT;

	//lint -e(715)   prev is not used in the function
}
#pragma MESSAGE DEFAULT C5703

static void AIRBAG_TIME_CONFIG(void)
{
	if (UISwitchOnEvent.Back)
	{
		NewState(STATE_IDLE, EVENT_UI_REQUEST);
		return;
	}

	// menu up/ menu down / right button cycle through options
	if (UISwitchOnEvent.MenuUp || UISwitchOnEvent.MenuDown || PanelSwitchOnEvent.Right) { Menu.state++; ConsoleDriver_ClearAllLines(); }

	switch (Menu.state & 1)
	{
	default:
		return;

	case 0:
		ConsoleDriver_AddLine(0, "Air Bags Detected");
		ConsoleDriver_AddLine(1, "Choose Air Prefill Time");
		ConsoleDriver_AddLine(3, "\x07 22 sec");
		ConsoleDriver_AddLine(4, "42 sec");     

		Panel_SetWaitLED(AUTO_BLINK_RATE);
		
		if (UISwitchOnEvent.Enter || PanelSwitchOnEvent.RetractAll)
		{
			AirBagTimeConfig.Set = AIRBAG_PREFILL_TIME_SHORT;
			break;
		}
		return;

	case 1:
		ConsoleDriver_AddLine(0, "Air Bags Detected");
		ConsoleDriver_AddLine(1, "Choose Air Prefill Time");
		ConsoleDriver_AddLine(3, "22 sec");
		ConsoleDriver_AddLine(4, "\x07 42 sec");        
				
		Panel_SetWaitLED(FAULT_BLINK_RATE);
		
		if (UISwitchOnEvent.Enter || PanelSwitchOnEvent.RetractAll)
		{
			AirBagTimeConfig.Set = AIRBAG_PREFILL_TIME_LONG;
			break;
		}
		return;
	}

	Config.UserBlock.AirBagPrefillTime = AirBagTimeConfig.Set;
	Config_UserBlock_Flush();
	NewState(STATE_OFF, EVENT_SEQUENCE_COMPLETE);
}

//////////
//////////
//////////
//////////


static void DriveAirBags(void)
{
	AIRBAG_STATE set = AIRBAG_STATE_OFF;
	
	if (Config.DiagnosticBlock.AirFeaturesEnabled)
	{
		switch (State)
		{
		// fill from these states if all jacks are up
		default:
		case STATE_OFF:
		case STATE_IDLE:
		case STATE_FAULT:
		case STATE_SETUP:
		case STATE_MENU:
		case STATE_AIRBAG_TIME_CONFIG:
			if (Input.Digital.ParkBrakeIsOn) Airbags.state = AIRBAG_STATE_OFF;
			else if (Input.Digital.JacksAllUp) Airbags.state = AIRBAG_STATE_FILL;
			break;
		
		case STATE_MANUAL_LEVEL:
			// need to fill before retract, dump before extend
			if (Manual.newPumpDirection == DIRECTION_RETRACT) Airbags.state = AIRBAG_STATE_FILL;
			else if (Manual.newPumpDirection == DIRECTION_EXTEND) Airbags.state = AIRBAG_STATE_DUMP;
			else if (Input.Digital.ParkBrakeIsOn) Airbags.state = AIRBAG_STATE_OFF;
			else if (Input.Digital.JacksAllUp) Airbags.state = AIRBAG_STATE_FILL;
			break;
		
		// always fill from these states
		case STATE_AUTO_RETRACT:
		case STATE_EMERGENCY_RETRACT:
			Airbags.state = AIRBAG_STATE_FILL;
			break;
		
		// always dump from these states
		case STATE_GROUND_JACKS_1:
		case STATE_AUTO_LEVEL_FIRST_AXIS:
		case STATE_GROUND_JACKS_2:
		case STATE_AUTO_LEVEL:
		case STATE_GROUND_AFTER_LEVEL:
			Airbags.state = AIRBAG_STATE_DUMP;
			break;
	
		case STATE_ZERO_MODE: // special case - zero mode configures air fill / dump.  Need to relinquish control to the state machine
			return;
		}
	
		if (!LowVoltage) set = Airbags.state;
	}
	
	// if airbags should be full, we need to constantly drive the output to keep them filling/full
	// emptying/dumping is different: output should only drive until they are empty
	switch (set)
	{
	default: //lint -fallthrough
	case AIRBAG_STATE_OFF:
		Deassert_AIR_DUMP_CONTROL;
		Deassert_AIR_FILL_CONTROL;
		Airbags.fillTimer = Airbags.dumpTimer = 0;
		break;
	
	case AIRBAG_STATE_FILL:
		Deassert_AIR_DUMP_CONTROL;
		Assert_AIR_FILL_CONTROL;
		if (Airbags.fillTimer < 0xFFFF) Airbags.fillTimer++;
		Airbags.dumpTimer = 0;
		break;
	
	case AIRBAG_STATE_DUMP:
		Deassert_AIR_FILL_CONTROL;
		
		if (Airbags.dumpTimer < AIRBAG_DUMP_TIME)
		{
			Airbags.dumpTimer++;
			Assert_AIR_DUMP_CONTROL;
		}
		else Deassert_AIR_DUMP_CONTROL;
		
		Airbags.fillTimer = 0;
		break;
	}
}

static void DriveStatusOutput(void)
{
	static uint16 timer = 0;
	
	// if latched fault - only blink for first minute after ignition startup
	if (Config.DiagnosticBlock.LatchedRetractError)
	{
		if (timer < SECONDS(60))
		{
			timer++;
			
			if ((uint8)timer < 20)          Assert_STATUS_OUTPUT_CONTROL;
			else if ((uint8)timer < 40)   Deassert_STATUS_OUTPUT_CONTROL;
			else if ((uint8)timer < 60)   Assert_STATUS_OUTPUT_CONTROL;
			else Deassert_STATUS_OUTPUT_CONTROL;
		}
		else Deassert_STATUS_OUTPUT_CONTROL;

		return;
	}
	
	switch (State)
	{
	case STATE_AUTO_RETRACT:
		if (++timer < SECONDS(0.250))
			return;
		break;
	
	case STATE_EMERGENCY_RETRACT:
		if (++timer < SECONDS(0.100))
			return;
		break;

	case STATE_OFF:
	case STATE_IDLE:
	case STATE_FAULT:
	case STATE_SETUP:
	case STATE_MENU:
	case STATE_GROUND_JACKS_1:
	case STATE_AUTO_LEVEL_FIRST_AXIS:
	case STATE_GROUND_JACKS_2:
	case STATE_AUTO_LEVEL:
	case STATE_MANUAL_LEVEL:
	case STATE_ZERO_MODE:
	case STATE_AIRBAG_TIME_CONFIG:
	case STATE_GROUND_AFTER_LEVEL:
		Deassert_STATUS_OUTPUT_CONTROL;
		return;
	}

	timer = 0;
	STATUS_OUTPUT_CONTROL ^= 1;
}

//////////
//////////
//////////
//////////

void Logic_Init(void)
{
	PSI_Switch_Debounce = PSI_SWTICH_DEBOUNCE_LONG;

	// clear command
	CheckLevelerType3Switches();
	CheckTouchPadSwitches();

	// halt all jacks
	Jack_SetPumpCmd(DIRECTION_HALT);
	Jack_DeactivateJacks(JACKS_ALL);

	// turn off panel LEDs
	LevelerType3_SetAllStatusOutputs(LEVELER_TYPE_3_OUTPUT_OFF);

	// init console driver period
	ConsoleDriver_SetTxTime(30); // 300ms

	// reset to off state
	SetState(STATE_OFF, EVENT_RESET);
}

// runs every 10 milliseconds
void Logic_Task10ms(void)
{
	if (Manufacturing_IsActive())
		return;

	// maintain timers
	if (StateTimer != 0xFFFF)
		StateTimer++;

	// maintian leaky hose timer, reset counter if timer reaches zero
	if (LeakyHoseTimer) LeakyHoseTimer--;
	else LeakyHoseCounter = 0;
	
	// safety - make sure debounce is long, unless emergency retract
	if (State != STATE_EMERGENCY_RETRACT)   PSI_Switch_Debounce = PSI_SWTICH_DEBOUNCE_LONG;

	// return all buttons to default state, we'll selectively disable them below
	// we reset the state of the buttons every loop, to make sure they stay corrupted if another module craps on our RAM
	// the philosophy here is that it's better to have an unused button accidentally "enabled" than to have in important button accidentally "disabled"
	// therefore: we enable all "state normal" buttons
	SetLevelerScreen(State);

	CheckLowVoltage();          // check for low battery condition

	// read/decode switches, check switch input states
	CheckLevelerType3Switches();
	CheckTouchPadSwitches();

	// module management
	CheckLossOfUI();                // handle loss of virtual UI
	CheckLossOfTouchpad();          // handle loss of touchpad
	HandleSleepTimeout();           // system deactivation logic (jacks haven't been moved for a while, loss of UI sources)
	CheckErrors();                  // determine if common errors are present
	UpdateLevelState();             // determine status of level
	StrokeLimitDetection();         // check for jack stroke limit
	CheckForEmergencyRetract(); // check for ER conditions
	DriveAirBags();                 // air bag control logic
	DriveStatusOutput();                // slide output enable logic

	// before executing the state, request all outputs off
	// the state will turn outputs back on as necessary
	Jack_SetPumpCmd(DIRECTION_HALT);
	Jack_DeactivateJacks(JACKS_ALL);

	// drive UI outputs
	ConsoleDriver_Task10ms();       // update panel text
	UpdateLevelerType3Status(); // update panel status leds
	UpdateTouchPadStatus();       // update touchpad leds 

	// execute state specific algorithms
	switch (State)
	{
	default: NewState(STATE_OFF, EVENT_STATE_RECOVERY); break;

	#undef DEFINE_STATE
	#define DEFINE_STATE(state, screen, buttons) case STATE_##state: state(); break;
	DEFINE_STATES   
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
