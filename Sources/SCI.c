// SCI.c
// SCI driver for Freescale MC9S12 microcontrollers
// Version 1.7
// Copyright (c) 2010, 2011, 2012, 2013 Innovative Design Solutions, Inc.
// All rights reserved

// history
// 1.0  First revision - based on HCS08 SCI driver version 1.7
// 1.1  SCI_TxBuffer now accepts void*
// 1.2  Added SCI_GetBaudRate() and SCI_SetBaudRate()
// 1.3  Added support for MC9S12G family
//      Added SCI_TxBreakCharacter()
//      Added SCI_TX_BUFFER_SIZE_BYTES
//      Added SCI_RX_BUFFER_SIZE_BYTES
// 1.4  Added DEBUG_SCI_INTERRUPT_TASK_PORT and DEBUG_SCI_BACKGROUND_TASK_PORT
// 1.5  Bug fixes
// 1.6  More Bug Fixes (TIE/TCIE)
// 1.7  More bug fixes (MaxTxQueueCount / MaxRxQueueCount)

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// USAGE:
//
// - Automatically generates support for standard IDS serial communication protocol
//
// - message packets are transmitted using a technique similar to SLIP/PPP
//   - 7E is start of frame / end of frame
//   - 7D is control character
//   - packet is formed as : 7E data checksum 7E
//   - 7E character transmitted as 7D 5E
//   - 7D character	transmitted as 7D 5D
//
// - The following functions are supported by default:
//   -- void SCI_Init(void);                              // initialize module
//   -- const uint8 * SCI_Rx(void);                       // return pointer to RX message (if any)
//   -- void SCI_RxFlushMessage(void);                    // remove oldest message from RX buffer
//   -- uint8 SCI_TxIsBufferFull(void);                   // determine if TX buffer has room for message
//   -- uint16 SCI_TxBufferBytesFree(void);               // returns number of free bytes in the buffer
//   -- uint8 SCI_Tx(uint8 len, ...);                     // queue message into TX buffer
//   -- uint8 SCI_TxBuffer(uint8 len, const uint8 * buf); // queue message into TX buffer
//   -- uint8 SCI_TxBreakCharacter(void);                 // transmits a break character
//
// - Customize this module by adding #define statements in "global.h"
//
// - The following values MUST be declared:
//   -- #define SCI_PORT(reg)              module##reg /* where module is the name of the desired */
//                                                     /* hardware peripheral -- identifies which */
//                                                     /* one to use when more than one exists    */
//                                                     /* example: SCI, SCI0, SCI1, etc...        */
//   -- #define SCI_DEFAULT_BAUD_RATE      val  /* desired baud rate at boot */
//   -- #define SCI_TX_BUFFER_SIZE_BYTES   val  /* size of byte buffer to store outgoing bytes */
//   -- #define SCI_RX_BUFFER_SIZE_BYTES   val  /* size of byte buffer to store incoming bytes (only if interrupt operation defined) */
//   -- #define SCI_RX_MAX_MESSAGE_LENGTH  val  /* maximum message length that can be received */
//
// - exactly ONE of the following must be defined
//   -- #define SCI_DRIVER_POLLED     /* driver operates in polled mode */
//   -- #define SCI_DRIVER_INTERRUPT  /* driver operates in interrupt mode */
//
// - the following are optional
//   -- #define SCI_MAINTAIN_STATISTICS  /* if defined, then module will maintain usage statistics */
//
// - POLLED operation defines the following functions
//   -- void SCI_Task(void);   // polling routine (call from fast background loop)
//
// - INTERRUPT operation defines the following functions
//   -- __interrupt void SCI_OnReceiveInterrupt(void);    // RX ISR
//   -- __interrupt void SCI_OnTransmitInterrupt(void);   // TX ISR
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// verify compiler and environment
#if (!defined __HIWARE__) || (__MWERKS__ != 1)
  #error This module designed for Metrowerks CodeWarrior for HC12 compiler
#endif
#if !defined __HC12__
  #error This module designed for HC12 processors
#endif

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

#define SCI_TX_MAX_MESSAGE_LENGTH   (SCI_TX_BUFFER_SIZE_BYTES - 1)

void SCI_Init(void);
void SCI_Task(void); // background processing of received bytes

#ifdef SCI_DRIVER_INTERRUPT
#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void SCI_OnInterrupt(void);
#pragma CODE_SEG DEFAULT
#endif

// receive functions
const uint8 * SCI_Rx(void);
void SCI_RxFlushMessage(void);

// transmit functions
uint16 SCI_TxBufferBytesInUse(void);
uint16 SCI_TxBufferBytesFree(void);
uint8 SCI_TxIsBufferFull(void);
uint8 SCI_Tx(uint8 len, ...);
uint8 SCI_TxBuffer(uint8 len, const void * buf);
uint8 SCI_TxBreakCharacter(void);

// baud rate select functions
uint32 SCI_GetBaudRate(void);
uint32 SCI_SetBaudRate(uint32 baud_rate);

#ifdef SCI_MAINTAIN_STATISTICS
uint32 SCI_GetNumBytesSent(void);
uint32 SCI_GetNumBytesReceived(void);
uint32 SCI_GetNumMessagesSent(void);
uint32 SCI_GetNumMessagesReceived(void);
uint16 SCI_GetTxBufferOverflowCount(void);
uint16 SCI_GetRxBufferOverflowCount(void);
uint16 SCI_GetMaxTxQueueCount(void);
uint16 SCI_GetMaxRxQueueCount(void);
#endif // SCI_MAINTAIN_STATISTICS

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// processor check
#if   defined MC9S12G48 || defined MC9S12G64 || defined MC9S12G96 || defined MC9S12G128 || defined MC9S12G192 || defined MC9S12G240
  #define S12SCIV5
#elif defined MC9S12GA192 || defined MC9S12GA240
  #define S12SCIV5
#elif defined MC9S12GN16 || defined MC9S12GN32 || defined MC9S12GN48
  #define S12SCIV5
#elif defined MC9S12HY32 || defined MC9S12HY48 || defined MC9S12HY64
  #define S12SCIV5
#elif defined MC9S12P32 || defined MC9S12P64 || defined MC9S12P96 || defined MC9S12P128
	#define S12SCIV5
#else
	#error SCI driver not designed for this processor
#endif

#if !defined S12SCIV5
  #error SCI module not defined
#endif

#if defined SCI_DRIVER_INTERRUPT && defined SCI_DRIVER_POLLED
	#error SCI driver: cannot define both SCI_DRIVER_POLLED and SCI_DRIVER_INTERRUPT
#elif !defined SCI_DRIVER_INTERRUPT && !defined SCI_DRIVER_POLLED
	#error SCI driver: must define SCI_DRIVER_POLLED or SCI_DRIVER_INTERRUPT
#endif

#ifndef SCI_DEFAULT_BAUD_RATE
	#error SCI_DEFAULT_BAUD_RATE must be defined
#endif

// determine size of index into TX buffer
#ifndef SCI_TX_BUFFER_SIZE_BYTES
  #error must define SCI_TX_BUFFER_SIZE_BYTES
#elif SCI_TX_BUFFER_SIZE_BYTES <= 256
  typedef uint8 TXINDEX;
#else
  typedef uint16 TXINDEX;
#endif

// determine size of index into RX buffer
#ifdef SCI_DRIVER_INTERRUPT
  #ifndef SCI_RX_BUFFER_SIZE_BYTES
    #error must define SCI_RX_BUFFER_SIZE_BYTES when SCI_DRIVER_INTERRUPT is enabled
  #elif SCI_RX_BUFFER_SIZE_BYTES <= 256
    typedef uint8 RXINDEX;
  #else
    typedef uint16 RXINDEX;
  #endif // SCI_RX_BUFFER_SIZE_BYTES
#endif // SCI_DRIVER_INTERRUPT

#ifdef SCI_TRANSMIT_LOOPBACK
static uint8 IgnoreLoopback = FALSE;
#endif

#ifdef SCI_MAINTAIN_STATISTICS
static uint32 BytesSent = 0;
static uint32 BytesReceived = 0;
static uint32 MessagesSent = 0;
static uint32 MessagesReceived = 0;
static uint16 TxOverflowCount = 0;
static uint16 RxOverflowCount = 0;
static uint16 MaxTxQueueCount = 0;
static uint16 MaxRxQueueCount = 0;
#define STATISTICS(exp) (exp)
uint32 SCI_GetNumBytesSent(void) { return AtomicRead32(&BytesSent); }
uint32 SCI_GetNumBytesReceived(void) { return AtomicRead32(&BytesReceived); }
uint32 SCI_GetNumMessagesSent(void) { return AtomicRead32(&MessagesReceived); }
uint32 SCI_GetNumMessagesReceived(void) { return AtomicRead32(&BytesSent); }
uint16 SCI_GetTxBufferOverflowCount(void) { return AtomicRead16(&TxOverflowCount); }
uint16 SCI_GetRxBufferOverflowCount(void) { return AtomicRead16(&RxOverflowCount); }
uint16 SCI_GetMaxTxQueueCount(void) { return AtomicRead16(&MaxTxQueueCount); }
uint16 SCI_GetMaxRxQueueCount(void) { return AtomicRead16(&MaxRxQueueCount); }
#else
#define STATISTICS(exp)
#endif // SCI_MAINTAIN_STATISTICS

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

uint32 SCI_GetBaudRate(void)
{
  uint32 denominator = SCI_PORT(BD);
  uint32 numerator;

  denominator <<= 3;
  numerator = (uint32)(FBUS) + denominator; // rounding
  denominator <<= 1;
  return numerator / denominator;  
}

uint32 SCI_SetBaudRate(uint32 baudrate)
{
  uint32 numerator;
  uint16 scibd;
  
  //              FBUS
  //  SCIBD = -------------
  //          16 * baudrate
  baudrate <<= 3;
  numerator = (uint32)(FBUS) + baudrate; // rounding
  baudrate <<= 1;
  scibd = ui32_to_ui16(numerator / baudrate);
  
  EnterCriticalSection();
  SCI_PORT(SR2_AMAP) = 0;
  SCI_PORT(BD) = scibd;
  LeaveCriticalSection();
  
  return SCI_GetBaudRate();
}

void SCI_Init(void)
{
	// NOTE: some regsiters are only accessible according to the state of the AMAP bit in SCISR2
  // SCICR2  - always accessible
  // SCISR1  - always accessible
  // SCISR2  - always accessible
  // SCIASR1 - only accessible if AMAP = 1
  // SCIACR1 - only accessible if AMAP = 1
  // SCIACR2 - only accessible if AMAP = 1
  // SCICR1  - only accessible if AMAP = 0
  // SCIBD   - only accessible if AMAP = 0

 	// SCICR2
	// 00?00000
	// ||||||||
	// ||||||| \_ SBK   0 = don't send break character
	// |||||| \__ RWU   0 = standby state off
	// ||||| \___ RE    0 = disable receiver (for now)
	// |||| \____ TE    0 = disable transmitter (for now)
	// ||| \_____ ILIE  0 = idle line interrupt disabled
	// || \______ RIE   ? = receive register full interrupt enabled/disabled
	// | \_______ TCIE  0 = transmission complete interrupt disabled
	//  \________ TIE   0 = transmit register empty interrupt disabled
	#ifdef SCI_DRIVER_INTERRUPT
	SCI_PORT(CR2) = 0x20;
	#else
	SCI_PORT(CR2) = 0x00;
	#endif

  // SCISR1
  // no initialization necessary

	// SCISR2
	// 1xx001xx
	// |  |||||
	// |  |||| \_ RAF    read only
	// |  ||| \__ TXDIR  (only used in single-wire mode)
	// |  || \___ BRK13  1 = break character is 13 or 14 bits long
	// |  | \____ RXPOL  0 = normal recieve polarity
	// |   \_____ TXPOL  0 = normal transmit polarity
	//  \________ AMAP   1 = view alternate registers
	SCI_PORT(SR2) = 0x84;

  // AMAP = 1
  // can read/write alternate registers

	SCI_PORT(ASR1) = 0xFF; // clear status bits

	// SCIACR1
	// 0xxxxx00
	// |     ||
	// |     | \_ BKDIE    0 = break detect interrupt enable
	// |      \__ BERRIE   0 = bit error interrupt disabled
	//  \________ RXEDGIE  0 = receive active edge interrupt disabled
	SCI_PORT(ACR1) = 0x00;
  
	// SCIACR2
	// xxxxx000
	//      |||
	//      || \_ BKDFE     0 = break detect circuit disabled
	//       \\__ BERRM0:1  00 = bit error detect circuit is disabled
  SCI_PORT(ACR2) = 0x00;

  // clear AMAP so we can read/write normal registers
  SCI_PORT(SR2_AMAP) = 0;

	// SCICR1
	// 0100110x
	// ||||||||
	// ||||||| \_ PT      Parity bit - not used
	// |||||| \__ PE      0 = parity disabled
	// ||||| \___ ILT     1 = Idle Line Type Bit - 1 start after stop bit
	// |||| \____ WAKE    1 = SCI Wake up SCI - 1 = Address mark wakeup
	// ||| \_____ M       0 = 8 bit characters
	// || \______ RSRC    0 = duplex mode
	// | \_______ SCISWAI 1 = SCI runs in WAIT mode
	//  \________ LOOPS   0 = disable loopback mode
	SCI_PORT(CR1) = 0x4C;

 	// program initial baud rate
	//  baud rate = FBUS / (16 x BD)
	SCI_PORT(BD) = (uint16) (((FBUS / 16.0) / SCI_DEFAULT_BAUD_RATE) + 0.5);

  // enable SCI receiver and transmitter
	SCI_PORT(CR2_RE) = 1;
  SCI_PORT(CR2_TE) = 1;
 
  (void)SCI_TxBreakCharacter();
 }

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// receiver

// function type

// reconstructed RX message buffer
typedef struct {
	uint8 Length;
	uint8 Data[SCI_RX_MAX_MESSAGE_LENGTH];
} RX_MESSAGE;

// buffer that holds the latest received message, for background
// processing
static RX_MESSAGE RxMessageBuf = { 0 };

// determine location of messages that are being reconstructed
#ifdef SCI_DRIVER_POLLED
  // in polled mode, since we have no raw byte buffer, we need some
  // sort of buffer to construct messages while RxMessageBuf is in use
  static RX_MESSAGE RxWorkBuf = { 0 };
#else
  // in interrupt mode, we don't need a dedicated work buffer
  // as we already have a large buffer to hold incoming bytes
  // we will write to the RxMessageBuf directly (when available)
  // otherwise they will just remain in the buffer
  #define RxWorkBuf RxMessageBuf
#endif // SCI_DRIVER_POLLED

// in interrupt operation, we need a buffer to store real time data streamed in from the UART
#ifdef SCI_DRIVER_INTERRUPT
static RXINDEX RxHead = 0;
static RXINDEX RxTail = 0;
static uint8 RxBuffer[SCI_RX_BUFFER_SIZE_BYTES];
#endif // SCI_DRIVER_INTERRUPT

const uint8 * SCI_Rx(void)
{
	if (!RxMessageBuf.Length)
		return NULL;
	return (const uint8 *) &RxMessageBuf;
}

// create the function to latch the work buffer
#ifdef SCI_DRIVER_POLLED
static void LatchRxWorkBuf(void)
{
  if (RxWorkBuf.Length)
  {
    RxMessageBuf = RxWorkBuf;
    RxWorkBuf.Length = 0;
  }  
}
#else
  // in interrupt mode we dont have a dedicated work buffer
  // so there is nothing to do
  #define LatchRxWorkBuf()
#endif

void SCI_RxFlushMessage(void)
{
  RxMessageBuf.Length = 0;
  LatchRxWorkBuf();
}

// NOTE: this function should only be called while the work buffer is empty
#pragma INLINE
static void RxDecodeByte(uint8 byte)
{
	static uint8 Length = 0;
	static uint8 EscapeCharacter = 0;
	static uint8 Checksum;
	static uint8 ChecksumValid = FALSE;

	// handle byte
	switch(byte)
	{
	case 0x7E: // start/end character
		// if
		// - at least 1 byte (+ checksum) was received
		// - AND message (without checksum) fits within buffer
		// - AND message checksum is valid
		// - AND RX buffer is empty
		if (Length > 1 && Length <= (elementsof(RxWorkBuf.Data)+1) && ChecksumValid)
		{
      RxWorkBuf.Length = Length - 1; // save length (without checksum);
      LatchRxWorkBuf(); // latch the buffer if we can
		  STATISTICS(MessagesReceived++);
		}

		// reset buffer
		Length = 0;
		Checksum = 0;
		EscapeCharacter = 0;
		ChecksumValid = FALSE;
		return;

	case 0x7D: // control character
		EscapeCharacter = 0x20;
		return;

	default: // regular character
		// handle previous control character
		byte ^= EscapeCharacter;
		EscapeCharacter = 0;

		// save byte if there is room in the buffer
		if (Length < elementsof(RxWorkBuf.Data))
			RxWorkBuf.Data[Length] = byte;

		// increase message length
		if (Length < 0xFF)
			Length++;

		// check if this byte fits the checksum criteria
		ChecksumValid = (byte == Checksum);

		// update checksum
		Checksum += byte;

		return;
	}
}

#pragma INLINE
static void SCI_RxTask(void)
{
  RXINDEX tail;

  // has a byte been received?
	if (SCI_PORT(SR1_RDRF))
  {
    // get the byte
    uint8 byte = SCI_PORT(DRL);

    STATISTICS(BytesReceived++);

#ifdef SCI_TRANSMIT_LOOPBACK
  	if (IgnoreLoopback)
  	{
  		IgnoreLoopback = FALSE;
  		return;
  	}
#endif

#ifdef SCI_DRIVER_INTERRUPT
    // interrupt operation
    // stuff byte into buffer for later processing
    tail = RxTail + 1;
    if (tail >= elementsof(RxBuffer)) tail = 0;
    if (tail != RxHead)
    {
      RxBuffer[RxTail] = byte;
      RxTail = tail;
#ifdef SCI_MAINTAIN_STATISTICS
      {
        int16 inuse = RxTail - RxHead;
        if (inuse < 0)
          inuse += elementsof(RxBuffer);
        if (MaxRxQueueCount < inuse) MaxRxQueueCount = inuse;
      }
#endif
      return;
    }
#else
    // polled operation
    // perform immediate decoding
    if (!RxWorkBuf.Length)
    {
      RxDecodeByte(byte);
      return;
    }
#endif

    // if we get here, then there was no space to put the byte
    STATISTICS(RxOverflowCount++);
  }
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

// transmitter

// TX buffer
// real time data streamed out of the UART
static TXINDEX TxHead = 0;
static TXINDEX TxTail = 0;
static uint8 TxBuffer[SCI_TX_BUFFER_SIZE_BYTES];

#pragma INLINE
uint16 SCI_TxBufferBytesInUse(void)
{
  int16 inuse = TxTail - TxHead;
  if (inuse < 0)
    inuse += elementsof(TxBuffer);
  return inuse;
}

// this function returns the "effective" number of free bytes in the TX buffer
// by effective I mean the number of bytes that a user can stuff via a call to SCI_Tx()
#pragma INLINE
uint16 SCI_TxBufferBytesFree(void)
{
  return elementsof(TxBuffer) - 1 - SCI_TxBufferBytesInUse(); // remove 1 byte for LEN character added to buffer
}

#pragma INLINE
uint8 SCI_TxIsBufferFull(void)
{
	return SCI_TxBufferBytesFree() == 0;
}

static uint8 QueueForTransmit(uint8 len, const uint8 * msg)
{
  uint8 result = FALSE;
  uint16 inuse;
  TXINDEX tail;

	EnterCriticalSection();

  // is there room for this message?
  inuse = SCI_TxBufferBytesInUse() + len + 1;
  if (inuse >= elementsof(TxBuffer))
  {
  	STATISTICS(TxOverflowCount++);
  	LeaveCriticalSection();
    return FALSE;
  }
#ifdef SCI_MAINTAIN_STATISTICS
  if (MaxTxQueueCount < inuse) MaxTxQueueCount = inuse;
#endif
  
  // write length
  tail = TxTail;
  TxBuffer[tail++] = len;
  if (tail >= elementsof(TxBuffer)) tail = 0;

  // copy bytes (if any)
  while (len--)
  {
    TxBuffer[tail++] = *msg++;
    if (tail >= elementsof(TxBuffer)) tail = 0;
  }
  
  TxTail = tail;
  
  STATISTICS(MessagesSent++);

	LeaveCriticalSection();

 // enable transmit buffer empty interrupts
#ifdef SCI_DRIVER_INTERRUPT
	SCI_PORT(CR2_TIE) = 1;
#endif

  return TRUE;
}

uint8 SCI_TxBreakCharacter(void)
{
  // queue a break character
  return QueueForTransmit(0, NULL);
}

// function commits the temporary message in the buffer for transmit
// returns non-zero on success, zero on failure
uint8 SCI_TxBuffer(uint8 len, const void *buf)
{
	if (len)
  	return QueueForTransmit(len, (const uint8 *) buf); 
  return FALSE;
}

uint8 SCI_Tx(uint8 len, ...)
{
  // this function uses an open parameter list (...)
  // as such, it expects the bytes to be transmitted to be tightly packed and on the stack
  // compiler option -Cni must be enabled, and specifies no integral promotion on chars  
  // without this option chars are promoted to ints before being stuffed on the stack
  // effectively corrupting the message
  // 
  // example:
  //     SCI_Tx(7, (uint8)x11, (uint16)0x2233, (uint8)0x44, (uint16)0x5566, (uint8)0x77);
  // without -Cni (FAILS)
  //     stack ->  00 11 22 33 00 44 55 66 00 77
  //                \\__________\\__________\\______ padding to promote char to int!
  // with -Cni (WORKS)
  //     stack ->  11 22 33 44 55 66 77
  #ifndef __CNI__
    #error Compiler option -Cni (no integral promotion on characters) must be enabled when using SCI_Tx()
  #endif

	if (len)
  	return QueueForTransmit(len, &len + sizeof(char)); 
  return FALSE;
}

#pragma INLINE
static void SCI_TxTask(void)
{
  static uint8 BytesLeft = 0;
  static uint8 Checksum;
  TXINDEX head;

  if (!SCI_PORT(SR1_TDRE))
    return;
  
  // what we do depends on how many bytes remain
  switch (BytesLeft)  
  {
  case 0: // inactive
		// find next message to send
		if (TxHead == TxTail)
 		{
#ifdef SCI_DRIVER_INTERRUPT
 		  // nothing to do, disable transmitter empty interrupt
 	    SCI_PORT(CR2_TIE) = 0;
#endif
 	    return;
 		}
		
 		// get new length
 		BytesLeft = TxBuffer[TxHead];
 		head = TxHead + 1;
 		if (head >= elementsof(TxBuffer)) head = 0;
 		TxHead = head;

     // is there a message?
 		if (!BytesLeft)
 		{
       // queue a break character
		  SCI_PORT(CR2_SBK) = 1;
		  SCI_PORT(CR2_SBK) = 0;
		  break;
    }
    	
    // found a message
    SCI_PORT(DRL) = 0x7E; // transmit start of frame
   	BytesLeft += 2; // 2 extra bytes, checksum and EOF
    Checksum = 0; // initalize checksum
    break;

  case 1: // all data bytes sent -- only need to transmit end of frame
    SCI_PORT(DRL) = 0x7E; // transmit end of frame
		BytesLeft = 0;
		break;

  default: // we must transmit next active data byte
    // we have bytes, right?
 		if (TxHead == TxTail)
 		{
       // buffer is empty, something got fucked up
 		  BytesLeft = 0;
#ifdef SCI_DRIVER_INTERRUPT
 		  // nothing to do, disable transmitter empty interrupt
 	    SCI_PORT(CR2_TIE) = 0;
#endif
      return;
    }
      
    // transmit the next regular data byte
  	switch(TxBuffer[TxHead])
  	{
  	case 0x7D:
  	case 0x7E:
  		SCI_PORT(DRL) = 0x7D;
  		TxBuffer[TxHead] ^= 0x20;
  		Checksum += 0x20;
  		break;
  	default:
  		SCI_PORT(DRL) = TxBuffer[TxHead];
  		Checksum += TxBuffer[TxHead];
  		if (--BytesLeft == 2)
  			TxBuffer[TxHead] = Checksum; // transmit checksum next pass
  		else
  		{
  		  head = TxHead + 1;
  		  if (head >= elementsof(TxBuffer)) head = 0;
     		TxHead = head;
  		}
  		break;
  	}
  	break;
	}

  // we get here if we transmitted something
  STATISTICS(BytesSent++);

	#ifdef SCI_TRANSMIT_LOOPBACK
	// ignore next byte received
	IgnoreLoopback = TRUE;
	#endif
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef SCI_DRIVER_INTERRUPT

// interrupt service routine
#pragma CODE_SEG __NEAR_SEG NON_BANKED
__interrupt void SCI_OnInterrupt(void)
{
#ifdef DEBUG_SCI_INTERRUPT_TASK_PORT
  DEBUG_SCI_INTERRUPT_TASK_PORT = 1;
#endif  
	SCI_RxTask(); // get next byte and stuff in queue
	SCI_TxTask(); // transmit next byte from queue
#ifdef DEBUG_SCI_INTERRUPT_TASK_PORT
  DEBUG_SCI_INTERRUPT_TASK_PORT = 0;
#endif  
}
#pragma CODE_SEG DEFAULT

// background task
void SCI_Task(void)
{
#ifdef DEBUG_SCI_BACKGROUND_TASK_PORT
  DEBUG_SCI_BACKGROUND_TASK_PORT = 1;
#endif

  LatchRxWorkBuf();

  // get buffered bytes from real time program and process
  while (RxHead != RxTail && !RxWorkBuf.Length)
  {
    uint8 byte = RxBuffer[RxHead];
    RXINDEX h = RxHead + 1;
    if (h >= elementsof(RxBuffer)) h = 0;
    RxHead = h;
    RxDecodeByte(byte);
  }

#ifdef DEBUG_SCI_BACKGROUND_TASK_PORT
  DEBUG_SCI_BACKGROUND_TASK_PORT = 0;
#endif
}

#else // SCI_DRIVER_INTERRUPT

// background task
void SCI_Task(void)
{
#ifdef DEBUG_SCI_BACKGROUND_TASK_PORT
  DEBUG_SCI_BACKGROUND_TASK_PORT = 1;
#endif
  LatchRxWorkBuf();
	SCI_RxTask(); // get next byte and and process
	SCI_TxTask(); // transmit next byte from queue
#ifdef DEBUG_SCI_BACKGROUND_TASK_PORT
  DEBUG_SCI_BACKGROUND_TASK_PORT = 0;
#endif
}

#endif // SCI_DRIVER_INTERRUPT


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
