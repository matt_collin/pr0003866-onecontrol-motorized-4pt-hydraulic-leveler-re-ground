// IDS-CAN Integration.c
// Product specific IDS-CAN integration
// (c) 2012, 2013, 2015 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// IDS-CAN message filters
// messages that match these identifiers will trigger the appropriate callback
// only MASK bits set to 1 are matched -- other bits are assumed to match
// filters are scanned in the order listed here
//
// there are two types of filters, TX and RX
// IDS_CAN_TX_MESSAGE(message_type, callback)
// IDS_CAN_RX_MESSAGE(message_type, callback)
//    filter match occurs when message_type matches the message
//    message_type = 0xFF implies that all messages match
//    callback is called if tx/rx match is made
#define IDS_CAN_MESSAGE_FILTERS \
	IDS_CAN_RX_MESSAGE_FILTER(IDS_CAN_MESSAGE_TYPE_COMMAND, LevelerType3_OnCommandMessageRx) \

// ** OPTIONAL **   comment out this block if PIDs support is not needed
//
// declare all PIDs supported, format is any combination of
// read only:
//     PID_READ_ONLY(id, read_expression)
// read/write (RAM):
//     PID_READ_WRITE(id, read_expression, session_id, write_expression)
// read/write (non-volatile)
//     PID_READ_WRITE_NVR(id, read_expression, session_id, write_expression)
// where:
//                 id : the 16-bit PID defined in the IDS-CAN specification
//         session_id : a session ID that must be unlocked/open to allow writing
//                      set to zero to allow writing at any time
//    read_expression : an expression which returns the value of the PID
//   write_expression : an expression which sets the value of the PID
#define IDS_CAN_PID_SUPPORT \
PID_READ_WRITE_NVR(IDS_CAN_PID_PRODUCTION_BYTES,       VAL_U8 = Config.ProductionBlock.ProductionByte, IDS_CAN_SESSION_ID_MANUFACTURING, Config.ProductionBlock.ProductionByte = VAL_U8; Config_ProductionBlock_Flush()) \
PID_READ_ONLY(IDS_CAN_PID_CAN_ADAPTER_MAC,             VAL_U48 = *(U48*) IDS_CAN_GetAdapterMAC()) \
PID_READ_ONLY(IDS_CAN_PID_IDS_CAN_CIRCUIT_ID,          VAL_U32 = IDS_CAN_GetCircuitID(device)) \
PID_READ_ONLY(IDS_CAN_PID_IDS_CAN_FUNCTION_NAME,       VAL_U16 = IDS_CAN_GetFunctionName(device)) \
PID_READ_ONLY(IDS_CAN_PID_IDS_CAN_FUNCTION_INSTANCE,   VAL_U8 = IDS_CAN_GetFunctionInstance(device)) \
PID_READ_ONLY(IDS_CAN_PID_BATTERY_VOLTAGE,             VAL_U32 = (uint32)Logic_GetVBatt() << 8) \
PID_READ_ONLY(IDS_CAN_PID_REGULATOR_VOLTAGE,           VAL_U32 = (uint32)(VDD * 65536.0 + 0.5)) \
PID_READ_ONLY(IDS_CAN_PID_NUM_TILT_SENSOR_AXES,        VAL_U8 = 2) \
PID_READ_ONLY(IDS_CAN_PID_TILT_AXIS_1_ANGLE,           VAL_I32 = (int32)Sensor.X.Angle << 8) \
PID_READ_ONLY(IDS_CAN_PID_TILT_AXIS_2_ANGLE,           VAL_I32 = (int32)Sensor.Y.Angle << 8) \
PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_1, 			 VAL_U8 = (uint8) Input.Digital.JacksAllUp) \
PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_2, 			 VAL_U8 = (uint8) Input.Digital.ParkBrakeIsOn) \
PID_READ_ONLY(IDS_CAN_PID_MANUFACTURING_PID_3, 			 VAL_U8 = (uint8) (STAT1 | (STAT2 << 1) | (STAT3 << 2) | (STAT4 << 3))) \

// ** OPTIONAL **   comment out this block if session support is not needed
//
// declare all SESSION_IDs supported, format is
//     SESSION(id, cypher, session_open_callback, session_close_callback)
// where:
//                        id :  the 16-bit SESSION_ID defined in the IDS-CAN specification
//                    cypher :  a unique 32-bit value that is used when generating the security key
//                              this value should match the value in the IDS-CAN specification
//  session_allowed_callback :  function that is called to determine if a session is allowed, can be NULL
//                              function prototype is CALLBACK(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
//     session_open_callback :  function that is called when the session is opened, can be NULL
//                              function prototype is CALLBACK(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
//     session_close_callback : function that is called when the session is opened, can be NULL
//                              function prototype is CALLBACK(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
#define IDS_CAN_SESSION_SUPPORT \
SESSION(IDS_CAN_SESSION_ID_MANUFACTURING,  0xB16BA115, NULL, Manufacturing_OnIDSCANSessionOpened, Manufacturing_OnIDSCANSessionClosed) \
SESSION(IDS_CAN_SESSION_ID_DIAGNOSTIC,     0xBABECAFE, NULL, Manufacturing_OnIDSCANSessionOpened, Manufacturing_OnIDSCANSessionClosed) \
SESSION(IDS_CAN_SESSION_ID_REMOTE_CONTROL, 0xB16B00B5, NULL, LevelerType3_OnIDSCANSessionOpened,  LevelerType3_OnIDSCANSessionClosed)

const uint8* IDS_CAN_GetAdapterMAC(void);

uint32 IDS_CAN_GetCircuitID(IDS_CAN_DEVICE device);

IDS_CAN_PRODUCT_ID IDS_CAN_GetProductID(void);

IDS_CAN_FUNCTION_NAME IDS_CAN_GetFunctionName(IDS_CAN_DEVICE device);
uint8 IDS_CAN_GetFunctionInstance(IDS_CAN_DEVICE device);

uint8 AppCallback_IDS_CAN_GetProductionByte(void);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

IDS_CAN_PRODUCT_ID IDS_CAN_GetProductID(void)
{
	return IDS_CAN_PRODUCT_ID_25776_ONECONTROL_MOTORIZED_4PT_HYDRAULIC_LEVELER;
}

const uint8* IDS_CAN_GetAdapterMAC(void)
{
//	static const uint8 ff[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
//	return ff;
	return Config.ProductionBlock.MAC;
}

#pragma MESSAGE DISABLE C5703 /* device not used */
uint32 IDS_CAN_GetCircuitID(IDS_CAN_DEVICE device)
{
	return 0; // cannot be assigned
}
#pragma MESSAGE DEFAULT C5703

#pragma MESSAGE DISABLE C5703 /* device not used */
IDS_CAN_FUNCTION_NAME IDS_CAN_GetFunctionName(IDS_CAN_DEVICE device)
{
	return IDS_CAN_FUNCTION_NAME_LEVELER;
}
#pragma MESSAGE DEFAULT C5703

#pragma MESSAGE DISABLE C5703 /* device not used */
uint8 IDS_CAN_GetFunctionInstance(IDS_CAN_DEVICE device)
{
	return 0;
}
#pragma MESSAGE DEFAULT C5703

uint8 AppCallback_IDS_CAN_GetProductionByte(void)
{
    return Config.ProductionBlock.ProductionByte;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
