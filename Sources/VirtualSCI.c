// VirtualSCI.c
// Virtual SCI driver
// (c) 1998, 2001, 2009 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void VirtualSCI_Init(uint16 init);
void VirtualSCI_Task500us(void);

uint8 VirtualSCI_RXIsBufferFull(void);
uint8 VirtualSCI_RXByte(uint8 pos);
void VirtualSCI_RXFlushMessage(void);

uint8 VirtualSCI_TXIsBufferFull(void);
uint8 VirtualSCI_TX(uint8 len, ...);
uint8 VirtualSCI_TXBuffer(uint8 len, const uint8 * buf);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define TX_BIT_TIME(p)			((uint16)((p * (TIMER_INTERRUPT_COUNTS * 3)) + 0.5))

#define TX_OC_VALUE_REG			TC4
#define TX_TIOS_BIT				TIOS_IOS4
#define TX_OC_FLAG				TFLG1_C4F
#define TX_OC_LEVEL_BIT			TCTL1_OL4
#define TX_OUTPUT_MODE_BIT		TCTL1_OM4

// byte level interface
#define RX_COM_PORT	PTT_PTT5

enum {
	TX_START_BIT = 0,
	TX_DATA_BIT0,
	TX_DATA_BIT1,
	TX_DATA_BIT2,
	TX_DATA_BIT3,
	TX_DATA_BIT4,
	TX_DATA_BIT5,
	TX_DATA_BIT6,
	TX_DATA_BIT7,
	TX_STOP_BIT1,
	TX_STOP_BIT2,
	TX_IDLE
};

static uint8 TXDataRegister;
static uint8 TXState = TX_IDLE;

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

// message level interface

// receive buffer
static struct {
	uint8 full;
	uint8 buffer[VIRTUAL_SCI_MAX_RX_MESSAGE_LENGTH];
} RX = { FALSE };

// transmit buffer
static struct {
	uint8 len;
	uint8 buffer[VIRTUAL_SCI_MAX_TX_MESSAGE_LENGTH+1]; // 1 extra checksum byte
} TX = { 0 };


// function returns zero if buffer is empty or the size
// of the message if the buffer is full
uint8 VirtualSCI_RXIsBufferFull(void)
{
	return RX.full;
}

// function returns the data byte at the specified index
uint8 VirtualSCI_RXByte(uint8 pos)
{
	// return data byte if it exists
	if (pos < sizeof(RX.buffer) && pos < RX.full)
		return RX.buffer[pos];
	return 0;
}

// function empties the RX buffer
void VirtualSCI_RXFlushMessage(void)
{
	RX.full = FALSE;
}


// function returns non-zero if there is no room in the TX buffer
// to fit a new message
uint8 VirtualSCI_TXIsBufferFull(void)
{
	return TX.len;
}

uint8 VirtualSCI_TX(uint8 len, ...)
{
	return VirtualSCI_TXBuffer(len, &len + sizeof(char));
}

// function commits the temporary message in the buffer for transmit
// returns non-zero on success, zero on failure
uint8 VirtualSCI_TXBuffer(uint8 len, const uint8 *msg)
{
	uint8 n = len;
	
	// exit buffer is full or if message is bad
	if (TX.len || !len || len > elementsof(TX.buffer)-1)
		return FALSE;

	// copy message and calculate checksum
	TX.buffer[len] = 0;
	while(n--)
	{
		TX.buffer[n] = *(msg++);
		TX.buffer[len] += TX.buffer[n];
	}

	// commit buffer
	TX.len = (uint8) (len+1);
	
	return TRUE;
}

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

// handle RX bytes and reassemble messages
static void RXHandler(uint8 msgByte)
{
	static uint8 Received7D = 0x00;
	static uint8 len = 0;
	static uint8 active = 0;
	static uint8 checksum = 0;

	// handle the byte type
	switch(msgByte)
	{
	case 0x7E: // start/end character
		// if anything was received (at least 1 byte + checksum)
		if (active && len > 1)
		{
			// place message length in buffer
			RX.full = --len; // drop checksum

			// verify checksum
			while (len)
				checksum -= RX.buffer[--len];

			// save message if checksum is correct
			if (checksum != 0)
				RX.full = FALSE;
		}

		// activate receiver if RX buffer is not full
		active = (uint8) (!RX.full);
		len = 0;
		Received7D = 0x00;
		break;
	case 0x7D: // 7D 5E is used to transmit 7D and/or 7E
		Received7D = 0x20;
		break;
	default: // normal byte
		msgByte ^= Received7D;
		Received7D = 0x00;
		
		// if receiver is active
		if (active)
		{
			// save byte while there is room in the buffer
			if (len < sizeof(RX.buffer))
				RX.buffer[len] = msgByte;
			// update message length
			if (len != 0xFF)
				len++;
			// assume this is the checksum byte
			checksum = msgByte;
		}
		break;
	}
}

static void RXDriver(uint8 rx)
{
	enum {
		RX_START_BIT = 0,
		RX_DATA_BIT0,
		RX_DATA_BIT1,
		RX_DATA_BIT2,
		RX_DATA_BIT3,
		RX_DATA_BIT4,
		RX_DATA_BIT5,
		RX_DATA_BIT6,
		RX_DATA_BIT7,
		RX_STOP_BIT
	};

	static uint8 SampleHistory = 0;
	static uint8 SkipSamples = 0;
	static uint8 state = RX_START_BIT;
	static uint8 InByte = 0;

	if (SkipSamples)
		SkipSamples--;
	else if (state == RX_START_BIT)
	{
		SampleHistory <<= 1;
		if (rx)
			SampleHistory++;
		// Get the bit value based on sample_history
		// must find an edge --> 100
		if ((SampleHistory & 0x07) == 0x04)
		{
			// got the start bit
			state++;
			SkipSamples = 2;
		}
	}
	else if (state < RX_STOP_BIT)
	{
		state++;
		SkipSamples = 2;
		InByte >>= 1;
		if (rx)
			InByte |= BIT7;
	}
	else
	{
		// did we find the stop bit
		SampleHistory = 0;
		if (rx)
		{
			SampleHistory++;
			RXHandler(InByte);
		}
		state = RX_START_BIT;
	}
}

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

// break apart TX messages into bytes
static void TXHandler(void)
{
	static uint8 active = FALSE;
	static uint8 bytecount;

	// anything to write?
	if (!TX.len)
		return;

	// start transmission next pass
	TXState = TX_START_BIT;

	// if transmitter is inactive
	if (!active)
	{
		TXDataRegister = 0x7E;  // transmit start character
		active = TRUE;          // activate driver
		bytecount = 0;          // reset byte count
		return;
	}

	// have all bytes have been sent?
	if (bytecount >= TX.len)
	{
		TXDataRegister = 0x7E;  // transmit end character
		TX.len = 0;             // remove message from TX buffer
		active = FALSE;         // deactivate driver
		return;
	}

	// transmit the next data byte
	switch(TX.buffer[bytecount])
	{
	case 0x7D:
	case 0x7E:
		TX.buffer[bytecount] ^= 0x20;
		TXDataRegister = 0x7D;
		break;
	default:
		TXDataRegister = TX.buffer[bytecount++];
		break;
	}
}

static uint8 TXDriver(void)
{
	static uint8 txBit = 0;
	
	if (TXState == TX_START_BIT)
	{
		TXState++;
		txBit = 0;
	}
	else if (TXState < TX_STOP_BIT1)
	{
		txBit = (uint8) (TXDataRegister & BIT0);
		TXDataRegister >>= 1;
		TXState++;
	}
	else if (TXState < TX_IDLE)
	{
		TXState++; /* cycle to next state */
		txBit = 1;
	}
	else //idle
		TXHandler();
		
	return txBit;
}

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
void VirtualSCI_Init(uint16 init)
{
	TX_OUTPUT_MODE_BIT = 1;			  					// 0 = toggle, 1 = set/clear on successful OC    
	TX_TIOS_BIT = 1;										// enable the OC channel 
	TX_OC_VALUE_REG = init + TX_BIT_TIME(0.5); 	// set the first OC to 0.5 bit time from now, to offset from RT
}

void VirtualSCI_Task500us(void)
{
	static uint8 BitTimer = 3;

	// read and handle RX line
	RXDriver(RX_COM_PORT);
	
	// handle software SCI transmitter every 3rd pass
	if(!--BitTimer)
	{
		BitTimer = 3;
		
		// clear flag, program next edge
		TX_OC_FLAG = 1;
		TX_OC_VALUE_REG += TX_BIT_TIME(1.0);

		// write TX line (inverted)
		if (Manufacturing_IsActive())
			TX_OC_LEVEL_BIT = (uint8) (Manufacturing_GetTouchPadPort() ? 0 : 1); 
		else TX_OC_LEVEL_BIT = (uint8) (TXDriver() ? 0 : 1); 
	}
}

//////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

#endif
