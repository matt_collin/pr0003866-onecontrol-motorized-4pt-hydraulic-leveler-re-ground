// Panel.c
// Lippert 5th Wheel leveler panel interface (auto panel)
// (c) 2004 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

#define PANEL_IN_MESSAGE               \
PANEL_MESSAGE_LENGTH(2)                \
PANEL_MESSAGE_ERROR(0, BIT7)           \
PANEL_MESSAGE_BIT(RetractAll, 0, BIT6) \
PANEL_MESSAGE_BIT(Manual,     0, BIT5) \
PANEL_MESSAGE_BIT(Auto,       0, BIT4) \
PANEL_MESSAGE_BIT(Front,      0, BIT3) \
PANEL_MESSAGE_BIT(OnOff,      0, BIT2) \
PANEL_MESSAGE_BIT(Right,      0, BIT1) \
PANEL_MESSAGE_BIT(Left,       0, BIT0) \
PANEL_MESSAGE_BIT(Rear,       1, BIT7) \

typedef struct {
	#define PANEL_MESSAGE_LENGTH(n)
	#define PANEL_MESSAGE_ERROR(byte, bit)
	#define PANEL_MESSAGE_BIT(name, byte, bit) uint8 name:1;
	PANEL_IN_MESSAGE
	#undef PANEL_MESSAGE_BIT
	#undef PANEL_MESSAGE_ERROR
	#undef PANEL_MESSAGE_LENGTH
} PANEL_INPUTS;

typedef struct {
	PANEL_INPUTS Input;
} PANEL;

extern PANEL @tiny Panel;

// define name, bit location
#define PANEL_OUT_MESSAGE                 \
PANEL_MESSAGE_LENGTH(2)                   \
PANEL_MESSAGE_ERROR(0, BIT7)              \
PANEL_MESSAGE_BIT(AllLevelLED,   1, BIT6) \
PANEL_MESSAGE_BIT(JacksDownLED,  1, BIT5) \
PANEL_MESSAGE_BIT(LowVoltageLED, 1, BIT4) \
PANEL_MESSAGE_BIT(ParkBrakeLED,  1, BIT3) \
PANEL_MESSAGE_BIT(LeftLED,       1, BIT2) \
PANEL_MESSAGE_BIT(RearLED,       1, BIT1) \
PANEL_MESSAGE_BIT(RightLED,      1, BIT0) \
PANEL_MESSAGE_BIT(FrontLED,      0, BIT7) \
PANEL_MESSAGE_BIT(ManualLED,     0, BIT6) \
PANEL_MESSAGE_BIT(AutoLED,       0, BIT5) \
PANEL_MESSAGE_BIT(SlopeLED,      0, BIT4) \
PANEL_MESSAGE_BIT(Buzzer,        0, BIT3) \
PANEL_MESSAGE_BIT(WaitLED,       0, BIT2) \
PANEL_MESSAGE_BIT(OnOffLED,      0, BIT1) \

#define PANEL_MESSAGE_LENGTH(n)
#define PANEL_MESSAGE_ERROR(byte, bit)
#define PANEL_MESSAGE_BIT(name, byte, bit) void Panel_Set##name(uint8 val);
PANEL_OUT_MESSAGE
#undef PANEL_MESSAGE_BIT
#undef PANEL_MESSAGE_ERROR
#undef PANEL_MESSAGE_LENGTH

void Panel_Init(void);
void Panel_Task(void);

uint8 Panel_Error(void);

uint16 Panel_GetInputs(void);

void Panel_SetOutputs(uint8 val);

// LEDs blink at this rate
#define FAST_BLINK_RATE  	2
#define FAULT_BLINK_RATE 	16
#define OUTPUT_2_HZ			10
#define OUTPUT_4_HZ	 		5

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// panel information
PANEL @tiny Panel = {
	{
		#define PANEL_MESSAGE_LENGTH(n)
		#define PANEL_MESSAGE_ERROR(byte, bit)
		#define PANEL_MESSAGE_BIT(name, byte, bit) 0,
		PANEL_IN_MESSAGE
		#undef PANEL_MESSAGE_BIT
		#undef PANEL_MESSAGE_ERROR
		#undef PANEL_MESSAGE_LENGTH
	}
};


#define TIMEOUT 20
static uint8 Timeout = TIMEOUT;

// incoming message
#define PANEL_MESSAGE_LENGTH(n) static uint8 @tiny RX[n];
#define PANEL_MESSAGE_ERROR(byte, bit)
#define PANEL_MESSAGE_BIT(name, byte, bit)
PANEL_IN_MESSAGE
#undef PANEL_MESSAGE_BIT
#undef PANEL_MESSAGE_ERROR
#undef PANEL_MESSAGE_LENGTH

// outgoing message
#define PANEL_MESSAGE_LENGTH(n) static uint8 @tiny TX[n];
#define PANEL_MESSAGE_ERROR(byte, bit)
#define PANEL_MESSAGE_BIT(name, byte, bit)
PANEL_OUT_MESSAGE
#undef PANEL_MESSAGE_BIT
#undef PANEL_MESSAGE_ERROR
#undef PANEL_MESSAGE_LENGTH

// output flashing
#define PANEL_MESSAGE_LENGTH(n) uint8 Flash[(n)*8], FlashTimer = 0;
#define PANEL_MESSAGE_ERROR(byte, bit)
#define PANEL_MESSAGE_BIT(name, byte, bit)
PANEL_OUT_MESSAGE
#undef PANEL_MESSAGE_BIT
#undef PANEL_MESSAGE_ERROR
#undef PANEL_MESSAGE_LENGTH

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define BIT_TO_INDEX(bit) /*lint --e(506)*/ ((bit == BIT0) ? 0 : (bit == BIT1) ? 1 : (bit == BIT2) ? 2 : (bit == BIT3) ? 3 : (bit == BIT4) ? 4 : (bit == BIT5) ? 5 : (bit == BIT6) ? 6 : 7)

#define INDEX(byte, bit)  /*lint --e(506)*/ ( ((byte) * 8) + BIT_TO_INDEX(bit))

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static uint8 TXBitOn(uint8 flash)
{
	if (!flash)
		return FALSE;
	if (flash == 1)
		return TRUE;
	return (uint8) (FlashTimer & (flash >> 1));
}

static void TX_Task(void)
{
	static uint8 debug1 = 0x01;
	static uint8 debug2 = 0x00;
   
	// don't talk to panel in test mode
	//matt
	//if (Config.ProductionBlock.ProductionByte.byte)
	//	return;

	// transmit to panel when buffer available
	if (!VirtualSCI_TXIsBufferFull())
	{
		// prepare outgoing message
		uint8 n = elementsof(TX);
		while (n--)
			TX[n] = 0;
		#define PANEL_MESSAGE_LENGTH(n)
		#define PANEL_MESSAGE_ERROR(byte, bit)
		#define PANEL_MESSAGE_BIT(name, byte, bit) if (TXBitOn(Flash[INDEX(byte,bit)])) TX[byte] |= bit;
		PANEL_OUT_MESSAGE
		#undef PANEL_MESSAGE_BIT
		#undef PANEL_MESSAGE_ERROR
		#undef PANEL_MESSAGE_LENGTH

		// handle timeout, zero out switches when communication lost
		if (Timeout)
			Timeout--;
		else
		{
			// zero out inputs
			#define PANEL_MESSAGE_LENGTH(n)
			#define PANEL_MESSAGE_ERROR(byte, bit)
			#define PANEL_MESSAGE_BIT(name, byte, bit) Panel.Input.name = 0;
			PANEL_IN_MESSAGE
			#undef PANEL_MESSAGE_BIT
			#undef PANEL_MESSAGE_ERROR
			#undef PANEL_MESSAGE_LENGTH

			// set error flag
			#define PANEL_MESSAGE_LENGTH(n)
			#define PANEL_MESSAGE_ERROR(byte, bit) TX[byte] |= bit;
			#define PANEL_MESSAGE_BIT(name, byte, bit)
			PANEL_OUT_MESSAGE
			#undef PANEL_MESSAGE_BIT
			#undef PANEL_MESSAGE_ERROR
			#undef PANEL_MESSAGE_LENGTH
		}

		if (VirtualSCI_TX(elementsof(TX), TX[0], TX[1]))
			FlashTimer++; // update flash timer
		
	}

}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void Panel_Init(void)
{
	Panel_SetOutputs(0);
}

void Panel_Task(void)
{
	uint8 len = VirtualSCI_RXIsBufferFull();

	TX_Task();

	// receive from panel
	if (len == elementsof(RX))
	{
		// unload switch data
		uint8 n = elementsof(RX);
		while (n--)
			RX[n] = VirtualSCI_RXByte(n);

		// copy message bits
		#define PANEL_MESSAGE_LENGTH(n)
		#define PANEL_MESSAGE_ERROR(byte, bit)
		#define PANEL_MESSAGE_BIT(name, byte, bit) if (RX[byte] & bit) Panel.Input.name = 1; else Panel.Input.name = 0;
		PANEL_IN_MESSAGE
		#undef PANEL_MESSAGE_BIT
		#undef PANEL_MESSAGE_ERROR
		#undef PANEL_MESSAGE_LENGTH

		// check error bit, reset timeout
		#define PANEL_MESSAGE_LENGTH(n)
		#define PANEL_MESSAGE_ERROR(byte, bit) if (!(RX[byte] & bit))
		#define PANEL_MESSAGE_BIT(name, byte, bit)
		PANEL_IN_MESSAGE
		#undef PANEL_MESSAGE_BIT
		#undef PANEL_MESSAGE_ERROR
		#undef PANEL_MESSAGE_LENGTH
			Timeout = TIMEOUT;
	}
	else if (len)
	{	
		//matt if (!Test_PanelMessageIn(len))
			return;
	}

	// flush messages when done
	if (len)
		VirtualSCI_RXFlushMessage();

	//Panel_SetOutputs(0);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

uint8 Panel_Error(void)
{
	return (uint8) (!Timeout);
}

uint16 Panel_GetInputs(void)
{
	WORD result;
	result.ui8.hi = RX[0];
	result.ui8.lo = RX[1];
	return result.ui16;
}

void Panel_SetOutputs(uint8 val)
{
	uint8 n = elementsof(Flash);
	while (n--)
		Flash[n] = val;
}

#define PANEL_MESSAGE_LENGTH(n)
#define PANEL_MESSAGE_ERROR(byte, bit)
#define PANEL_MESSAGE_BIT(name, byte, bit) void Panel_Set##name(uint8 val) { Flash[INDEX(byte,bit)] = val; }
PANEL_OUT_MESSAGE
#undef PANEL_MESSAGE_BIT
#undef PANEL_MESSAGE_ERROR
#undef PANEL_MESSAGE_LENGTH

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
