// Manufacturing.c
// 13495 Lippert 5th Wheel SW manufacturing mode
// (c) 2006, 2009, 2013 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void Manufacturing_Init(void);
void Manufacturing_Task10ms(void);
uint8 Manufacturing_MessageReceived(const uint8 * message);
uint8 Manufacturing_IsActive(void);
uint8 Manufacturing_GetTouchPadPort(void);

void Manufacturing_OnIDSCANSessionOpened(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
void Manufacturing_OnIDSCANSessionClosed(IDS_CAN_DEVICE device, uint8 address, IDS_CAN_SESSION_ID id);
void Manufacturing_CANMessageRx(const CAN_RX_MESSAGE *message);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static uint8 Active = 0;
uint8 Manufacturing_IsActive(void) { return Active; }

static uint8 TouchPadPort = 0;
uint8 Manufacturing_GetTouchPadPort(void) { return TouchPadPort; }

void Callback_OnConfigProductionBlockLoadEvent(uint8 success)
{
	if (!success)
	{
		// these values are only held in RAM
		// we do *not* overwrite the corrupted EEPROM/NVM
		// therefore, we can analyze warranty returns by reading the raw/corrupted data using MemProbe
		Config.ProductionBlock.ProductionByte = 0xFF;
		Config.ProductionBlock.XLSBsPerDegree = 2100;
		Config.ProductionBlock.YLSBsPerDegree = 2100;
		Config.ProductionBlock.VBATT_EngineRun = VBATT_LSB(11.5);
		Config.ProductionBlock.VBATT_Low = VBATT_LSB(10.2);

		// setting the MAC address to a random value
		// this is a defensive measure: it is possible that uninitialized NVM is full of FFs or 00s
		// and it would be disastrous to have multiple CAN modules with the same IDS-CAN MAC address
		// so we pre-load some pseudo-random values to be safe
		// NOTE: these are temporary values -- the production tester MUST write a valid MAC address to the part prior to shipment
		Config.ProductionBlock.MAC[0] = Random8(); 
		Config.ProductionBlock.MAC[1] = Random8();
		Config.ProductionBlock.MAC[2] = Random8();
		Config.ProductionBlock.MAC[3] = Random8();
		Config.ProductionBlock.MAC[4] = Random8();
		Config.ProductionBlock.MAC[5] = Random8();

		// set this flag so we aren't setting DTCs during manufacturing
		// needs to be in the same block as the production byte in case of load failure out in the field
		Config.ProductionBlock.DtcByte = 0x01;
	}
}

#pragma MESSAGE DISABLE C5703 /* variables not used */
void Manufacturing_OnIDSCANSessionOpened(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id)
{
   //lint -e(715)   variabels not used in the function
}
#pragma MESSAGE DEFAULT C5703

#pragma MESSAGE DISABLE C5703 /* variables not used */
void Manufacturing_OnIDSCANSessionClosed(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id)
{
   //lint -e(715)   variabels not used in the function
}
#pragma MESSAGE DEFAULT C5703

void Manufacturing_Init(void)
{
	Active = 0;
}

void Manufacturing_Task10ms(void)
{
	if (Active)
		Active--;
}

void Manufacturing_CANMessageRx(const CAN_RX_MESSAGE *message)
{
	uint8 buff[16];
	uint8 len;
	uint8 i;
	uint8 dlc;
	CAN_TX_MESSAGE msg;

	if (Config.ProductionBlock.ProductionByte)
	{
		//must be this fixed id which is in reserved range
		if ((message->ID & ~CAN_ID_EXTENDED) == 0x1C0300FF)
		{
			len = (message->Length) + 7;
			// 0   1   2     3     4     5    6     7    8    9    10   11   12   13   14
			//F9  70 <id3> <id2> <id1> <id0> <dlc> <D0> <D1> <D2> <D3> <D4> <D5> <D6> <D7>

			buff[0] = 0xF9;
			buff[1] = 0x70;
			*((uint32*) &buff[2]) = message->ID; //2,3,4,5
			buff[6] = message->Length;
			for (i = 0; i < message->Length; i++)
			{
				buff[i + 7] = message->Data[i];
			}

			// send back to PC tool
			(void) SCI_TxBuffer(len, &buff[0]);

			//echo RX message back on CAN bus, use MAC address which is programmed previously
			msg.ID = message->ID;
			dlc = message->Length;
			msg.Length = dlc;

			for (i = 0; i < dlc; i++)
			{
				if (i < elementsof(Config.ProductionBlock.MAC))
				{
					msg.Data[i] = Config.ProductionBlock.MAC[i];
				}
				else
				{
					msg.Data[i] = message->Data[i];
				}
			}

			// attempt transmit
			(void) CAN_Tx(&msg, NULL);
		}
	}
}

uint8 Manufacturing_MessageReceived(const uint8 * message)
{
	uint8 len = message[0];
	uint8 inputs = 0;
	
	// handle F9 message
	switch (message[2])
	{
		default:
			break;

		case 0x00:
		if (len == 3)
		{
			Config.ProductionBlock.ProductionByte = message[3];
			Config_ProductionBlock_Flush();
		}
		else if (len != 2)
			break;
		return SCI_Tx(3, (uint8) 0xF9, (uint8) 0x00, Config.ProductionBlock.ProductionByte);

		case 0x01:
		if (len == 3)
		{
			Config.ProductionBlock.DtcByte = message[3];
			Config_ProductionBlock_Flush();
		}
		else if (len != 2)
			break;
		return SCI_Tx(3, (uint8)0xF9, (uint8)0x01, Config.ProductionBlock.DtcByte);

		case 0x10:	// Read inputs:  F9 10
			if (!TP_RX_SIGNAL)			inputs |= BIT6;
			if (!STAT4)						inputs |= BIT5;
			if (!STAT3)						inputs |= BIT4;
			if (!STAT2)						inputs |= BIT3;
			if (!STAT1)						inputs |= BIT2;
			if (!PARK_BRAKE_SIGNAL)		inputs |= BIT1;
			if (!PSI_SWITCH_SIGNAL)		inputs |= BIT0;
			return SCI_Tx(4, 
							(uint8) 0xF9, 
							(uint8) 0x10,
							(uint8) inputs,
							(uint8) ADC_Convert8(ADC_VBATT_SIGNAL));
			break;

		case 0x13:	// Control motors/valves F9 13 
			if (len == 4) 
			{
				// reset manufacturing mode timeout 
				Active = 200;
				if (message[3] & BIT7)	Assert_PUMP_CONTROL; else Deassert_PUMP_CONTROL;
 				if (message[3] & BIT6)	Assert_4WAY_VALVE_CONTROL; else Deassert_4WAY_VALVE_CONTROL;
				if (message[3] & BIT5)	Assert_LF_CONTROL; else  Deassert_LF_CONTROL;
				if (message[3] & BIT4)	Assert_RR_CONTROL; else  Deassert_RR_CONTROL;
				if (message[3] & BIT3)	Assert_RF_CONTROL; else Deassert_RF_CONTROL;
				if (message[3] & BIT2)	Assert_LR_CONTROL; else  Deassert_LR_CONTROL;
				if (message[3] & BIT1)	Assert_AIR_FILL_CONTROL; else Deassert_AIR_FILL_CONTROL;
				if (message[3] & BIT0)	Assert_AIR_DUMP_CONTROL; else Deassert_AIR_DUMP_CONTROL;
				
				if (message[4] & BIT7)	Assert_STATUS_OUTPUT_CONTROL; else Deassert_STATUS_OUTPUT_CONTROL;
				if (message[4] & BIT6)	TouchPadPort = 1; else TouchPadPort = 0;
  			}
			else if (len != 2)
				break;
 			return SCI_Tx(4, (uint8) 0xF9, (uint8) 0x13, (uint8) message[3], (uint8) message[4]);
			break;
			
		case 0x15:
			return	SCI_Tx(13, (uint8) 0x15, 
					Sensor.X.Set, Sensor.X.Reset, Sensor.X.Filter, 
					Sensor.Y.Set, Sensor.Y.Reset, Sensor.Y.Filter);
		
		case 0x16:
			return SCI_Tx(15, (uint8) 0x16, Sensor.X.Angle, Sensor.Y.Angle);
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
