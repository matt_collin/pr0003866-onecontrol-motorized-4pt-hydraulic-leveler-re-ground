// ConsoleDriver.c
// State Machined Console Text Driver
// (c) 2015, 2016 Innovative Design Solutions, Inc.
// All rights reserved																							

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void ConsoleDriver_Task10ms(void);

void ConsoleDriver_Reset(void); 
void ConsoleDriver_AddLine(uint8 line, const char * string);
void ConsoleDriver_Update(void);
void ConsoleDriver_SetTxTime(uint8 set);
void ConsoleDriver_ClearAllLines(void);

void ConsoleDriver_ShowAngle(uint8 startline);
void ConsoleDriver_DisplayJacksThatAreMoving(uint8 line);
void ConsoleDriver_ShowCountdown(uint8 startline, uint16 total_time, uint16 time_elapsed);

extern const char * blankline;
extern const char * pleasewait;
extern const char * autoleveltext;
extern const char * analyzingtext;
extern const char * linebreak;
extern const char * remainstill;

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if CONSOLE_WIDTH != 25
	#error need to resize the strings in this file
#endif

const char * blankline = 		"                         ";
const char * pleasewait =    	"Please Wait...";
const char * autoleveltext = 	"AUTO LEVEL";
const char * analyzingtext = 	"Analyzing...";
const char * linebreak = 		"----------------";
const char * remainstill = 	"Please remain still...";

// Text output state machine
// can alternate several displays of text, with delays in between
// buffers MUST be static!

// Text output state machine
// can alternate several displays of text, with delays in between
// buffers MUST be static!

static struct {
   uint8 TxTime, Timeout;
   const char * Line[CONSOLE_HEIGHT];
} Console;

void ConsoleDriver_Update(void) { Console.Timeout = 0; }
void ConsoleDriver_SetTxTime(uint8 set) { Console.TxTime = set; }
void ConsoleDriver_Reset(void)
{
   ConsoleDriver_ClearAllLines();
   Console.Timeout = 0;
}

void ConsoleDriver_ClearAllLines(void)
{
   uint8 m;

   for (m=0; m<CONSOLE_HEIGHT; m++)
      Console.Line[m] = blankline;
}

void ConsoleDriver_AddLine(uint8 line, const char * string)
{
   if (line < CONSOLE_HEIGHT)
      Console.Line[line] = string;
}

void ConsoleDriver_Task10ms(void)
{
   if (Console.Timeout) Console.Timeout--;
   else
   {
      // commit text to the console
      IDS_CAN_SetConsoleText(DEVICE_LEVELER, &Console.Line[0]); // NOTE: must point to an array that is either CONSOLE_HEIGHT elements, or NULL terminated 
      Console.Timeout = Console.TxTime;
   }
}
typedef struct { char Sign; uint8 Whole, Fraction;} PRINT_DIGITS;

// take a fixed point value, convert to printable decimal form
static PRINT_DIGITS MakePrintable(int16 fixed, uint16 places)
{
	PRINT_DIGITS result;

	// make value positive
	if (fixed < 0)
	{
		result.Sign = '-';
		fixed = -fixed;
	}
	else
		result.Sign = '+';

	// get fractional part
	// convert to decimal readable number
	result.Fraction = (uint8) (((uint16) ((uint8) fixed * places) + 0x80) >> 8);
	
	// get whole part
	// handle fraction overflow
	fixed = ((uint16) fixed) >> 8;
	if (result.Fraction >= places)
	{
		result.Fraction = 0;
		fixed++;
	}

	// clip to max uint8
	if ((uint16) fixed > 255)
	{
	   result.Whole = 255;
	   result.Fraction = (uint8) places - 1;
	}
	else
	{
		result.Whole = (uint8) fixed;

		// due to rounding, we might have -0.0
		// ensure this always displays as a positive value
		// ensure that -0.0 does not display
		if (!result.Whole && !result.Fraction)
			result.Sign = '+';
	}

	return result;
}

void ConsoleDriver_ShowAngle(uint8 startline)
{
	// make printable
	PRINT_DIGITS digits;
	static char line1[CONSOLE_WIDTH] = "Front/Rear:   . \xB0      ";
	static char line2[CONSOLE_WIDTH] = "Side/Side:    . \xB0      ";
	
	if (startline < (CONSOLE_HEIGHT - 1))
	{
		digits = MakePrintable(Y_AXIS, 10);

		// clip
		if (digits.Whole > 9)
		{
			digits.Whole = 9;
			digits.Fraction = 9;
		}

		line1[12] = digits.Sign;
		line1[13] = (char) (digits.Whole + 0x30);
		line1[15] = (char) (digits.Fraction + 0x30);

		digits = MakePrintable(X_AXIS, 10);

		// clip
		if (digits.Whole > 9)
		{
			digits.Whole = 9;
			digits.Fraction = 9;
		}

		line2[12] = digits.Sign;
		line2[13] = (char) (digits.Whole + 0x30);
		line2[15] = (char) (digits.Fraction + 0x30);

		ConsoleDriver_AddLine(startline, line1);
		ConsoleDriver_AddLine(startline+1, line2);
	}
}

void ConsoleDriver_DisplayJacksThatAreMoving(uint8 line)
{
	static char text[CONSOLE_WIDTH]; // local buffer for text
	const char * jack_text;
	uint8 i;
	DIRECTION display_direction = Jack_GetPumpAct();
	JACKS moving = Jack_GetJacksAct();
   
   // concat the direction and jack strings
	if (display_direction == DIRECTION_EXTEND)	{ i = 10; (void) memcpy(text, "Extending                ", CONSOLE_WIDTH); }
	else if (display_direction == DIRECTION_RETRACT)	{ i = 11; (void) memcpy(text, "Retracting               ", CONSOLE_WIDTH); }
	else
	{
		// no jacks moving.  blank the line and return;
		ConsoleDriver_AddLine(line, blankline);
		return;
	}

    switch (moving)
    {
        case JACKS_FRONT: jack_text = "Front jacks"; break;
        case JACKS_RIGHT: jack_text = "Right jacks"; break;
        case JACKS_LEFT:  jack_text = "Left jacks "; break;
        case JACKS_REAR:  jack_text = "Rear jacks "; break;
        case JACKS_LF:    jack_text = "Left Front "; break;
        case JACKS_RF:    jack_text = "Right Front"; break;
        case JACKS_LR:    jack_text = "Left Rear  "; break;
        case JACKS_RR:    jack_text = "Right Rear "; break;

        default: //lint -fallthrough
        case JACKS_ALL:
        case JACKS_NONE:  jack_text = "           "; break;
    }

	if (i + 11 <= CONSOLE_WIDTH) // safety check
	{
	   (void) memcpy(&text[i], jack_text, 11);
	}
	ConsoleDriver_AddLine(line, &text[0]);
}

void ConsoleDriver_ShowCountdown(uint8 startline, uint16 total_time, uint16 time_elapsed)
{
	static char text[CONSOLE_WIDTH] = "Please wait...          ";
	static uint8 last = 0;
	uint8 left = 0;
	
		
	if (startline < CONSOLE_HEIGHT)
	{
		left = (uint8) ((total_time - time_elapsed) / 100);
		
		if (last != left)
		{
			text[16] = (char) ((left / 10) + 0x30);
			text[17] = (char) ((left % 10) + 0x30);

			ConsoleDriver_AddLine(startline, text);
		}
	
		last = left;
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
