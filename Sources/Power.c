// Power.c
// power managment and monitoring
// (c) 2007, 2010 Innovative Design Solutions, Inc.
// All rights reserved

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

void Power_ExtSensorPowerTask1ms(void);

typedef enum { POWER_OFF, POWER_ON, POWER_FAULT } POWER_STATE;

void Power_EnableExternalSensor(void);
void Power_DisableExternalSensor(void);
POWER_STATE Power_ExternalSensorState(void);

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#include "global.h"

static uint8 WantExtSensorEnabled = FALSE;
static POWER_STATE ExtSensorState = POWER_OFF;

void Power_EnableExternalSensor(void)       { WantExtSensorEnabled = TRUE; }
void Power_DisableExternalSensor(void)      { WantExtSensorEnabled = FALSE; }
POWER_STATE Power_ExternalSensorState(void) { return WantExtSensorEnabled ? ExtSensorState : POWER_OFF; }

void Power_ExtSensorPowerTask1ms(void)
{
	static uint8 Enable = FALSE;
	static uint8 FaultTimer = 0;
	static uint8 StableTimer = 0;

	if (Manufacturing_IsActive())
	  return;
	
	if (WantExtSensorEnabled)
		Enable = 0xFF; // keep feed on for 255 ms after user requests it off
	else if (Enable)
		Enable--;

	// control hall feed power
	if (!Enable)
	{
		// we want the hall feed off
		ExtSensorState = POWER_OFF;
		Deassert_EXT_SENSOR_VBATT_CONTROL;
	}
	else if (EXT_SENSOR_VBATT_CONTROL) // we want the feed on, check if it is already on
	{
		// hall feed is on, check for fault
		if (!EXT_SENSOR_SPROT)
		{
			FaultTimer = 0; // circuit OK
			if (StableTimer)
				StableTimer--;
			else
				ExtSensorState = POWER_ON;
		}
		else if (++FaultTimer >= 5)
		{
			ExtSensorState = POWER_FAULT;
			Deassert_EXT_SENSOR_VBATT_CONTROL;
		}
	}
	else if (ExtSensorState != POWER_FAULT) // we want the feed on, it isn't on, can we turn it on?
	{
		// turn on the hall feed
		Assert_EXT_SENSOR_VBATT_CONTROL;
		FaultTimer = 0;
		StableTimer = 0xFF;
	}

}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif
